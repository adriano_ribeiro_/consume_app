let finishSalesTable = null,
    finishSaleTable = null,
    isModalShow = false,
    STATUS_FINISH = 0;

$(()=> {
    getOpenFinishSales();
    initFinishSaleTable();
});

function getOpenFinishSales() {
    finishSalesTable = $('#finish-sales-table').DataTable({
        "ajax": {
            "url": uri.saleItem.getOpenFinishSales(STATUS_FINISH),
            "beforeSend": function (xhr) {
                // xhr.setRequestHeader("Authorization", "Bearer " + token);
                loadingDataTable();
            }
        },
        "columns": [
            { "data":"sale_value"},
            { "data":"updated_at"},
            { "data":"id" }
        ],
        "columnDefs": [
            {
                "targets": 0,
                'render': function (data, type, full, meta) {
                    return maskMoneyPTBR(data);
                }
            },
            {
                "targets": 1,
                'render': function (data, type, full, meta) {
                    return data ? moment(data).utcOffset('GMT-3:00').format('DD/MM/YYYY HH:mm:ss') : "";
                }
            },
            {
                "render": function (data) {
                    return `<button onClick="details(${data})" class="btn btn-info btn-sm scroll-click"><i class="icon-copy fa fa-info-circle" aria-hidden="true"></i> Detalhes</button>`
                },
                "targets": 2,
                "searchable": false,
                "orderable":  false,
            }
        ],
        'scrollCollapse': true,
        "serverSide": true,
        "processing": true,
        // "dom": 'lrtip',
        "lengthMenu": [[10, 20, 30], [10, 20, 30]],
        "language": {
            'url': URL_DATATABLE_LANGUAGE,
            paginate: {
                next: '<i class="ion-chevron-right"></i>',
                previous: '<i class="ion-chevron-left"></i>'
            }
        },
        'order': [[1, 'desc']]
    });
}

function initFinishSaleTable() {
    finishSaleTable = $('#finish-sale-table').DataTable({
        "language": {
            "url": URL_DATATABLE_LANGUAGE
        }
    });
}

function finishSaleTableModal(saleId) {
    finishSaleTable = $('#finish-sale-table').DataTable({
        "ajax": {
            "url": uri.saleItem.current(saleId),
            "beforeSend": function (xhr) {
                // xhr.setRequestHeader("Authorization", "Bearer " + token);
            }
        },
        "columns": [
            {"data": "barcode"},
            {"data": "product"},
            {"data": "unit_price"},
            {"data": "quantity"},
            {"data": "subtotal"},
        ],
        "columnDefs": [
            {
                "targets": 2,
                'render': function (data, type, full, meta) {
                    return maskMoneyPTBR(data);
                }
            },
            {
                "targets": 4,
                'render': function (data, type, full, meta) {
                    full.sale_value 
                    ? $('.sale_value').html(maskMoneyPTBR(full.sale_value.toString()))
                    : $('.sale_value').html('R$ 0,00');
                    return maskMoneyPTBR(data);
                }
            }
        ],
        'scrollCollapse': true,
        "dom": 'lrtip',
        "language": {
            'url': URL_DATATABLE_LANGUAGE,
            paginate: {
                next: '<i class="ion-chevron-right"></i>',
                previous: '<i class="ion-chevron-left"></i>'
            }
        },
        'autoWidth': true,
        "paging":   false,
        "ordering": false,
        "info":     false,
        'order': [[0, 'desc']]
    });
}

function details(data) {
    finishSaleTable.clear().destroy();
    finishSaleTableModal(data);
    setTimeout(() => {
        showLoading('Por favor aguarde', 'Carregando . . .');
        $('#modal-details').modal({ backdrop: 'static', keyboard: true, show: true });
        swal.close();
    }, 100);
}

function print(data) {
    
}