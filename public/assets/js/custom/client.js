
let clientTable = null,
    isModalShow = false;

$(()=> {
    addMask('#phone', '(00) 0-0000-0000');
    addMask('#birth_date', '00/00/0000');
    removeStyle();
    clientAll();
    $(document).on('keypress', function(e) {
        // e.preventDefault();
        if(e.which == 13 && isModalShow) {
            e.preventDefault();
            createOrUpdate();
        }
    });
});

function clientAll() {
    clientTable = $('#clients-table').DataTable({
        "ajax": {
            "url": uri.client.all,
            "beforeSend": function (xhr) {
                // xhr.setRequestHeader("Authorization", "Bearer " + token);
                loadingDataTable('clients-table');
            }
        },
        "columns": [
            { "data":"name" },
            { "data":"email" },
            { "data":"instagram" },
            { "data":"phone" },
            { "data":"birth_date" },
            { "data":"id" }
        ],
        "columnDefs": [
            {
                "targets": 3,
                'render': function (data, type, full, meta) {
                    var formatter = new StringMask("(00) 0.0000-0000", { reverse: true });
                    return  formatter.apply(data);
                }
            },
            {
                "targets": 4,
                'render': function (data, type, full, meta) {
                    return data ? moment(data).utcOffset('GMT-3:00').format('DD/MM/YYYY') : "";
                }
            },
            {
                "render": function (data) {
                    return `<button onClick="modalCreateOrUpdate(${data})" class="btn btn-info btn-sm scroll-click"><i class="icon-copy fa fa-pencil" aria-hidden="true"></i> Alterar</button>`
                },
                "targets": 5,
                "searchable": false
            }
        ],
        "scrollCollapse": true,
            "serverSide": true,
            "processing": true,
            // "dom": "lrtip",
            "lengthMenu": [[10, 20, 30], [10, 20, 30]],
            "language": {
                "url": URL_DATATABLE_LANGUAGE,
                "paginate": {
                    "next": "<i class='ion-chevron-right'></i>",
                    "previous": "<i class='ion-chevron-left'></i>"
                }
            },
        'order': [[0, 'asc']]
    });
}

function modalCreateOrUpdate(data) {
    if(!data) {
        $('#client-create-update').trigger("reset");
        $('.title-form-client').text('Cadastro de clientes');
        $('.create-update').text('Salvar');
        $('.password').removeAttr('hidden');
        $('#modal-create-or-update').modal({ backdrop: 'static', keyboard: true, show: true });
    } else {
        details(data);
    }
    isModalShow = true;
    $('#name').trigger("focus");
}

function createOrUpdate() {
    if(!$('#client_id').val()) {
        create();
    } else {
        update($('#client_id').val());
    }
}

function validateData() {
    let data = {
        'name': $.trim($('#name').val()),
        'email': $.trim($('#email').val()),
        'instagram': $.trim($('#instagram').val()),
        'phone': $.trim(removeMask($('#phone'))),
        'birth_date': $.trim($('#birth_date').val()),
        'status': true
    };

    let isValid = true;

    if(!data.name) {
        addBorderRed('#name');
        messageError('Informe o nome do cliente');
        isValid = false;
    }

    // if(!data.email) {
    //     addBorderRed('#email');
    //     messageError('EMAIL OBRIGATÓRIO');
    //     isValid = false;
    // }

    if(data.email && !validaEmail(data.email)) {
        addBorderRed('#email');
        messageError('Informe um email válido');
        isValid = false;
    }

    if(data.birth_date) {
        if (!validDate(data.birth_date)) {
            addBorderRed('#birth_date');
            messageError('Informe a data válida');
            isValid = false;
        }
        data.birth_date = data.birth_date.replaceAll("/", "-");
    }

    return !isValid ? isValid : data;
}

function create() {
    let data = validateData();

    if(!data) {
        return false;
    }

    $.ajax({
        type: 'POST',
        url: uri.client.create,
        data: JSON.stringify(data),
        dataType: 'json',
        headers: {
            "content-type": "application/json;charset=UTF-8"
        },
        beforeSend: function (xhr) {
            showLoading('Por favor aguarde', 'Cadastrando cliente. . .');
            // xhr.setRequestHeader(`Authorization', 'Bearer ${token}`);
        },
        success: function (response) {
            swal.close();
            $('#client-create-update').trigger("reset");
            clientTable.ajax.reload();
            messageSuccess("Cliente cadastrado com sucesso!");
        },
        error: function (data) {
            swal.close();
            if (data.status === HTTP_INTERNAL_SERVER_ERROR) {
                messageError("Ocorreu uma inconsistência no sistema ao tentar cadastrar um cliente.");
            } else if (data.status === HTTP_UNAUTHORIZED && data.responseJSON.message === "Expired JWT Token") {
                swal(
                    {
                        title: 'Informação',
                        text: 'Sua sessão expirou faça o login novamente.',
                        type: 'info',
                        allowOutsideClick: false
                    }
                ).then((result) => {
                    if (result.value) {
                        window.location.href = "/logout";
                    }
                });
            } else if (data.responseJSON.message) {
                messageError(data.responseJSON.message);
            } else {
                messageError("Ocorreu uma inconsistência no sistema ao tentar cadastrar um cliente.");
            }
        }
    });
}

function update(id) {
    let data = validateData();

    if(!data) {
        return false;
    }

    $.ajax({
        type: 'POST',
        url: uri.client.update(id),
        data: JSON.stringify(data),
        dataType: 'json',
        headers: {
            "content-type": "application/json;charset=UTF-8"
        },
        beforeSend: function (xhr) {
            showLoading('Por favor aguarde', 'Atualizando os dados do cliente. . .');
            // xhr.setRequestHeader(`Authorization', 'Bearer ${token}`);
        },
        success: function (response) {
            swal.close();
            $('#client_id').val(null);
            $('#client-create-update').trigger("reset");
            clientTable.ajax.reload();
            $('#modal-create-or-update').modal('hide');
            isModalShow = false;
            messageSuccess("Dados alterado com sucesso!");
        },
        error: function (data) {
            swal.close();
            if (data.status === HTTP_INTERNAL_SERVER_ERROR) {
                messageError("Ocorreu uma inconsistência no sistema ao tentar alterar os dados do cliente.");
            } else if (data.status === HTTP_UNAUTHORIZED && data.responseJSON.message === "Expired JWT Token") {
                swal(
                    {
                        title: 'INFORMAÇÃO',
                        text: 'Sua sessão expirou faça o login novamente.',
                        type: 'info',
                        allowOutsideClick: false
                    }
                ).then((result) => {
                    if (result.value) {
                        window.location.href = "/logout";
                    }
                });
            } else if (data.responseJSON.message) {
                messageError(data.responseJSON.message);
            } else {
                messageError("Ocorreu uma inconsistência no sistema ao tentar alterar os dados do cliente.");
            }
        }
    });
}

function details(id) {
    $.ajax({
        type: 'GET',
        url: uri.client.find(id),
        dataType: 'json',
        headers: {
            "content-type": "application/json;charset=UTF-8"
        },
        beforeSend: function (xhr) {
            showLoading('Por favor aguarde', 'Carregando os dados do cleinte. . .');
            // xhr.setRequestHeader(`Authorization', 'Bearer ${token}`);
        },
        success: function (response) {
            swal.close();
            $('#client-create-update').trigger("reset");
            $('#client_id').val(response.data.id);
            $('#name').val(response.data.person.name);
            $('#email').val(response.data.person.email);
            $('#instagram').val(response.data.instagram);
            $('#phone').val(response.data.phone);
            $('#birth_date').val(response.data.birth_date ? moment(response.data.birth_date).utcOffset('GMT-3:00').format('DD/MM/YYYY') : "");
            $('.title-form-client').text('Alterar dados do cliente');
            $('.create-update').text('Alterar');
            $('#modal-create-or-update').modal({ backdrop: 'static', keyboard: true, show: true });
        },
        error: function (data) {
            swal.close();
            if (data.status === HTTP_INTERNAL_SERVER_ERROR) {
                messageError("Ocorreu uma inconsistência no sistema, se persistir contate o suporte.");
            } else if (data.status === HTTP_UNAUTHORIZED && data.responseJSON.message === "Expired JWT Token") {
                swal(
                    {
                        title: 'Informação',
                        text: 'Sua sessão expirou faça o login novamente.',
                        type: 'info',
                        allowOutsideClick: false
                    }
                ).then((result) => {
                    if (result.value) {
                        window.location.href = "/logout";
                    }
                });
            } else if (data.responseJSON.message) {
                messageError(data.responseJSON.message);
            } else {
                messageError("Ocorreu uma inconsistência no sistema, se persistir contate o suporte.");
            }
        }
    });
}

function removeStyle() {
    removeBorderOnFocus('#name');
    removeBorderOnFocus('#email');
    removeBorderOnFocus('#instagram');
    removeBorderOnFocus('#phone');
    removeBorderOnFocus('#birth_date');
}

function modalClose() {
    isModalShow = false;
    $('#client_id').val(null);
    removeBorder('#name');
    removeBorder('#email');
    removeBorder('#instagram');
    removeBorder('#phone');
    removeBorder('#birth_date');
}