
$(()=> {
    $('#username').trigger('focus');
    auth();
});

function auth() {
    $('#user-auth').on('submit', function(e) {
        e.preventDefault();
        let username = $('#username').val(); //document.getElementById('username').value,
            password = $('#password').val() //document.getElementById('password').value;

        let isValid = true;

        if(!username) {
            addBorderRed('#username');
            messageError('Informe o login do usuário');
            isValid = false;
        }

        if(!password) {
            addBorderRed('#password');
            messageError('Informe a senha do usuário');
            isValid = false;
        }

        if(!isValid) {
            return false;
        }

        $.ajax({
            type: 'POST',
            url: '/api/v1/user/auth',
            data: JSON.stringify({"login": username, "password": password}),
            dataType: 'json',
            headers: {
                "content-type": "application/json;charset=UTF-8"
            },
            beforeSend: function (xhr) {
                showLoading('Por favor aguarde', 'Carregando. . .');
            },
            success: function (response) {
                swal.close();
                window.location.href = '/home';
            },
            error: function (data) {
                swal.close();
                if (data.status === 500) {
                    messageError("Ocorreu uma inconsistência no sistema ao tentar cadastrar uma categoria.");
                } else if (data.status === 401 && data.responseJSON.message === "Expired JWT Token") {
                    swal(
                        {
                            title: 'Informação',
                            text: 'Sua sessão expirou faça o login novamente.',
                            type: 'info',
                            allowOutsideClick: false
                        }
                    ).then((result) => {
                        if (result.value) {
                            window.location.href = "/logout";
                        }
                    });
                } else if (data && data.responseJSON && data.responseJSON.message) {
                    messageError(data.responseJSON.message);
                } else {
                    messageError("Ocorreu uma inconsistência no sistema ao tentar efetuar o login.");
                }
            }
        });
    });
}

function showLoading(message, text) {

    swal({
        title: message,
        text: text,
        allowOutsideClick: false,
        onBeforeOpen: function () {
            swal.showLoading();
        }
    });
}

function addBorderRed(element) {
    $(element).attr("style", "border: 1px solid #dc3545");
}

function removeBorder(element) {
    $(element).removeAttr("style");
}

function removeBorderOnFocus(element) {
    $(element).on('focus', () => {
        $(element).removeAttr("style");
    });
}

/**
 * @param message
 */
 function messageSuccess(message) {
    toastr.success(message, 'Sucesso');
}

/**
 * @param message
 */
function messageError(message) {
    toastr.error(message, 'Atenção');
}