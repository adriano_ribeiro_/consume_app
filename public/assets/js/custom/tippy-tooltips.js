$(() => {
    documentToSign(); // Documentos para assinar
    historic(); // Histórico de fluxos
    authenticityDocument(); // Autenticação de documentos
    authorizerFlow(); // Autorização de fluxo
    professionalProfile(); // Solicitação de perfil profissional
    companyRepresent(); // Representante de empresas
    observerFlow(); // Observador de fluxo
    contactBook(); // Lista de contatos
    adhesionCredit(); // Adesão de crédito
    payments(); // Pagamentos assinatura
    refuse(); // Estorno de crédito
    levelUpgrade2(); // Upgrade Nível 2
    calendar(); // Calendário
    approveDisapproveFlow(); // Aprova/Reprova fluxo
    templateDocument(); // Modelo de documentos
    legalRepresentative(); // Adicionando representante legal
    listApresenterLegal(); // Lista de representantes legais
    groupCompany(); // Grupos para empresa
    downloadDocuments(); // Download documentos
    newSign(); // Nova assinatura
    addNewFlow(); // Cadastrar novo fluxo
    addPaperDocuments(); // Formulário de adicionar papéis assinatura em lote
    upgradeLevel(); // Solicitação de nível 2
    shoppingCart(); // Carrinho de compra
    headerBar(); // Botões do barra de cabeçalho
    dropdownHeaderBar(); // Dropdown do perfil no headerbar
    levelSecurity();  // Nível de segurança
    reopenFlows(); // Reabertura dos fluxos
    secondFactor(); // autenticação de segundo fator
});

function documentToSign() {
    tippy('#infoDocToSign', {
        content: '<b>Lista de documentos pendentes para você assinar.<b>',
        placement: 'right',
        animation: 'scale',
        allowHTML: true,
        trigger: 'mouseenter click',
    });
}

function historic() {
    tippy('#infoSearch', {
        content: '<b>Listagem dos serviços de assinatura em que você participou.<b>',
        placement: 'right',
        animation: 'scale',
        allowHTML: true,
        trigger: 'mouseenter click',
    });
    tippy('#infoHistoric', {
        content: '<b>Listagem dos serviços de assinatura em que você participou.<b>',
        placement: 'right',
        animation: 'scale',
        allowHTML: true,
        trigger: 'mouseenter click',
    });
    tippy('#infoTableBatchHistoric', {
        content: '<b>Listagem dos serviços de assinaturas em lote.<b>',
        placement: 'right',
        animation: 'scale',
        allowHTML: true,
        trigger: 'mouseenter click',
    });
}

function authenticityDocument() {
    tippy('#infoAuthenticity', {
        content: '<b>Verifique se seu documento está autenticado no nosso sistema.<b>',
        placement: 'right',
        animation: 'scale',
        allowHTML: true,
        trigger: 'mouseenter click',
    });
}

function authorizerFlow() {
    tippy('#infoAuthorizer', {
        content: '<b>Lista de fluxos aguardando sua autorização.<b>',
        placement: 'right',
        animation: 'scale',
        allowHTML: true,
        trigger: 'mouseenter click',
    });
}

function professionalProfile() {
    tippy('#infoProfessionalProfile', {
        content: '<b>Solicite o cadastro de sua empresa.<b>',
        placement: 'right',
        animation: 'scale',
        allowHTML: true,
        trigger: 'mouseenter click',
    });
}

function companyRepresent() {
    tippy('#infoCompanyRepresent', {
        content: '<b>Aqui estão todas as empresas que você representa.<b>',
        placement: 'right',
        animation: 'scale',
        allowHTML: true,
        trigger: 'mouseenter click',
    });
}

function observerFlow() {
    tippy('#infoObserver', {
        content: '<b>Lista de fluxos que você pode observar.<b>',
        placement: 'right',
        animation: 'scale',
        allowHTML: true,
        trigger: 'mouseenter click',
    });
}

function contactBook() {
    tippy('#infoContactBook', {
        content: '<b>Lista de contatos salvos para adicionar em fluxos de assinatura.<b>',
        placement: 'right',
        animation: 'scale',
        allowHTML: true,
        trigger: 'mouseenter click',
    });
}

function adhesionCredit() {
    tippy('#infoAdhesionCredit', {
        content: '<b>Faça adesão de crédito para sua conta.<b>',
        placement: 'right',
        animation: 'scale',
        allowHTML: true,
        trigger: 'mouseenter click',
    });
}

function payments() {
    tippy('#infoPayments', {
        content: '<b>Serviços aguardando pagamento solicitado por outras pessoas.<b>',
        placement: 'right',
        animation: 'scale',
        allowHTML: true,
        trigger: 'mouseenter click',
    });
}

function refuse() {
    tippy('#infoRefuse', {
        content: '<b>Lista dos seus estornos de créditos pendentes.<b>',
        placement: 'right',
        animation: 'scale',
        allowHTML: true,
        trigger: 'mouseenter click',
    });
}

function levelUpgrade2() {
    tippy('#infoLevelUpgrade2', {
        content: '<b>Faça o UPGRADE para o nível ouro da sua e-Identidade.<br>Basta enviar a cópia de um dos documentos abaixo, para que nosso suporte possa analisar seus dados.<b>',
        placement: 'right',
        animation: 'scale',
        allowHTML: true,
        trigger: 'mouseenter click',
    });
}

function calendar() {
    tippy('#infoCalendar', {
        content: '<b>Visualize no calendário as datas dos seus serviços pendentes e finalizados.<b>',
        placement: 'right',
        animation: 'scale',
        allowHTML: true,
        trigger: 'mouseenter click',
    });
}

function approveDisapproveFlow() {
    tippy('#infoApproveDisapproveFlow', {
        content: '<b>Lista de fluxos aguardando sua analise.<b>',
        placement: 'right',
        animation: 'scale',
        allowHTML: true,
        trigger: 'mouseenter click',
    });
}

function templateDocument() {
    tippy('#infoTemplateDocument', {
        content: '<b>Monte seu modelo de documento para ser usado em suas assinaturas em lote.<b>',
        placement: 'right',
        animation: 'scale',
        allowHTML: true,
        trigger: 'mouseenter click',
    });
}

function legalRepresentative() {
    tippy('#infoLegalRepresentative', {
        content: '<b>Adicione representantes legais para o perfil de sua empresa.<b>',
        placement: 'right',
        animation: 'scale',
        allowHTML: true,
        trigger: 'mouseenter click',
    });
}

function listApresenterLegal() {
    tippy('#infoListApresenterLegal', {
        content: '<b>Lista dos representantes legais da sua empresa.<b>',
        placement: 'right',
        animation: 'scale',
        allowHTML: true,
        trigger: 'mouseenter click',
    });
}

function groupCompany() {
    tippy('#infoGroupCompany', {
        content: '<b>Lista dos grupos cadastrados para sua empresa.<b>',
        placement: 'right',
        animation: 'scale',
        allowHTML: true,
        trigger: 'mouseenter click',
    });
}

function downloadDocuments() {
    tippy('#infoDownloadDocuments', {
        content: '<b>Aqui você pode baixa os documentos de seus serviços.<b>',
        placement: 'right',
        animation: 'scale',
        allowHTML: true,
        trigger: 'mouseenter click',
    });
}

function upgradeLevel() {
    tippy('#infoUpgradeLevel2', {
        content: '<b>Envie seus documentos para solicitar o nível ouro.<b>',
        placement: 'right',
        animation: 'scale',
        allowHTML: true,
        trigger: 'mouseenter click',
    });

    tippy('#infoUpgradeLevel3', {
        content: '<b>Envie seus documentos para solicitar o nível platinum.<b>',
        placement: 'right',
        animation: 'scale',
        allowHTML: true,
        trigger: 'mouseenter click',
    });

    tippy('#infoSelfieWithDocument', {
        content: '<b>Envie uma selfie segurando seu documento ao lado do seu rosto.<b>',
        placement: 'right',
        animation: 'scale',
        allowHTML: true,
        trigger: 'mouseenter click',
    });

    tippy('#infoDataExtra', {
        content: '<b>São dados extras que você pode enviar para ficar mais preciso a validação com a Serpro.<b>',
        placement: 'right',
        animation: 'scale',
        allowHTML: true,
        trigger: 'mouseenter click',
    });

    tippy('#infoDocumentIdentifier', {
        content: '<b>Informe o número de identificação do documento<b>',
        placement: 'right',
        animation: 'scale',
        allowHTML: true,
        trigger: 'mouseenter click',
    });

    tippy('#infoIssuingBody', {
        content: '<b>Informe o orgão emissor (abreviado) do documento<b>',
        placement: 'right',
        animation: 'scale',
        allowHTML: true,
        trigger: 'mouseenter click',
    });

    tippy('#infoIssueDate', {
        content: '<b>Informe a data de expedição do documento<b>',
        placement: 'right',
        animation: 'scale',
        allowHTML: true,
        trigger: 'mouseenter click',
    });

    tippy('#infoNationality', {
        content: '<b>Informe a sua nacionalidade<b>',
        placement: 'right',
        animation: 'scale',
        allowHTML: true,
        trigger: 'mouseenter click',
    });
}

// inicio nova assinatura
function newSign() {
    tippy('#infoFlow', {
        content: '<b>Sua assinatura será entre pessoas físicas ou jurídicas?<b>',
        placement: 'right',
        animation: 'scale',
        allowHTML: true,
        trigger: 'mouseenter click',
    });
    tippy('#infoFlowPJ', {
        content: '<b>Escolha um fluxo pré definido ou um simples<b>',
        placement: 'right',
        animation: 'scale',
        allowHTML: true,
        trigger: 'mouseenter click',
    });
    tippy('#infoSignCNPJ', {
        content: '<b>CNPJ da empresa que vai assinar o documento<b>',
        placement: 'right',
        animation: 'scale',
        allowHTML: true,
        trigger: 'mouseenter click',
    });
    tippy('#infoAddNewFlow', {
        content: '<b>Cadastre um novo fluxo auto aprovado ou escolha um grupo para aprovar esse novo fluxo.<b>',
        placement: 'right',
        animation: 'scale',
        allowHTML: true,
        trigger: 'mouseenter click',
    });
    tippy('#infoIdentifier', {
        content: '<b>Identificador customizado para facilitar na busca do seu documento.<b>',
        placement: 'right',
        animation: 'scale',
        allowHTML: true,
        trigger: 'mouseenter click',
    });
    tippy('#infoTypeDocument', {
        content: '<b>Tipo de documento que as partes vão assinar.<b>',
        placement: 'right',
        animation: 'scale',
        allowHTML: true,
        trigger: 'mouseenter click',
    });
    tippy('#infoFlowSign', {
        content: `<b>Define a ordem dos participantes.<br>
                     Exemplo 1: assinantes, testemunhas, aprovadores.<br><br>
                     Primeiro quem visualiza o documento são os<br> participante(s) com o(s) papel(is) de assinante(s),<br><br>
                     Depois as testemunha(s) e por ultimo o(s) aprovador(es).<br><br>
                     Exemplo 2: aprovadores, assinantes, testemunhas.<br><br>
                     Primeiro quem visualiza o documento são os<br> participante(s) com o(s) papel(is) de aprovador(es),<br><br>
                     Depois as testemunha(s) e por ultimo o(s) assinante(s).<br><br>
                     O fluxo segue na ordem definida.
                  <b>`,
        placement: 'right',
        animation: 'scale',
        allowHTML: true,
        trigger: 'mouseenter click',
    });
    tippy('#infoDeadline', {
        content: '<b>Data limite para assinar o documento, caso não defina uma data o processo ocorrerar de acordo com o fluxo.<b>',
        placement: 'right',
        animation: 'scale',
        allowHTML: true,
        trigger: 'mouseenter click',
    });
    tippy('#infoUploadDocument', {
        content: '<b>Documento que será assinado.<b>',
        placement: 'right',
        animation: 'scale',
        allowHTML: true,
        trigger: 'mouseenter click',
    });
    tippy('#infoTemplateDocumentBatch', {
        content: '<b>Escolha um tipo de documento para carregar os modelos de documentos e escolha um para assinar.<b>',
        placement: 'right',
        animation: 'scale',
        allowHTML: true,
        trigger: 'mouseenter click',
    });
    tippy('#infoOrder', {
        content: '<b>Ordem para as partes, ao definir ordem as partes precisam esperar sua vez de assinar o documento, a ordem é de acordo com a linha da tabela, de cima para baixo.<b>',
        placement: 'right',
        animation: 'scale',
        allowHTML: true,
        trigger: 'mouseenter click',
    });
    tippy('#infoRenderTemplate', {
        content: '<b>Visualize seu modelo de documento com os campos que serão substituido pelo os valores que você definir.<b>',
        placement: 'right',
        animation: 'scale',
        allowHTML: true,
        trigger: 'mouseenter click',
    });
    tippy('#infoMagicFields', {
        content: '<b>Os dados substitui os campos que são referente ao modelo de documento escolhido, pode ser adicionado por arquivo CSV ou adicione manualmente por formulário. ' +
            ' Os campos são definidos por: {{ nome_do_campo }}<b>',
        placement: 'right',
        animation: 'scale',
        allowHTML: true,
        trigger: 'mouseenter click',
    });
    tippy('#infoPapers', {
        content: '<b>Adicione os papéis de acordo com fluxo escolhido.<b>',
        placement: 'right',
        animation: 'scale',
        allowHTML: true,
        trigger: 'mouseenter click',
    });
    tippy('#infoFileCsvDocument', {
        content: '<b>Arquivo que contem os dados que substituirão os campos referente ao documento que vai ser assinado.<b>',
        placement: 'right',
        animation: 'scale',
        allowHTML: true,
        trigger: 'mouseenter click',
    });
    tippy('#infoFileCsvFlow', {
        content: '<b>Arquivo que contem todos os papéis referente ao fluxo de assinatura.<b>',
        placement: 'right',
        animation: 'scale',
        allowHTML: true,
        trigger: 'mouseenter click',
    });
    tippy('#infoIsRepresentative', {
        content: '<b>Esse participante vai representar uma empresa?<br> Se sim digite o CNPJ da empresa.<b>',
        placement: 'right',
        animation: 'scale',
        allowHTML: true,
        trigger: 'mouseenter click',
    });
}

// fim nova assinatura

function addNewFlow() {
    tippy('#infoNameNewFlow', {
        content: '<b>Nome para identificar a ordem do(s) participante(s).<b>',
        placement: 'right',
        animation: 'scale',
        allowHTML: true,
        trigger: 'mouseenter click',
    });
    tippy('#infoTypesSignFlow', {
        content: '<b>Defina se seu fluxo é para pessoa física, júridica interna e/ou júridica externa.<b>',
        placement: 'right',
        animation: 'scale',
        allowHTML: true,
        trigger: 'mouseenter click',
    });
    tippy('#infoPaperNewFlow', {
        content: '<b>Papéis que ocorrerá no fluxo para assinar o documento.<b>',
        placement: 'right',
        animation: 'scale',
        allowHTML: true,
        trigger: 'mouseenter click',
    });
    tippy('#infoGroupNewFlow', {
        content: '<b>Caso não escolha um setor, o fluxo será auto aprovado.<b>',
        placement: 'right',
        animation: 'scale',
        allowHTML: true,
        trigger: 'mouseenter click',
    });

    tippy('#infoGroupAuthorizer', {
        content: '<b>Para selecionar um ou mais setor(es) para autorizar o fluxo, precisa selecionar o papel autorizador nos papéis de assinatura.<b>',
        placement: 'right',
        animation: 'scale',
        allowHTML: true,
        trigger: 'mouseenter click',
    });
    tippy('#infoGroupObserver', {
        content: '<b>Para selecionar um ou mais setor(es) para observar o fluxo, precisa selecionar o papel autorizador nos papéis de assinatura..<b>',
        placement: 'right',
        animation: 'scale',
        allowHTML: true,
        trigger: 'mouseenter click',
    });
    tippy('#infoGroupSubscriber', {
        content: '<b>Para selecionar um ou mais setor(es) para assinanr o fluxo, precisa selecionar o papel autorizador nos papéis de assinatura.<b>',
        placement: 'right',
        animation: 'scale',
        allowHTML: true,
        trigger: 'mouseenter click',
    });
    tippy('#infoGroupWitness', {
        content: '<b>Para selecionar um ou mais setor(es) para testemunhar o fluxo, precisa selecionar o papel autorizador nos papéis de assinatura.<b>',
        placement: 'right',
        animation: 'scale',
        allowHTML: true,
        trigger: 'mouseenter click',
    });
    tippy('#infoIdentifier', {
        content: '<b>Nome para identificação do seu documento.<b>',
        placement: 'right',
        animation: 'scale',
        allowHTML: true,
        trigger: 'mouseenter click',
    });
    tippy('#infoTypeDocument', {
        content: '<b>Escolha o tipo referente ao seu documento.<b>',
        placement: 'right',
        animation: 'scale',
        allowHTML: true,
        trigger: 'mouseenter click',
    });
    tippy('#infoSecuritylevel', {
        content: '<b>Escolha qual vai ser o nível de segurança mínimo do(s) participante(s) para assinar o documento.<b>',
        placement: 'right',
        animation: 'scale',
        allowHTML: true,
        trigger: 'mouseenter click',
    });
}

function addPaperDocuments() {
    tippy('#infoNameDocuments', {
        content: '<b>Documento que ocorrerá a(s) assinatura(s).<b>',
        placement: 'right',
        animation: 'scale',
        allowHTML: true,
        trigger: 'mouseenter click',
    });
    tippy('#infoOrderDocuments', {
        content: '<b>Defina para qual documento esses dados pertecem.<b>',
        placement: 'right',
        animation: 'scale',
        allowHTML: true,
        trigger: 'mouseenter click',
    });
    tippy('#infoSecuritylevel', {
        content: `<b>Define o nível para os participantes assinarem o documento.<br><br>
                     NÍVEL PRATA: Basta se cadastrar em nossa plataforma e validar os dados por e-mail ou celular. <br><br>
                     NÍVEL OURO: Upload da foto da face com o documento do lado. (obs: Documento com foto). <br><br>
                     NÍVEL PLATINUM: Upload do comprovante de endereço.<b>`,
        placement: 'right',
        animation: 'scale',
        allowHTML: true,
    });
    tippy('#infoPaperDocuments', {
        content: '<b>Papel de acordo com o fluxo.<b>',
        placement: 'right',
        animation: 'scale',
        allowHTML: true,
        trigger: 'mouseenter click',
    });
    tippy('#infoContactDocuments', {
        content: '<b>Tipo de contato para notificar o participante do fluxo, se for CPF enviaremos um email, caso seja cadastrado.<b>',
        placement: 'right',
        animation: 'scale',
        allowHTML: true,
        trigger: 'mouseenter click',
    });
    tippy('#infoPaymentFlowBatch', {
        content: '<b>Para cada documento gerado informe um pagante.<br> Escolha um contato para adicionar o pagante.<b>',
        placement: 'right',
        animation: 'scale',
        allowHTML: true,
        trigger: 'mouseenter click',
    });
    tippy('#infoFormValidate', {
        content: '<b>Meio ao qual enviaremos o código de validação (tipos: email, sms ou app) para assinar o documento.<b>',
        placement: 'right',
        animation: 'scale',
        allowHTML: true,
        trigger: 'mouseenter click',
    });
    tippy('#infoDeadlineDocuments', {
        content: '<b>Data limite para assinar o documento, caso não defina uma data o processo ocorrerar de acordo com o fluxo.<b>',
        placement: 'right',
        animation: 'scale',
        allowHTML: true,
        trigger: 'mouseenter click',
    });
}

function shoppingCart() {
    tippy('#infoShoppingCart', {
        content: '<b>Aqui você pode visualizar o seu carrinho de compras com os seus serviços pendentes para pagamento.<b>',
        placement: 'right',
        animation: 'scale',
        allowHTML: true,
        trigger: 'mouseenter click',
    });
}

function headerBar() {
    tippy('#shopping-cart', {
        content: '<b>Carrinho de Compras<b>',
        placement: 'bottom',
        animation: 'scale',
        allowHTML: true,
        trigger: 'mouseenter',
    });

    tippy('#notification-btn', {
        content: '<b>Notificações<b>',
        placement: 'bottom',
        animation: 'scale',
        allowHTML: true,
        trigger: 'mouseenter',
    });
}

function dropdownHeaderBar() {
    tippy('#profile-user', {
        content: '<b>Informações do Perfil<b>',
        placement: 'left',
        animation: 'scale',
        allowHTML: true,
        trigger: 'mouseenter',
    });

    tippy('#upgrade-level2', {
        content: '<b>Faça uma atualização de nível<b>',
        placement: 'left',
        animation: 'scale',
        allowHTML: true,
        trigger: 'mouseenter',
    });

    tippy('#solicitation-free', {
        content: '<b>Visualize a quantidade de solicitações gratuitas que você possui<b>',
        placement: 'left',
        animation: 'scale',
        allowHTML: true,
        trigger: 'mouseenter',
    });

    tippy('#user-perfis', {
        content: '<b>Faça uma troca entre seus perfis<b>',
        placement: 'left',
        animation: 'scale',
        allowHTML: true,
        trigger: 'mouseenter',
    });

    tippy('#change-password', {
        content: '<b>Altere sua senha<b>',
        placement: 'left',
        animation: 'scale',
        allowHTML: true,
        trigger: 'mouseenter',
    });

    tippy('#credit-account', {
        content: '<b>Créditos da conta<b>',
        placement: 'left',
        animation: 'scale',
        allowHTML: true,
        trigger: 'mouseenter',
    });

    tippy('#exit-account', {
        content: '<b>Sair da conta<b>',
        placement: 'left',
        animation: 'scale',
        allowHTML: true,
        trigger: 'mouseenter',
    });
}

function levelSecurity() {
    tippy('.level-security', {
        content: '<b>Nível de segurança<b>',
        placement: 'left',
        animation: 'scale',
        allowHTML: true,
        trigger: 'mouseenter',
    });
}

function reopenFlows() {
    tippy('#infoReopenFlow', {
        content: '<b>Listagem dos fluxos cancelados disponiveis para serem abertos novamente.<b>',
        placement: 'right',
        animation: 'scale',
        allowHTML: true,
        trigger: 'mouseenter click',
    });
}

function secondFactor() {
    tippy('#infoSecondFactor', {
        content: '<b>Ao habiliatar segurança de segundo fator,<br>o login será em duas etapas.<br><br>1° - Informe usuário e senha<br><br>2° - Informe o código gerado no aplicativo ou enviado por email.<b>',
        placement: 'right',
        animation: 'scale',
        allowHTML: true,
        trigger: 'mouseenter click',
    });
}