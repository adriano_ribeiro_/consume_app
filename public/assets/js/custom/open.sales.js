let openSalesTable = null,
    isModalShow = false,
    STATUS_OPEN = 1;

$(()=> {
    getOpenFinishSales();
});

function getOpenFinishSales() {
    openSalesTable = $('#open-sales-table').DataTable({
        "ajax": {
            "url": uri.saleItem.getOpenFinishSales(STATUS_OPEN),
            "beforeSend": function (xhr) {
                // xhr.setRequestHeader("Authorization", "Bearer " + token);
                loadingDataTable();
            }
        },
        "columns": [
            { "data":"amount"},
            { "data":"updated_at"},
            { "data":"id" }
        ],
        "columnDefs": [
            {
                "targets": 0,
                'render': function (data, type, full, meta) {
                    return maskMoneyPTBR(data);
                }
            },
            {
                "targets": 1,
                'render': function (data, type, full, meta) {
                    return data ? moment(data).utcOffset('GMT-3:00').format('DD/MM/YYYY HH:mm:ss') : "";
                }
            },
            {
                "render": function (data) {
                    return `<button onClick="openSale(${data})" class="btn btn-info btn-sm scroll-click"><i class="icon-copy fa fa-folder-open" aria-hidden="true"></i> Abrir</button>`
                },
                "targets": 2,
                "searchable": false
            }
        ],
        'scrollCollapse': true,
        "serverSide": true,
        "processing": true,
        // "dom": 'lrtip',
        "lengthMenu": [[10, 20, 30], [10, 20, 30]],
        "language": {
            'url': URL_DATATABLE_LANGUAGE,
            paginate: {
                next: '<i class="ion-chevron-right"></i>',
                previous: '<i class="ion-chevron-left"></i>'
            }
        },
        'order': [[0, 'asc']]
    });
}

function openSale(data) {
    localStorage.setItem('sale_id', data);
    window.location.href = "/nova-venda"
}