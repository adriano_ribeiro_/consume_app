const uri = {

    user: {
        auth: '/api/v1/user/auth',
        logout: '/api/v1/user/logout',
        create: '/api/v1/user/create',
        all: '/api/v1/user/all',
        find: function (id) {
            return `/api/v1/user/find/${id}`;
        },
        update: function (id) {
            return `/api/v1/user/update/${id}`;
        },
    },

    client: {
        create: '/api/v1/client/create',
        all: '/api/v1/client/all',
        find: function (id) {
            return `/api/v1/client/find/${id}`;
        },
        update: function (id) {
            return `/api/v1/client/update/${id}`;
        },
        findByStatus: function (status) {
            return `/api/v1/client/find/status/${status}`;
        },
        findByName: '/api/v1/client/get/name'
    },
    
    category: {
        create: '/api/v1/category/create',
        all: '/api/v1/category/all',
        find: function (id) {
            return `/api/v1/category/find/${id}`;
        },
        update: function (id) {
            return `/api/v1/category/update/${id}`;
        },
        findByStatus: function (status) {
            return `/api/v1/category/find/status/${status}`;
        },
    },

    provider: {
        create: '/api/v1/provider/create',
        all: '/api/v1/provider/all',
        findAll: '/api/v1/provider/findAll',
        find: function (id) {
            return `/api/v1/provider/find/${id}`;
        },
        update: function (id) {
            return `/api/v1/provider/update/${id}`;
        },
        findByStatus: function (status) {
            return `/api/v1/provider/find/status/${status}`;
        },
    },

    product: {
        create: '/api/v1/product/create',
        all: '/api/v1/product/all',
        find: function (id) {
            return `/api/v1/product/find/${id}`;
        },
        update: function (id) {
            return `/api/v1/product/update/${id}`;
        },
        barcodeName: '/api/v1/product/barcode-name'
    },

    saleItem: {
        create: '/api/v1/sale-item/create',
        current: function (saleId) {
            return `/api/v1/sale-item/find/sale/${saleId}`;
        },
        remove: '/api/v1/sale-item/sale-remove-item',
        finish: '/api/v1/sale-item/finish-sale',
        countSaleOpenFinish: '/api/v1/sale-item/sale-open-finish',
        all: '',
        getOpenFinishSales: function (status) {
            return `/api/v1/sale-item/open-finish-sales/${status}`;
        },
    }
};