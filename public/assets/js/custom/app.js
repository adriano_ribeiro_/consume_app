//? 1 para vendas em aberto e 0 para vendas finalizadas
function countSaleOpenFinish() {
    $.ajax({
        type: 'GET',
        url: uri.saleItem.countSaleOpenFinish,
        dataType: 'json',
        headers: {
            "content-type": "application/json;charset=UTF-8"
        },
        // beforeSend: function (xhr) {
            // showLoading('Por favor aguarde', 'Carregando os dados da categoria. . .');
            // xhr.setRequestHeader(`Authorization', 'Bearer ${token}`);
        // },
        success: function (response) {
            $('.count-sale-open').text(response.data.count_open);
            $('.count-sale-finish').text(response.data.count_finish);
        },
        error: function (data) {
            swal.close();
            if (data.status === HTTP_INTERNAL_SERVER_ERROR) {
                messageError("Ocorreu uma inconsistência no sistema, se persistir contate o suporte.");
            } else if (data.status === HTTP_UNAUTHORIZED && data.responseJSON.message === "Expired JWT Token") {
                swal(
                    {
                        title: 'Informação',
                        text: 'Sua sessão expirou faça o login novamente.',
                        type: 'info',
                        allowOutsideClick: false
                    }
                ).then((result) => {
                    if (result.value) {
                        window.location.href = "/logout";
                    }
                });
            } else if (data.responseJSON.message) {
                messageError(data.responseJSON.message);
            } else {
                messageError("Ocorreu uma inconsistência no sistema, se persistir contate o suporte.");
            }
        }
    });
}

function logout() {
    $.ajax({
        type: 'GET',
        url: uri.user.logout,
        dataType: 'json',
        headers: {
            "content-type": "application/json;charset=UTF-8"
        },
        beforeSend: function (xhr) {
            showLoading('Por favor aguarde', 'Saindo. . .');
            // xhr.setRequestHeader(`Authorization', 'Bearer ${token}`);
        },
        success: function (response) {
            window.location.href = "/";
        },
        error: function (data) {
            swal.close();
            if (data.status === HTTP_INTERNAL_SERVER_ERROR) {
                messageError("Ocorreu uma inconsistência no sistema, se persistir contate o suporte.");
            } else if (data.status === HTTP_UNAUTHORIZED && data.responseJSON.message === "Expired JWT Token") {
                swal(
                    {
                        title: 'Informação',
                        text: 'Sua sessão expirou faça o login novamente.',
                        type: 'info',
                        allowOutsideClick: false
                    }
                ).then((result) => {
                    if (result.value) {
                        window.location.href = "/logout";
                    }
                });
            } else if (data.responseJSON.message) {
                messageError(data.responseJSON.message);
            } else {
                messageError("Ocorreu uma inconsistência no sistema, se persistir contate o suporte.");
            }
        }
    });
}

function addBorderRed(element) {
    $(element).attr("style", "border: 1px solid #dc3545");
}

function removeBorder(element) {
    $(element).removeAttr("style");
}

function removeBorderOnFocus(element) {
    $(element).on('focus', () => {
        $(element).removeAttr("style");
    });
}

function validaEmail(email) {
    let regex = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    return regex.test(email);
}

function calculatePercentage(amount, percentage) {
    amount = realToNumber(amount);
    return parseFloat(((percentage / 100) * amount).toFixed(2));
}

function calculatePercentageDifference(amount, valueWithIncrease) {
    amount = realToNumber(amount);
    valueWithIncrease = realToNumber(valueWithIncrease);
    return (((valueWithIncrease - amount)/amount)*100).toFixed(2).replace(",", ".");
}

function maskMoneyPTBR(value) {
    return `R$ ${parseFloat(value).formatMoney(2, ',', '.')}`;
}

function maskMoney(element) {
    let amount = $(element);
    let amountFormatted = `R$ ${parseFloat(amount.text()).formatMoney(2, ',', '.')}`;
    amount.text(amountFormatted);
}

function realToNumber(value) {
    var amount = value.replace("R$ ", "");
    if (amount === "") {
        return 0;
    } else {
        amount = amount.replace(".", "");
        amount = amount.replace(",", ".");
        return parseFloat(amount);
    }
}

/**
 ** verifica se o campo é vazio
 *
 * @param value
 */
 function isEmpty(value) {
    value = value ?? "";
    const isEmptyInput = str => !str.trim().length;
    return isEmptyInput(value);
}

/**
 ** converte data
 *
 * @param value
 * @return {number}
 */
 function convertDate(value) {
    value = !value ? null : value.replaceAll("/", "-");
    return value;
}

/**
 ** converte valor para pt-BR
 *
 * @param value
 * @return {number}
 */
function convertCoinToFloat(value) {
    if (!value) {
        value = null;
    } else {
        value = value.replace(".", "");
        value = value.replace(",", ".");
        value = parseFloat(value);
    }
    return value;
}

Number.prototype.formatMoney = function (c, d, t) {
    var n = this, c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "," : d, t = t == undefined ? "." : t,
        s = n < 0 ? "-" : "", i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};

function maskMoneyInput() {
    Number.prototype.formatMoney = function (c, d, t) {
        var n = this, c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "," : d,
            t = t == undefined ? "." : t, s = n < 0 ? "-" : "", i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
            j = (j = i.length) > 3 ? j % 3 : 0;
        return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
    };
    $(".money").maskMoney({
        prefix: "R$ ",
        decimal: ",",
        thousands: "."
    });
}

//* pattern => '00.000.000/0000-00'
function addMask(element, pattern) {
    $(element).mask(pattern)
}

//* mask('85988279902', (##) #.####-####)
function mask(value, pattern) {
    let i = 0;
    const v = value.toString();
    return pattern.replace(/#/g, () => v[i++] || '');
}

function removeMask(element) {
    return $(element).val().replace(/[^0-9]/gi, '');
}

function select2Multiple(element, placeholder, modalParent = "") {
    $(element).select2({
        dropdownAutoWidth: false,
        multiple: true,
        placeholder: placeholder,
        allowClear: true,
        dropdownParent: modalParent
    });

    // Ajusta largura do placeholder no campo
    $('.select2-search--inline').css('width', '100%');
    $('.select2-search__field').css('width', '100%');
}

function select2Single(element, placeholder, modalParent = "") {
    $(element).select2({
        language: "pt-BR",
        dropdownAutoWidth: false,
        multiple: false,
        placeholder: placeholder,
        allowClear: true,
        dropdownParent: modalParent
    });

    // Ajusta largura do placeholder no campo
    $('.select2-search--inline').css('width', '100%');
    $('.select2-search__field').css('width', '100%');
}

function select2SingleSearch(element, uri, placeholder, modalParent = "") {
    $(element).select2({
        language: "pt-BR",
        dropdownAutoWidth: false,
        allowClear: true,
        placeholder: placeholder,
        dropdownParent: modalParent,
        ajax: {
            url: uri,
            dataType: 'json',
            delay: 250,
            processResults: function (response) {
                var param = $.map(response.data, function (obj) {
                    obj.id = obj.id;
                    obj.text = obj.name;

                    return obj;
                });

                return {
                    results: param,
                    pagination: {
                        more: false
                    }
                };
            },
            cache: true
        }
    });
    // Ajusta largura do placeholder no campo
    $('.select2-search--inline').css('width', '100%');
    $('.select2-search__field').css('width', '100%');
}

function loadingDataTable(table = ".dataTables") {
    let divProcessing = `${table}_processing`;
    let html = `<div style="display: flex; align-items: center;">
                    <div class="loader" style="margin-right: 1rem; width: 3rem; height: 3rem;"></div>
                    Processando...
                </div>`;

    $(divProcessing).attr("style", "display: block; width: auto; padding: 2rem;");
    $(divProcessing).html(html);
}

// $(".datepicker").datepicker({
//     format: "dd/mm/yyyy",
//     startDate: '-3d',
//     isRTL: false,
//     autoclose: true,
//     language: 'pt-BR'
// });

// $(".ano").datepicker({
//     format: "yyyy",
//     viewMode: "years",
//     minViewMode: "years",
//     isRTL: false,
//     autoclose: true,
//     language: 'pt-BR'
// });

/**
 * formato: DD-MM-YYYY
 * @param date 
 * @return boolean
 */
function validDate(date) {
    let day = [
        '01', '1', '02', '2', '03', '3', '04', '4', '05', '5',
        '06', '6', '07', '7', '08', '8', '09', '9', '10', '11',
        '12', '13', '14', '15', '16', '17', '18', '19', '20',
        '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31'
    ],
        month = [
            '01', '1', '02', '2', '03', '3', '04', '4', '05', '5', '06', '6',
            '07', '7', '08', '8', '09', '9', '10', '11', '12'
    ];       

    date = date.replaceAll("/", "-");
    let value = date.split("-");
    if(value[0].length === 0 || value[0].length > 2 || value[0] === '0' || value[0] === '00' || day.indexOf(value[0]) == -1) {
        return false;
    } else if(value[1].length === 0 || value[1].length > 2 || month.indexOf(value[1]) == -1) {
        return false;
    } else if(value[2].length !== 4 || value[2] === "0000") {
        return false;
    }
    return true;
}

function validateCpfCnpj(val) {
    if (val.length == 14) {
        var cpf = val.trim();

        cpf = cpf.replace(/\./g, '');
        cpf = cpf.replace('-', '');
        cpf = cpf.split('');

        var v1 = 0;
        var v2 = 0;
        var aux = false;

        for (var i = 1; cpf.length > i; i++) {
            if (cpf[i - 1] != cpf[i]) {
                aux = true;
            }
        }

        if (aux == false) {
            return false;
        }

        for (var i = 0, p = 10; (cpf.length - 2) > i; i++, p--) {
            v1 += cpf[i] * p;
        }

        v1 = ((v1 * 10) % 11);

        if (v1 == 10) {
            v1 = 0;
        }

        if (v1 != cpf[9]) {
            return false;
        }

        for (var i = 0, p = 11; (cpf.length - 1) > i; i++, p--) {
            v2 += cpf[i] * p;
        }

        v2 = ((v2 * 10) % 11);

        if (v2 == 10) {
            v2 = 0;
        }

        if (v2 != cpf[10]) {
            return false;
        } else {
            return true;
        }
    } else if (val.length == 18) {
        var cnpj = val.trim();

        cnpj = cnpj.replace(/\./g, '');
        cnpj = cnpj.replace('-', '');
        cnpj = cnpj.replace('/', '');
        cnpj = cnpj.split('');

        var v1 = 0;
        var v2 = 0;
        var aux = false;

        for (var i = 1; cnpj.length > i; i++) {
            if (cnpj[i - 1] != cnpj[i]) {
                aux = true;
            }
        }

        if (aux == false) {
            return false;
        }

        for (var i = 0, p1 = 5, p2 = 13; (cnpj.length - 2) > i; i++, p1--, p2--) {
            if (p1 >= 2) {
                v1 += cnpj[i] * p1;
            } else {
                v1 += cnpj[i] * p2;
            }
        }

        v1 = (v1 % 11);

        if (v1 < 2) {
            v1 = 0;
        } else {
            v1 = (11 - v1);
        }

        if (v1 != cnpj[12]) {
            return false;
        }

        for (var i = 0, p1 = 6, p2 = 14; (cnpj.length - 1) > i; i++, p1--, p2--) {
            if (p1 >= 2) {
                v2 += cnpj[i] * p1;
            } else {
                v2 += cnpj[i] * p2;
            }
        }

        v2 = (v2 % 11);

        if (v2 < 2) {
            v2 = 0;
        } else {
            v2 = (11 - v2);
        }

        if (v2 != cnpj[13]) {
            return false;
        } else {
            return true;
        }
    } else {
        return false;
    }
}