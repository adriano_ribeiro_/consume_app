/**
 * Apresenta um loader utilizando o sweet alert
 *
 * @param message
 * @param texto
 */
function showLoading(message, text) {

    swal({
        title: message,
        text: text,
        allowOutsideClick: false,
        onBeforeOpen: function () {
            swal.showLoading();
        }
    });
}

function swalSuccess(title) {
    swal(
        {
            position: 'center',
            type: 'success',
            title: title,
            showConfirmButton: false,
            timer: 2000
        }
    )
}

/**
 * @param message
 */
function messageSuccess(message) {
    toastr.success(message, 'Sucesso');
}

/**
 * @param message
 */
function messageInfo(message) {
    toastr.info(message, 'Informação');
}

/**
 * @param message
 */
function messageWarning(message) {
    toastr.warning(message, 'Atenção');
}

/**
 * @param message
 */
function messageError(message) {
    toastr.error(message, 'Atenção');
}