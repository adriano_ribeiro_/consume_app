
let userTable = null,
    isModalShow = false;

$(()=> {
    removeStyle();
    userAll();
    $(document).on('keypress', function(e) {
        // e.preventDefault();
        if(e.which == 13 && isModalShow) {
            e.preventDefault();
            createOrUpdate();
        }
    });
});

function userAll(){
    userTable = $('#users-table').DataTable({
        "ajax": {
            "url": uri.user.all,
            "beforeSend": function (xhr) {
                // xhr.setRequestHeader("Authorization", "Bearer " + token);
                loadingDataTable('users-table');
            }
        },
        "columns": [
            { "data":"person.name" },
            { "data":"person.email" },
            { "data":"login" },
            { "data":"id" }
        ],
        "columnDefs": [
            {
                "render": function (data) {
                    return `<button onClick="modalCreateOrUpdate(${data})" class="btn btn-info btn-sm scroll-click"><i class="icon-copy fa fa-pencil" aria-hidden="true"></i> Alterar</button>`
                },
                "targets": 3,
                "searchable": false
            }
        ],
        'scrollCollapse': true,
            // "serverSide": true,
            // "processing": true,
            "dom": 'lrtip',
            "lengthMenu": [[10, 20, 30], [10, 20, 30]],
            "language": {
                'url': URL_DATATABLE_LANGUAGE,
                paginate: {
                    next: '<i class="ion-chevron-right"></i>',
                    previous: '<i class="ion-chevron-left"></i>'
                }
            },
        'order': [[0, 'asc']]
    });
}

function modalCreateOrUpdate(data) {
    if(!data) {
        $('#user-create-update').trigger("reset");
        $('.title-form-user').text('Cadastro de usuários');
        $('.create-update').text('Salvar');
        $('.password').removeAttr('hidden');
        $('#modal-create-or-update').modal({ backdrop: 'static', keyboard: true, show: true });
    } else {
        details(data);
    }
    isModalShow = true;
    $('#name').trigger('focus');
}

function createOrUpdate() {
    if(!$('#user_id').val()) {
        create();
    } else {
        update($('#user_id').val());
    }
}

function create() {
    let data = {
        'name': $.trim($('#name').val()),
        'email': $.trim($('#email').val()),
        'login': $.trim($('#login').val()),
        'password': $.trim($('#password').val()),
        'confirm_password': $.trim($('#confirm_password').val()),
        'status': true
    };

    let isValid = true;

    if(!data.name) {
        addBorderRed('#name');
        messageError('Informe o nome do usuário');
        isValid = false;
    }

    if(!data.email) {
        addBorderRed('#email');
        messageError('Informe o email do usuário');
        isValid = false;
    }

    if(data.email && !validaEmail(data.email)) {
        addBorderRed('#email');
        messageError('Informe um email válido');
        isValid = false;
    }

    if(!data.login) {
        addBorderRed('#login');
        messageError('Informe o login do usuário');
        isValid = false;
    }

    if(!data.password) {
        addBorderRed('#password');
        messageError('Informe a senha do usuário');
        isValid = false;
    }

    if(!data.confirm_password) {
        addBorderRed('#confirm_password');
        messageError('Confirme a senha do usuário');
        isValid = false;
    }

    if(data.password && data.confirm_password && data.password !== data.confirm_password) {
        addBorderRed('#password');
        addBorderRed('#confirm_password');
        messageError('Senhas não conferem');
        isValid = false;
    }

    if(!isValid) {
        return false;
    }

    $.ajax({
        type: 'POST',
        url: uri.user.create,
        data: JSON.stringify(data),
        dataType: 'json',
        headers: {
            "content-type": "application/json;charset=UTF-8"
        },
        beforeSend: function (xhr) {
            showLoading('Por favor aguarde', 'Cadastrando usuário. . .');
            // xhr.setRequestHeader(`Authorization', 'Bearer ${token}`);
        },
        success: function (response) {
            swal.close();
            $('#user-create-update').trigger("reset");
            userTable.ajax.reload();
            messageSuccess("Usuário cadastrado com sucesso!");
        },
        error: function (data) {
            swal.close();
            if (data.status === HTTP_INTERNAL_SERVER_ERROR) {
                messageError("Ocorreu uma inconsistência no sistema ao tentar cadastrar um usuário.");
            } else if (data.status === HTTP_UNAUTHORIZED && data.responseJSON.message === "Expired JWT Token") {
                swal(
                    {
                        title: 'Informação',
                        text: 'Sua sessão expirou faça o login novamente.',
                        type: 'info',
                        allowOutsideClick: false
                    }
                ).then((result) => {
                    if (result.value) {
                        window.location.href = "/logout";
                    }
                });
            } else if (data.responseJSON.message) {
                messageError(data.responseJSON.message);
            } else {
                messageError("Ocorreu uma inconsistência no sistema ao tentar cadastrar um usuário.");
            }
        }
    });
}

function update(id) {
    let data = {
        'name': $.trim($('#name').val()),
        'email': $.trim($('#email').val()),
        'login': $.trim($('#login').val()),
        'status': true
    };

    let isValid = true;

    if(!data.name) {
        addBorderRed('#name');
        messageError('Informe o nome do usuário');
        isValid = false;
    }

    if(!data.email) {
        addBorderRed('#email');
        messageError('Informe o email do usuário');
        isValid = false;
    }

    if(data.email && !validaEmail(data.email)) {
        addBorderRed('#email');
        messageError('Informe um email válido');
        isValid = false;
    }

    if(!data.login) {
        addBorderRed('#login');
        messageError('Informe o login do usuário');
        isValid = false;
    }

    if(!isValid) {
        return false;
    }

    $.ajax({
        type: 'POST',
        url: uri.user.update(id),
        data: JSON.stringify(data),
        dataType: 'json',
        headers: {
            "content-type": "application/json;charset=UTF-8"
        },
        beforeSend: function (xhr) {
            showLoading('Por favor aguarde', 'Atualizando os dados do usuário. . .');
            // xhr.setRequestHeader(`Authorization', 'Bearer ${token}`);
        },
        success: function (response) {
            swal.close();
            $('#user_id').val(null);
            $('#user-create-update').trigger("reset");
            userTable.ajax.reload();
            $('#modal-create-or-update').modal('hide');
            isModalShow = false;
            messageSuccess("Dados alterado com sucesso!");
        },
        error: function (data) {
            swal.close();
            if (data.status === HTTP_INTERNAL_SERVER_ERROR) {
                messageError("Ocorreu uma inconsistência no sistema ao tentar alterar um usuário.");
            } else if (data.status === HTTP_UNAUTHORIZED && data.responseJSON.message === "Expired JWT Token") {
                swal(
                    {
                        title: 'INFORMAÇÃO',
                        text: 'Sua sessão expirou faça o login novamente.',
                        type: 'info',
                        allowOutsideClick: false
                    }
                ).then((result) => {
                    if (result.value) {
                        window.location.href = "/logout";
                    }
                });
            } else if (data.responseJSON.message) {
                messageError(data.responseJSON.message);
            } else {
                messageError("Ocorreu uma inconsistência no sistema ao tentar alterar um usuário.");
            }
        }
    });
}

function details(id) {
    $.ajax({
        type: 'GET',
        url: uri.user.find(id),
        dataType: 'json',
        headers: {
            "content-type": "application/json;charset=UTF-8"
        },
        beforeSend: function (xhr) {
            showLoading('Por favor aguarde', 'Carregando os dados do usuário. . .');
            // xhr.setRequestHeader(`Authorization', 'Bearer ${token}`);
        },
        success: function (response) {
            swal.close();
            $('#user-create-update').trigger("reset");
            $('#user_id').val(response.data.id);
            $('#name').val(response.data.person.name);
            $('#email').val(response.data.person.email);
            $('#login').val(response.data.login);
            $('.password').attr("hidden", "hidden");
            $('.title-form-user').text('Atualizar dados do usuarios');
            $('.create-update').text('Atualizar');
            $('#modal-create-or-update').modal({ backdrop: 'static', keyboard: true, show: true });
        },
        error: function (data) {
            swal.close();
            if (data.status === HTTP_INTERNAL_SERVER_ERROR) {
                messageError("Ocorreu uma inconsistência no sistema, se persistir contate o suporte.");
            } else if (data.status === HTTP_UNAUTHORIZED && data.responseJSON.message === "Expired JWT Token") {
                swal(
                    {
                        title: 'Informação',
                        text: 'Sua sessão expirou faça o login novamente.',
                        type: 'info',
                        allowOutsideClick: false
                    }
                ).then((result) => {
                    if (result.value) {
                        window.location.href = "/logout";
                    }
                });
            } else if (data.responseJSON.message) {
                messageError(data.responseJSON.message);
            } else {
                messageError("Ocorreu uma inconsistência no sistema, se persistir contate o suporte.");
            }
        }
    });
}

function removeStyle() {
    removeBorderOnFocus('#name');
    removeBorderOnFocus('#email');
    removeBorderOnFocus('#login');
    removeBorderOnFocus('#password');
    removeBorderOnFocus('#confirm_password');
}

function modalClose() {
    isModalShow = false;
    $('#user_id').val(null);
    removeBorder('#name');
    removeBorder('#email');
    removeBorder('#login');
    removeBorder('#password');
    removeBorder('#confirm_password');
}