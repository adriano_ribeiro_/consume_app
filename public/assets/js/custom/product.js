let productTable = null,
    isModalShow = false,
    isEventInput = 0; // inicializa com zero


$(()=> {
    maskMoneyInput();
    addMask('#purchase_date', '00/00/0000');
    removeStyle();
    productAll();
    selectCategoriesActive();
    selectProvidersActive();
    calculatePrice();
    onFocusCalculatePrice();
    $(document).on('keypress', function(e) {
        // e.preventDefault();
        if(e.which == 13 && isModalShow) {
            e.preventDefault();
            createOrUpdate();
        }
    });
});

function productAll() {
    productTable = $('#products-table').DataTable({
        "ajax": {
            "url": uri.product.all,
            "beforeSend": function (xhr) {
                // xhr.setRequestHeader("Authorization", "Bearer " + token);
                loadingDataTable('products-table');
            }
        },
        "columns": [
            // {"data": "barcode"},
            {"data": "name"},
            {"data": "provider_name"},
            {"data": "category_name"},
            // {"data": "purchase_price"},
            // {"data": "percentage_wholesale"},
            // {"data": "wholesale_price"},
            // {"data": "percentage_retail"},
            {"data": "retail_price"},
            {"data": "quantity"},
            // {"data": "purchase_date"},
            {"data": "id"}
        ],
        "columnDefs": [
            {
                "targets": 3,
                'render': function (data, type, full, meta) {
                    return maskMoneyPTBR(data);
                }
            },
            // {
            //     "targets": 6,
            //     'render': function (data, type, full, meta) {
            //         return maskMoneyPTBR(data);
            //     }
            // },
            // {
            //     "targets": 8,
            //     'render': function (data, type, full, meta) {
            //         return maskMoneyPTBR(data);
            //     }
            // },
            // {
            //     "targets": 5,
            //     'render': function (data, type, full, meta) {
            //         return data ? moment(data).utcOffset('GMT-3:00').format('DD/MM/YYYY') : "";
            //     }
            // },
            {
                "render": function (data) {
                    return `<button onClick="modalDetails(${data})" class="btn btn-secondary btn-sm scroll-click"><i class="icon-copy fa fa-info-circle" aria-hidden="true"></i> Detalhes</button>
                            <button onClick="modalCreateOrUpdate(${data})" class="btn btn-info btn-sm scroll-click"><i class="icon-copy fa fa-pencil" aria-hidden="true"></i> Alterar</button>`
                },
                "targets": 5,
                "searchable": false
            }
        ],
        'scrollCollapse': true,
            // "serverSide": true,
            // "processing": true,
            "dom": 'lrtip',
            "lengthMenu": [[10, 20, 30], [10, 20, 30]],
            "language": {
                'url': URL_DATATABLE_LANGUAGE,
                paginate: {
                    next: '<i class="ion-chevron-right"></i>',
                    previous: '<i class="ion-chevron-left"></i>'
                }
            },
        'order': [[0, 'asc']]
    });
}

function modalDetails(data) {
    details(data, true);
}

function modalCreateOrUpdate(data) {
    if(!data) {
        $('#product-create-update').trigger("reset");
        $('.title-form-product').text('Cadastro de produtos');
        $('.create-update').text('Salvar');
        $('#modal-create-or-update').modal({ backdrop: 'static', keyboard: true, show: true });
    } else {
        details(data);
    }
    isModalShow = true;
    $('#barcode').trigger('focus');
}

function createOrUpdate() {
    if(!$('#product_id').val()) {
        create();
    } else {
        update($('#product_id').val());
    }
}

function validateData() {
    let data = {
        'barcode': $.trim($('#barcode').val()),
        'name': $.trim($('#name').val()),
        'provider_id': $.trim($('#provider_id').val()),
        'category_id': $.trim($('#category_id').val()),
        'purchase_price': realToNumber($.trim($('#purchase_price').val())),
        'percentage_wholesale': $.trim($('#percentage_wholesale').val()),
        'wholesale_price': realToNumber($.trim($('#wholesale_price').val())),
        'percentage_retail': $.trim($('#percentage_retail').val()),
        'retail_price': realToNumber($.trim($('#retail_price').val())),
        'quantity': $.trim($('#quantity').val()),
        'purchase_date': $.trim(convertDate($('#purchase_date').val())),
        'status': true
    };

    let isValid = true;

    if(!data.barcode) {
        addBorderRed('#barcode');
        messageError('Informe o código barras do produto');
        isValid = false;
    }

    if(!data.name) {
        addBorderRed('#name');
        messageError('Informe o nome do produto');
        isValid = false;
    }

    if(!data.provider_id) {
        addBorderRed('#provider_id');
        messageError('Informe o fornecedor do produto');
        isValid = false;
    }

    if(!data.category_id) {
        addBorderRed('#category_id');
        messageError('Informe a categoria do produto');
        isValid = false;
    }

    if(!data.purchase_price) {
        addBorderRed('#purchase_price');
        messageError('Informe o preço de compra do produto');
        isValid = false;
    }

    if(!data.percentage_wholesale) {
        addBorderRed('#percentage_wholesale');
        messageError('Informe a porcentagem do valor de atacado');
        isValid = false;
    }

    if(!data.wholesale_price) {
        addBorderRed('#wholesale_price');
        messageError('Informe o preço de atacado');
        isValid = false;
    }

    if(!data.percentage_retail) {
        addBorderRed('#percentage_retail');
        messageError('Informe a porcentagem do valor de varejo');
        isValid = false;
    }

    if(!data.retail_price) {
        addBorderRed('#retail_price');
        messageError('Informe o preço de varejo');
        isValid = false;
    }

    if(!data.quantity) {
        addBorderRed('#quantity');
        messageError('Informe a quantidade do produto');
        isValid = false;
    }

    if(!data.purchase_date) {
        addBorderRed('#purchase_date');
        messageError('Informe a data de compra do produto');
        isValid = false;
    } else if (!validDate(data.purchase_date)) {
        addBorderRed('#purchase_date');
        messageError('Informe a data válida');
        isValid = false;
    }

    return !isValid ? isValid : data;
}

function create() {
    let data = validateData();

    if(!data) {
        return false;
    }

    $.ajax({
        type: 'POST',
        url: uri.product.create,
        data: JSON.stringify(data),
        dataType: 'json',
        headers: {
            "content-type": "application/json;charset=UTF-8"
        },
        beforeSend: function (xhr) {
            showLoading('Por favor aguarde', 'Cadastrando produto. . .');
            // xhr.setRequestHeader(`Authorization', 'Bearer ${token}`);
        },
        success: function (response) {
            swal.close();
            productTable.ajax.reload();
            $('#product-create-update').trigger("reset");
            messageSuccess("Produto cadastrado com sucesso!");
        },
        error: function (data) {
            swal.close();
            if (data.status === HTTP_INTERNAL_SERVER_ERROR) {
                messageError("Ocorreu uma inconsistência no sistema, se persistir contate o suporte.");
            } else if (data.status === HTTP_UNAUTHORIZED && data.responseJSON.message === "Expired JWT Token") {
                swal(
                    {
                        title: 'Informação',
                        text: 'Sua sessão expirou faça o login novamente.',
                        type: 'info',
                        allowOutsideClick: false
                    }
                ).then((result) => {
                    if (result.value) {
                        window.location.href = "/logout";
                    }
                });
            } else if (data.status === HTTP_BAD_REQUEST && data.responseJSON && data.responseJSON.message) {
                var posts = data.responseJSON.message.split(".");
                posts.forEach((value, key) => {
                    $.trim(value) ? messageError(`${value}.`) : null;
                });
            } else {
                messageError("Ocorreu uma inconsistência no sistema, se persistir contate o suporte.");
            }
        }
    });
}

function update(id) {
    let data = validateData();

    if(!data) {
        return false;
    }

    $.ajax({
        type: 'POST',
        url: uri.product.update(id),
        data: JSON.stringify(data),
        dataType: 'json',
        headers: {
            "content-type": "application/json;charset=UTF-8"
        },
        beforeSend: function (xhr) {
            showLoading('Por favor aguarde', 'Atualizando produto. . .');
            // xhr.setRequestHeader(`Authorization', 'Bearer ${token}`);
        },
        success: function (response) {
            swal.close();
            $('#product_id').val(null);
            $('#product-create-update').trigger("reset");
            productTable.ajax.reload();
            $('#modal-create-or-update').modal('hide');
            isModalShow = false;
            messageSuccess("Produto alterado com sucesso!");
        },
        error: function (data) {
            swal.close();
            if (data.status === HTTP_INTERNAL_SERVER_ERROR) {
                messageError("Ocorreu uma inconsistência no sistema, se persistir contate o suporte.");
            } else if (data.status === HTTP_UNAUTHORIZED && data.responseJSON.message === "Expired JWT Token") {
                swal(
                    {
                        title: 'INFORMAÇÃO',
                        text: 'Sua sessão expirou faça o login novamente.',
                        type: 'info',
                        allowOutsideClick: false
                    }
                ).then((result) => {
                    if (result.value) {
                        window.location.href = "/logout";
                    }
                });
            } else if (data.responseJSON.message) {
                messageError(data.responseJSON.message);
            } else {
                messageError("Ocorreu uma inconsistência no sistema, se persistir contate o suporte.");
            }
        }
    });
}

function details(id, isDetails = false) {
    $.ajax({
        type: 'GET',
        url: uri.product.find(id),
        dataType: 'json',
        headers: {
            "content-type": "application/json;charset=UTF-8"
        },
        beforeSend: function (xhr) {
            showLoading('Por favor aguarde', 'carregando os dados do produto');
            // xhr.setRequestHeader(`Authorization', 'Bearer ${token}`);
        },
        success: function (response) {
            swal.close();
            $('#product-create-update').trigger("reset");
            $('#product_id').val(response.data.id);
            $('#barcode').val(response.data.barcode);
            $('#name').val(response.data.name);
            $('#provider_id').val(response.data.provider_id);
            $('#category_id').val(response.data.category_id);
            $('#purchase_price').val(maskMoneyPTBR(response.data.purchase_price));
            $('#percentage_wholesale').val(response.data.percentage_wholesale);
            $('#wholesale_price').val(maskMoneyPTBR(response.data.wholesale_price));
            $('#percentage_retail').val(response.data.percentage_retail);
            $('#retail_price').val(maskMoneyPTBR(response.data.retail_price));
            $('#quantity').val(response.data.quantity);
            $('#purchase_date').val(moment(response.data.purchase_date).utcOffset('GMT-3:00').format('DD/MM/YYYY'));
            if(isDetails) {
                $('.title-form-product').text('Detalhes do produto');
                $('.details').attr('hidden', true);
                $('#modal-create-or-update').modal({ backdrop: 'static', keyboard: true, show: true });
            } else {
                $('.details').attr('hidden', false);
                $('.title-form-product').text('Atualizar dados do produto');
                $('.create-update').text('Atualizar');
                $('#modal-create-or-update').modal({ backdrop: 'static', keyboard: true, show: true });
            }
        },
        error: function (data) {
            swal.close();
            if (data.status === HTTP_INTERNAL_SERVER_ERROR) {
                messageError("Ocorreu uma inconsistência no sistema, se persistir contate o suporte.");
            } else if (data.status === HTTP_UNAUTHORIZED && data.responseJSON.message === "Expired JWT Token") {
                swal(
                    {
                        title: 'Informação',
                        text: 'Sua sessão expirou faça o login novamente.',
                        type: 'info',
                        allowOutsideClick: false
                    }
                ).then((result) => {
                    if (result.value) {
                        window.location.href = "/logout";
                    }
                });
            } else if (data.responseJSON.message) {
                messageError(data.responseJSON.message);
            } else {
                messageError("Ocorreu uma inconsistência no sistema, se persistir contate o suporte.");
            }
        }
    });
}

function selectCategoriesActive() {
    $.ajax({
        type: 'GET',
        url: uri.category.findByStatus(STATUS_ACTIVE),
        dataType: 'json',
        headers: {
            "content-type": "application/json;charset=UTF-8"
        },
        beforeSend: function (xhr) {
            showLoading('Por favor aguarde', 'Carregando as categorias. . .');
            // xhr.setRequestHeader(`Authorization', 'Bearer ${token}`);
        },
        success: function (response) {
            swal.close();
            $('#category_id').html('');
            let options = `<option value="" selected>Selecione a categoria</option>`;
            
            response.data.forEach((value, key) => {
                options += `<option value="${value.id}">${value.name}</option>`;
            });
            
            $('#category_id').append(options);
        },
        error: function (data) {
            swal.close();
            if (data.status === HTTP_INTERNAL_SERVER_ERROR) {
                messageError("Ocorreu uma inconsistência no sistema, se persistir contate o suporte.");
            } else if (data.status === HTTP_UNAUTHORIZED && data.responseJSON.message === "Expired JWT Token") {
                swal(
                    {
                        title: 'Informação',
                        text: 'Sua sessão expirou faça o login novamente.',
                        type: 'info',
                        allowOutsideClick: false
                    }
                ).then((result) => {
                    if (result.value) {
                        window.location.href = "/logout";
                    }
                });
            } else if (data.responseJSON.message) {
                messageError(data.responseJSON.message);
            } else {
                messageError("Ocorreu uma inconsistência no sistema, se persistir contate o suporte.");
            }
        }
    });
}

function selectProvidersActive() {
    $.ajax({
        type: 'GET',
        url: uri.provider.findByStatus(STATUS_ACTIVE),
        dataType: 'json',
        headers: {
            "content-type": "application/json;charset=UTF-8"
        },
        beforeSend: function (xhr) {
            showLoading('Por favor aguarde', 'Carregando os Fornecedores. . .');
            // xhr.setRequestHeader(`Authorization', 'Bearer ${token}`);
        },
        success: function (response) {
            swal.close();
            $('#provider_id').html('');
            let options = `<option value="" selected>Selecione o fornecedor</option>`;
            
            response.data.forEach((value, key) => {
                options += `<option value="${value.id}">${value.name}</option>`;
            });
            
            $('#provider_id').append(options);
        },
        error: function (data) {
            swal.close();
            if (data.status === HTTP_INTERNAL_SERVER_ERROR) {
                messageError("Ocorreu uma inconsistência no sistema, se persistir contate o suporte.");
            } else if (data.status === HTTP_UNAUTHORIZED && data.responseJSON.message === "Expired JWT Token") {
                swal(
                    {
                        title: 'Informação',
                        text: 'Sua sessão expirou faça o login novamente.',
                        type: 'info',
                        allowOutsideClick: false
                    }
                ).then((result) => {
                    if (result.value) {
                        window.location.href = "/logout";
                    }
                });
            } else if (data.responseJSON.message) {
                messageError(data.responseJSON.message);
            } else {
                messageError("Ocorreu uma inconsistência no sistema, se persistir contate o suporte.");
            }
        }
    });
}

function removeStyle() {
    removeBorderOnFocus('#barcode');
    removeBorderOnFocus('#name');
    removeBorderOnFocus('#provider_id');
    removeBorderOnFocus('#category_id');
    // removeBorderOnFocus('#purchase_price');
    // removeBorderOnFocus('#percentage_wholesale');
    // removeBorderOnFocus('#wholesale_price');
    // removeBorderOnFocus('#percentage_retail');
    // removeBorderOnFocus('#retail_price');
    removeBorderOnFocus('#quantity');
    removeBorderOnFocus('#purchase_date');
}

function modalClose() {
    isModalShow = false;
    $('#product_id').val(null);
    removeBorder('#barcode');
    removeBorder('#name');
    removeBorder('#provider_id');
    removeBorder('#category_id');
    removeBorder('#purchase_price');
    removeBorder('#percentage_wholesale');
    removeBorder('#wholesale_price');
    removeBorder('#percentage_retail');
    removeBorder('#retail_price');
    removeBorder('#quantity');
    removeBorder('#purchase_date');
}

function onFocusCalculatePrice() {
    $('#purchase_price').on('focus', () => {
        isEventInput = 1; 
        $('#purchase_price').removeAttr("style");
    });

    $('#percentage_wholesale').on('focus', () => {
        isEventInput = 2; 
        $('#percentage_wholesale').removeAttr("style");
    });

    $('#wholesale_price').on('focus', () => {
        isEventInput = 3; 
        $('#wholesale_price').removeAttr("style");
    });

    $('#percentage_retail').on('focus', () => {
        isEventInput = 4; 
        $('#percentage_retail').removeAttr("style");
    });

    $('#retail_price').on('focus', () => {
        isEventInput = 5; 
        $('#retail_price').removeAttr("style");
    });
}

function calculatePrice() {
    $('.calculate_price').on('keyup', function(e) {
        //? Preço no atacado
        if(isEventInput === 1) {
            let valuePercentWholesale = calculatePercentage($('#purchase_price').val(), $('#percentage_wholesale').val());
            let wholesalePrice = realToNumber($('#purchase_price').val());
            wholesalePrice = (wholesalePrice + valuePercentWholesale).toFixed(2);
            $('#wholesale_price').val(maskMoneyPTBR(wholesalePrice));

            //? Preço no varejo
            let valuePercentRetailPrice = calculatePercentage($('#wholesale_price').val(), $('#percentage_retail').val());
            let retailPrice = realToNumber($('#wholesale_price').val());
            retailPrice = (retailPrice + valuePercentRetailPrice).toFixed(2);
            $('#retail_price').val(maskMoneyPTBR(retailPrice));
        }

        // if(isEventInput === 2) {
        //     if(!$('#purchase_price').val()) {
        //         addBorderRed('#purchase_price');
        //         messageError('Informe o preço de compra do produto');
        //     } else {
        //         let valuePercentWholesale = calculatePercentage($('#purchase_price').val(), $('#percentage_wholesale').val());
        //         let wholesalePrice = realToNumber($('#purchase_price').val());
        //         wholesalePrice = (wholesalePrice + valuePercentWholesale).toFixed(2);
        //         $('#wholesale_price').val(maskMoneyPTBR(wholesalePrice));
        //     }
        // }

        // if(isEventInput === 3) {
        //     if(!$('#purchase_price').val()) {
        //         addBorderRed('#purchase_price');
        //         messageError('Informe o preço de compra do produto');
        //     } else {
        //         let purchasePrice = $('#purchase_price').val();
        //         let wholesalePrice = $('#wholesale_price').val();
        //         $('#percentage_wholesale').val(calculatePercentageDifference(purchasePrice, wholesalePrice));
        //     }
        // }

        // if(isEventInput === 4) {
        //     if(!$('#purchase_price').val()) {
        //         addBorderRed('#purchase_price');
        //         messageError('Informe o preço de compra do produto');
        //     } else {
        //         let purchasePrice = $('#purchase_price').val();
        //         let wholesalePrice = $('#wholesale_price').val();
        //         $('#percentage_retail').val(calculatePercentageDifference(purchasePrice, wholesalePrice));
        //     }
        // }

        // if(isEventInput === 5) {
        //     if(!$('#purchase_price').val()) {
        //         addBorderRed('#purchase_price');
        //         messageError('Informe o preço de compra do produto');
        //     } else {
        //         let purchasePrice = $('#purchase_price').val();
        //         let retailPrice = $('#retail_price').val();
        //         $('#percentage_retail').val(calculatePercentageDifference(purchasePrice, retailPrice));
        //     }
        // }
    });
}