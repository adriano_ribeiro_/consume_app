
let categoryTable = null,
    isModalShow = false;

$(()=> {
    removeStyle();
    categoriesAll();
    $(document).on('keypress', function(e) {
        e.preventDefault();
        if(e.which == 13 && isModalShow) {
            e.preventDefault();
            createOrUpdate();
        }
    });
});

function categoriesAll(){
    categoryTable = $('#categories-table').DataTable({
        "ajax": {
            "url": uri.category.all,
            "beforeSend": function (xhr) {
                // xhr.setRequestHeader("Authorization", "Bearer " + token);
                loadingDataTable();
            }
        },
        "columns": [
            { "data":"name" },
            { "data":"id" }
        ],
        "columnDefs": [
            {
                "render": function (data) {
                    return `<button onClick="modalCreateOrUpdate(${data})" class="btn btn-info btn-sm scroll-click"><i class="icon-copy fa fa-pencil" aria-hidden="true"></i> Alterar</button>`
                },
                "targets": 1,
                "searchable": false
            }
        ],
        'scrollCollapse': true,
        "serverSide": true,
        "processing": true,
        // "dom": 'lrtip',
        "lengthMenu": [[10, 20, 30], [10, 20, 30]],
        "language": {
            'url': URL_DATATABLE_LANGUAGE,
            paginate: {
                next: '<i class="ion-chevron-right"></i>',
                previous: '<i class="ion-chevron-left"></i>'
            }
        },
        'order': [[0, 'asc']]
    });
}

function modalCreateOrUpdate(data) {
    if(!data) {
        $('#category-create-update').trigger("reset");
        $('.title-form-category').text('Cadastro de categorias');
        $('.create-update').text('Salvar');
        $('#modal-create-or-update').modal({ backdrop: 'static', keyboard: true, show: true });
    } else {
        details(data);
    }
    $('#name').trigger("focus");
    isModalShow = true;
}

function createOrUpdate() {
    if(!$('#category_id').val()) {
        create();
    } else {
        update($('#category_id').val());
    }
}

function create() {
    let data = {
        'name': $.trim($('#name').val()),
        'status': true
    };

    if(!data.name) {
        addBorderRed('#name');
        messageError('Informe o nome da categoria');
        return false;
    }

    $.ajax({
        type: 'POST',
        url: uri.category.create,
        data: JSON.stringify(data),
        dataType: 'json',
        headers: {
            "content-type": "application/json;charset=UTF-8"
        },
        beforeSend: function (xhr) {
            showLoading('Por favor aguarde', 'Cadastrando categoria. . .');
            // xhr.setRequestHeader(`Authorization', 'Bearer ${token}`);
        },
        success: function (response) {
            swal.close();
            $('#category-create-update').trigger("reset");
            categoryTable.ajax.reload();
            messageSuccess("Categoria cadastrada com sucesso!");
        },
        error: function (data) {
            swal.close();
            if (data.status === HTTP_INTERNAL_SERVER_ERROR) {
                messageError("Ocorreu uma inconsistência no sistema ao tentar cadastrar uma categoria.");
            } else if (data.status === HTTP_UNAUTHORIZED && data.responseJSON.message === "Expired JWT Token") {
                swal(
                    {
                        title: 'Informação',
                        text: 'Sua sessão expirou faça o login novamente.',
                        type: 'info',
                        allowOutsideClick: false
                    }
                ).then((result) => {
                    if (result.value) {
                        window.location.href = "/logout";
                    }
                });
            } else if (data.responseJSON.message) {
                messageError(data.responseJSON.message);
            } else {
                messageError("Ocorreu uma inconsistência no sistema ao tentar cadastrar uma categoria.");
            }
        }
    });
}

function update(id) {
    let data = {
        'name': $.trim($('#name').val()),
        'status': true
    };

    if(!data.name) {
        addBorderRed('#name');
        messageError('Informe o nome da categoria');
        return false;
    }

    $.ajax({
        type: 'POST',
        url: uri.category.update(id),
        data: JSON.stringify(data),
        dataType: 'json',
        headers: {
            "content-type": "application/json;charset=UTF-8"
        },
        beforeSend: function (xhr) {
            showLoading('Por favor aguarde', 'Atualizando os dados da categoria. . .');
            // xhr.setRequestHeader(`Authorization', 'Bearer ${token}`);
        },
        success: function (response) {
            swal.close();
            $('#category_id').val(null);
            $('#category-create-update').trigger("reset");
            categoryTable.ajax.reload();
            $('#modal-create-or-update').modal('hide');
            isModalShow = false;
            messageSuccess("Categoria alterada com sucesso!");
        },
        error: function (data) {
            swal.close();
            if (data.status === HTTP_INTERNAL_SERVER_ERROR) {
                messageError("Ocorreu uma inconsistência no sistema ao tentar alterar uma categoria.");
            } else if (data.status === HTTP_UNAUTHORIZED && data.responseJSON.message === "Expired JWT Token") {
                swal(
                    {
                        title: 'Informação',
                        text: 'Sua sessão expirou faça o login novamente.',
                        type: 'info',
                        allowOutsideClick: false
                    }
                ).then((result) => {
                    if (result.value) {
                        window.location.href = "/logout";
                    }
                });
            } else if (data.responseJSON.message) {
                messageError(data.responseJSON.message);
            } else {
                messageError("Ocorreu uma inconsistência no sistema ao tentar alterar uma categoria.");
            }
        }
    });
}

function details(id) {
    $.ajax({
        type: 'GET',
        url: uri.category.find(id),
        dataType: 'json',
        headers: {
            "content-type": "application/json;charset=UTF-8"
        },
        beforeSend: function (xhr) {
            showLoading('Por favor aguarde', 'Carregando os dados da categoria. . .');
            // xhr.setRequestHeader(`Authorization', 'Bearer ${token}`);
        },
        success: function (response) {
            swal.close();
            $('#category-create-update').trigger("reset");
            $('#category_id').val(response.data.id);
            $('#name').val(response.data.name);
            $('.title-form-category').text('Atualizar dados da categoria');
            $('.create-update').text('Atualizar');
            $('#modal-create-or-update').modal({ backdrop: 'static', keyboard: true, show: true });
        },
        error: function (data) {
            swal.close();
            if (data.status === HTTP_INTERNAL_SERVER_ERROR) {
                messageError("Ocorreu uma inconsistência no sistema, se persistir contate o suporte.");
            } else if (data.status === HTTP_UNAUTHORIZED && data.responseJSON.message === "Expired JWT Token") {
                swal(
                    {
                        title: 'Informação',
                        text: 'Sua sessão expirou faça o login novamente.',
                        type: 'info',
                        allowOutsideClick: false
                    }
                ).then((result) => {
                    if (result.value) {
                        window.location.href = "/logout";
                    }
                });
            } else if (data.responseJSON.message) {
                messageError(data.responseJSON.message);
            } else {
                messageError("Ocorreu uma inconsistência no sistema, se persistir contate o suporte.");
            }
        }
    });
}

function removeStyle() {
    removeBorderOnFocus('#name');
}

function modalClose() {
    isModalShow = false
    $('#category_id').val(null);
    removeBorder('#name');
}