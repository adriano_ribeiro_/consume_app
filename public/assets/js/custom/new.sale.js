
let saleItemTable = null,
    isModalShow = true;

$(()=> {
    tableItemSale();
    barcodeOrName();
    getClient();
    removeStyles();
    calculateDiscount();
    changeBarcodeOrProduct();
    changeIsDiscount();

    $(document).on('keypress', function(e) {
        // e.preventDefault();
        if(e.which == 13 && isModalShow) {
            e.preventDefault();
            box();
        }
    });
    // para versão do jquery 3.6
    // $(document).on('select2:open', () => {
    //     document.querySelector('.select2-search__field').focus();
    // });
    // Ajusta largura do placeholder no campo
    onFocusSelect2();
    // onFocusSelect2Client();
});

function onFocusSelect2() {
    setTimeout(() => {
        $('#barcode-product').select2('open');
        $('#barcode-product').val(null).trigger('change');
    }, 100); 
}

// function onFocusSelect2Client() {
//     setTimeout(() => {
//         $('#client_id').select2('open');
//         $('#client_id').val(null).trigger('change');
//     }, 100); 
// }

function tableItemSale(saleId = 0) {
    if(saleId === 0 && localStorage.getItem('sale_id')) {
        saleId = localStorage.getItem('sale_id');
        $('#sale_id').val(saleId);
        localStorage.setItem('sale_id', '');
    }
    saleItemTable = $('#sale-table').DataTable({
        "ajax": {
            "url": uri.saleItem.current(saleId),
            "beforeSend": function (xhr) {
                // xhr.setRequestHeader("Authorization", "Bearer " + token);
                loadingDataTable();
            }
        },
        "columns": [
            { "data":"barcode" },
            { "data":"product" },
            { "data":"unit_price" },
            { "data":"quantity" },
            { "data":"subtotal" },
            { "data":"sale_id" }
        ],
        "columnDefs": [
            {
                "targets": 2,
                'render': function (data, type, full, meta) {
                    return maskMoneyPTBR(data);
                }
            },
            {
                "targets": 4,
                'render': function (data, type, full, meta) {
                    full.amount 
                    ? $('.amount').html(maskMoneyPTBR(full.amount.toString()))
                    : $('.amount').html('R$ 0,00');
                    return maskMoneyPTBR(data);
                }
            },
            {
                "render": function (data, type, full, meta) {
                    return `<button onClick="modalRemoveItems()" data-items="${window.btoa(JSON.stringify(full))}" class="btn btn-danger btn-sm scroll-click remove-items"><i class="fa fa-trash"></i> Remover</button>`
                },
                "targets": 5,
                "searchable": false
            }
        ],
        'scrollCollapse': true,
        "dom": 'lrtip',
        "language": {
            'url': URL_DATATABLE_LANGUAGE,
            paginate: {
                next: '<i class="ion-chevron-right"></i>',
                previous: '<i class="ion-chevron-left"></i>'
            }
        },
        'autoWidth': true,
        "paging":   false,
        "ordering": false,
        "info":     false,
        'order': [[0, 'desc']]
    });
}

function barcodeOrName() {
    $(function () {
        $('#barcode-product').select2({
            language: "pt-BR",
            dropdownAutoWidth: false,
            minimumInputLength: 4,
            width: 'resolve',
            placeholder: '',
            allowClear: true,
            ajax: {
                url: uri.product.barcodeName,
                dataType: 'json',
                delay: 100,
                processResults: function (response) {
                    var data = $.map(response.data, function (obj) {
                        obj.id = obj.id;
                        obj.text = `${obj.name} - Qtd: ${obj.quantity}`;

                        return obj;
                    });
                    if(data.length === 1) {

                    }
                    return {
                        results: data,
                        pagination: {
                            more: false
                        }
                    };
                },
                cache: true
            }
        });
    });

    // Ajusta largura do placeholder no campo
    $('.select2-search--inline').css('width', '100%');
    $('.select2-search__field').css('width', '100%');
}

function newSale() {
    saleItemTable.clear().destroy();
    $('.amount').html('R$ 0,00');
    tableItemSale(0);
    $('#sale_id').val(null);
    setTimeout(() => {
        $('#barcode-product').select2('open');
        $('#barcode-product').val(null).trigger('change');
    }, 100); 
}

function box() {
    let data = {
        'sale_id': $.trim($('#sale_id').val()),
        'product_id': $.trim($('#barcode-product').val()),
        'quantity': $.trim($('#quantity').val()),
        'is-attacked': $('#is-attacked').is(':checked'),
        'status': true
    };

    let isValid = true;

    if(!data.product_id) {
        $('.select2-selection').css('border-color','#dc3545');
        messageError('Informe um produto');
        isValid = false;
    }
    if(!data.quantity || data.quantity == 0) {
        addBorderRed('#quantity');
        messageError('Informe a quantidade');
        isValid = false;
    }
    if(!isValid) {
        return false;
    }

    $.ajax({
        type: 'POST',
        url: uri.saleItem.create,
        data: JSON.stringify(data),
        dataType: 'json',
        headers: {
            "content-type": "application/json;charset=UTF-8"
        },
        beforeSend: function (xhr) {
            showLoading('Por favor aguarde', 'Adicionando item. . .');
            // xhr.setRequestHeader(`Authorization', 'Bearer ${token}`);
        },
        success: function (response) {
            swal.close();
            saleItemTable.clear().destroy();
            tableItemSale(response.data.sale_id);
            $('#sale_id').val(response.data.sale_id);
            $('#quantity').val(1);
            $('.amount').text(maskMoneyPTBR(response.data.amount));
            countSaleOpenFinish();
            onFocusSelect2();
            messageSuccess("Item adicionado com sucesso!");
        },
        error: function (data) {
            swal.close();
            if (data.status === HTTP_INTERNAL_SERVER_ERROR) {
                messageError("Ocorreu uma inconsistência no sistema, se persistir contate o suporte.");
            } else if (data.status === HTTP_UNAUTHORIZED && data.responseJSON.message === "Expired JWT Token") {
                swal(
                    {
                        title: 'Informação',
                        text: 'Sua sessão expirou faça o login novamente.',
                        type: 'info',
                        allowOutsideClick: false
                    }
                ).then((result) => {
                    if (result.value) {
                        window.location.href = "/logout";
                    }
                });
            } else if (data.responseJSON && data.responseJSON.message) {
                setTimeout(() => {
                    $('#barcode-product').select2('open');
                }, 100); 
                messageError(data.responseJSON.message);
            } else {
                setTimeout(() => {
                    $('#barcode-product').select2('open');
                }, 100); 
                messageError("Ocorreu uma inconsistência no sistema, se persistir contate o suporte.");
            }
        }
    });
}

function details(id) {
    $.ajax({
        type: 'GET',
        url: uri.category.find(id),
        dataType: 'json',
        headers: {
            "content-type": "application/json;charset=UTF-8"
        },
        beforeSend: function (xhr) {
            showLoading('AGUARDE', 'CARREGANDO DADOS DA CATEGORIA');
            // xhr.setRequestHeader(`Authorization', 'Bearer ${token}`);
        },
        success: function (response) {
            swal.close();
            $('#sale-item-create-update').trigger("reset");
            $('#category_id').val(response.data.id);
            $('#name').val(response.data.name);
            $('.title-form-category').text('ATUALIZAR DADOS DA CATEGORIA');
            $('.create-update').text('ATUALIZAR');
            $('#modal-create-or-update').modal({ backdrop: 'static', keyboard: true, show: true });
            isModalShow = false;
        },
        error: function (data) {
            swal.close();
            if (data.status === HTTP_INTERNAL_SERVER_ERROR) {
                messageError("Ocorreu uma inconsistência no sistema, se persistir contate o suporte.");
            } else if (data.status === HTTP_UNAUTHORIZED && data.responseJSON.message === "Expired JWT Token") {
                swal(
                    {
                        title: 'Informação',
                        text: 'Sua sessão expirou faça o login novamente.',
                        type: 'info',
                        allowOutsideClick: false
                    }
                ).then((result) => {
                    if (result.value) {
                        window.location.href = "/logout";
                    }
                });
            } else if (data.responseJSON.message) {
                messageError(data.responseJSON.message);
            } else {
                messageError("Ocorreu uma inconsistência no sistema, se persistir contate o suporte.");
            }
        }
    });
}

function changeBarcodeOrProduct() {
    $('#barcode-product').on('change', function(e) {
        e.preventDefault();
        if($(this).val()) {
            box();
        }
    });
}

function changeIsDiscount() {
    $('#is_discount').on('change', function(e) {
        e.preventDefault();
        if($(this).val() == '0') {
            $('.discount').attr('hidden', true);
        } else {
            $('.discount').attr('hidden', false);
        }
    });
}

function modalRemoveItems() {
    saleItemTable.on('click', '.remove-items', function (e) {
        e.preventDefault();
        let item = JSON.parse(atob($(this).data("items")));
        $('#item_id').val(item.item_id);
        $('#items').val(item.quantity);
        $('#unit_price').val(item.unit_price);
        $('#quantity-items').val(item.quantity);
        $('#modal-remove-item').modal({ backdrop: 'static', keyboard: true, show: true });
    });
}

function removeItem() {
    let data = {
        'item_id': $('#item_id').val(),
        'quantity': $('#quantity-items').val(),
    };
    $.ajax({
        method: "POST",
        url: uri.saleItem.remove,
        data: JSON.stringify(data),
        dataType: 'JSON',
        headers: {
            "content-type": "application/json;charset=UTF-8"
        },
        beforeSend: function (xhr) {
            showLoading('Por favor aguarde', 'Removendo item. . .');
            // xhr.setRequestHeader('Authorization', `Bearer ${token}`);
        },
        success: function (response) {
            swal.close();
            saleItemTable.clear().destroy();
            tableItemSale(response.data.sale_id);
            $('#sale_id').val(response.data.sale_id);
            $('#quantity').val(1);
            $('.amount').text(maskMoneyPTBR(response.data.amount));
            countSaleOpenFinish();
            $('#modal-remove-item').modal('hide');
        },
        error: function (result, textStatus, errorThrown) {
            swal.close();

            swal({
                title: 'Atenção',
                text: 'Motivo: ' + result.responseJSON.message,
                type: 'error',
            })
        }
    });
}

function removeStyles() {
    removeBorderOnFocus('#quantity');
    removeBorderOnFocus('#discount');

    $('#barcode-product').on('change', function() {
        $('.select2-selection').css('border-color','#c9c9c9');
    });
}

function getClient() {
    $(function () {
        $('#client_id').select2({
            language: "pt-BR",
            dropdownAutoWidth: false,
            minimumInputLength: 3,
            width: 'resolve',
            placeholder: '',
            allowClear: true,
            dropdownParent: '#modal-finish-sale',
            ajax: {
                url: uri.client.findByName,
                dataType: 'json',
                delay: 100,
                processResults: function (response) {
                    var data = $.map(response.data, function (obj) {
                        obj.id = obj.id;
                        obj.text = `${obj.name}`;

                        return obj;
                    });
                    if(data.length === 1) {

                    }
                    return {
                        results: data,
                        pagination: {
                            more: false
                        }
                    };
                },
                cache: true
            }
        });
    });

    // Ajusta largura do placeholder no campo
    // $('.select2-search--inline').css('width', '100%');
    // $('.select2-search__field').css('width', '100%');
}

function modalClose() {
    removeBorder('#discount');
    $('#is_discount').val(0);
    $('#discount').val(null);
    $('.discount').attr('hidden', true);
    $('#amount').val(null);
}

function modalFinishSale() {
    if(saleItemTable && saleItemTable.rows().data().toArray() <= 0) {
        messageInfo('Não existe itens na venda para serem finalizados');
        return false;
    }
    $('#amount').val($('.amount').text());
    $('#modal-finish-sale').modal({ backdrop: 'static', keyboard: true, show: true });
}

function calculateDiscount() {
    $('#discount').on('input', function(e) {
        let valuePercent = calculatePercentage($('.amount').text(), $('#discount').val());
        let amount = realToNumber($('.amount').text());
        amount = (amount - valuePercent).toFixed(2);
        $('#amount').val(maskMoneyPTBR(amount));
    });
}

function finishSale() {
    let data = {
        'sale_id': $.trim($('#sale_id').val()),
        'client_id': $.trim($('#client_id').val()) ? parseInt($('#client_id').val()) : null,
        'sale_value': realToNumber($('#amount').val()),
        'status': false
    };

    if($('#is_discount').val() == 1 && !$.trim($('#discount').val())) { // 1 => sim
        addBorderRed('#discount');
        messageError('Informe quantos porcento deseja dar de desconto');
        return false;
    } 

    $.ajax({
        type: 'POST',
        url: uri.saleItem.finish,
        data: JSON.stringify(data),
        dataType: 'json',
        headers: {
            "content-type": "application/json;charset=UTF-8"
        },
        beforeSend: function (xhr) {
            showLoading('Por favor aguarde', 'Finalizando a venda. . .');
            // xhr.setRequestHeader(`Authorization', 'Bearer ${token}`);
        },
        success: function (response) {
            swal.close();
            $('.amount').text('R$ 0,00');
            $('#modal-finish-sale').modal('hide');
            messageSuccess("Venda finalizada com sucesso!");
            saleItemTable.clear().destroy();
            tableItemSale(0);
            $('#sale_id').val(null);
            $('#is_discount').val(0);
            $('#discount').val(null);
            $('.discount').attr('hidden', true);
            $('#amount').val(null);
            countSaleOpenFinish();
        },
        error: function (data) {
            swal.close();
            if (data.status === HTTP_INTERNAL_SERVER_ERROR) {
                messageError("Ocorreu uma inconsistência ao tentar finalizar a venda.");
            } else if (data.status === HTTP_UNAUTHORIZED && data.responseJSON.message === "Expired JWT Token") {
                swal(
                    {
                        title: 'INFORMAÇÃO',
                        text: 'Sua sessão expirou faça o login novamente.',
                        type: 'info',
                        allowOutsideClick: false
                    }
                ).then((result) => {
                    if (result.value) {
                        window.location.href = "/logout";
                    }
                });
            } else if (data.responseJSON.message) {
                messageError(data.responseJSON.message);
            } else {
                messageError("Ocorreu uma inconsistência ao tentar finalizar a venda.");
            }
        }
    });
}