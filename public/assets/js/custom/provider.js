let providerTable = null,
    isModalShow = false;

$(()=> {
    addMask('#cnpj', '00.000.000/0000-00');
    addMask('#phone', '(00) 0.0000-0000');
    providerAll();
    removeStyle();
    $(document).on('keypress', function(e) {
        // e.preventDefault();
        if(e.which == 13 && isModalShow) {
            e.preventDefault();
            createOrUpdate();
        }
    });
});

function providerAll() {
    providerTable = $('#providers-table').DataTable({
        "ajax": {
            "url": uri.provider.all,
            "beforeSend": function (xhr) {
                // xhr.setRequestHeader("Authorization", "Bearer " + token);
                loadingDataTable('providers-table');
            }
        },
        "columns": [
            { "data":"cnpj" },
            { "data":"name" },
            { "data":"phone" },
            { "data":"id" }
        ],
        "columnDefs": [
            {
                "targets": 0,
                'render': function (data, type, full, meta) {
                    var formatter = new StringMask("00.000.000/0000-00", { reverse: true });
                    return  formatter.apply(data);
                }
            },
            {
                "targets": 2,
                'render': function (data, type, full, meta) {
                    var formatter = new StringMask("(00) 0.0000-0000", { reverse: true });
                    return  formatter.apply(data);
                }
            },
            {
                "render": function (data) {
                    return `<button onClick="modalCreateOrUpdate(${data})" class="btn btn-info btn-sm scroll-click"><i class="icon-copy fa fa-pencil" aria-hidden="true"></i> Alterar</button>`
                },
                "targets": 3,
                "searchable": false
            }
        ],
        'scrollCollapse': true,
            // "serverSide": true,
            // "processing": true,
            "dom": 'lrtip',
            "lengthMenu": [[10, 20, 30], [10, 20, 30]],
            "language": {
                'url': URL_DATATABLE_LANGUAGE,
                paginate: {
                    next: '<i class="ion-chevron-right"></i>',
                    previous: '<i class="ion-chevron-left"></i>'
                }
            },
        'order': [[0, 'asc']]
    });
}

function modalCreateOrUpdate(data) {
    if(!data) {
        $('#provider-create-update').trigger("reset");
        $('.title-form-provider').text('Cadastro de fornecedores');
        $('.create-update').text('Salvar');
        $('#modal-create-or-update').modal({ backdrop: 'static', keyboard: true, show: true });
    } else {
        details(data);
    }
    isModalShow = true;
    $('#name').trigger('focus');
}

function createOrUpdate() {
    if(!$('#provider_id').val()) {
        create();
    } else {
        update($('#provider_id').val());
    }
}

function create() {
    let cnpj = $('#cnpj').val();
    let data = {
        'name': $.trim($('#name').val()),
        'cnpj': $.trim(removeMask($('#cnpj'))),
        'phone': $.trim(removeMask($('#phone'))),
        'status': true
    };

    let isValid = true;

    if(!data.name) {
        addBorderRed('#name');
        messageError('Infome o nome do fornecedor');
        isValid = false;
    }
    if(!data.cnpj) {
        addBorderRed('#cnpj');
        messageError('Informe o CNPJ do fornecedor');
        isValid = false;
    }

    if(data.cnpj && !validateCpfCnpj(cnpj)) {
        addBorderRed('#cnpj');
        messageError('Informe um CNPJ válido');
        isValid = false;
    }
    // if(!data.phone) {
    //     addBorderRed('#phone');
    //     messageError('Informe o celular');
    //     isValid = false;
    // }
    if(!isValid) {
        return false;
    }

    $.ajax({
        type: 'POST',
        url: uri.provider.create,
        data: JSON.stringify(data),
        dataType: 'json',
        headers: {
            "content-type": "application/json;charset=UTF-8"
        },
        beforeSend: function (xhr) {
            showLoading('Por favor aguarde', 'Cadastrado fornecedor. . .');
            // xhr.setRequestHeader(`Authorization', 'Bearer ${token}`);
        },
        success: function (response) {
            swal.close();
            $('#provider-create-update').trigger("reset");
            providerTable.ajax.reload();
            messageSuccess("Fornecedor cadastrado com sucesso!");
        },
        error: function (data) {
            swal.close();
            if (data.status === HTTP_INTERNAL_SERVER_ERROR) {
                messageError("Ocorreu uma inconsistência ao tentar cadastrar um fornecedor.");
            } else if (data.status === HTTP_UNAUTHORIZED && data.responseJSON.message === "Expired JWT Token") {
                swal(
                    {
                        title: 'Informação',
                        text: 'Sua sessão expirou faça o login novamente.',
                        type: 'info',
                        allowOutsideClick: false
                    }
                ).then((result) => {
                    if (result.value) {
                        window.location.href = "/logout";
                    }
                });
            } else if (data.responseJSON.message) {
                messageError(data.responseJSON.message);
            } else {
                messageError("Ocorreu uma inconsistência ao tentar cadastrar um fornecedor.");
            }
        }
    });
}

function update(id) {
    let cnpj = $('#cnpj').val();
    let data = {
        'name': $.trim($('#name').val()),
        'cnpj': $.trim(removeMask($('#cnpj'))),
        'phone': $.trim(removeMask($('#phone'))),
        'status': true
    };

    let isValid = true;

    if(!data.name) {
        addBorderRed('#name');
        messageError('Infome o nome do fornecedor');
        isValid = false;
    }
    if(!data.cnpj) {
        addBorderRed('#cnpj');
        messageError('Informe o CNPJ do fornecedor');
        isValid = false;
    }

    if(data.cnpj && !validateCpfCnpj(cnpj)) {
        addBorderRed('#cnpj');
        messageError('Informe um CNPJ válido');
        isValid = false;
    }
    if(!isValid) {
        return false;
    }

    $.ajax({
        type: 'POST',
        url: uri.provider.update(id),
        data: JSON.stringify(data),
        dataType: 'json',
        headers: {
            "content-type": "application/json;charset=UTF-8"
        },
        beforeSend: function (xhr) {
            showLoading('Por favor aguarde', 'Atualizando os dados do fornecedor');
            // xhr.setRequestHeader(`Authorization', 'Bearer ${token}`);
        },
        success: function (response) {
            swal.close();
            $('#provider_id').val(null);
            $('#provider-create-update').trigger("reset");
            providerTable.ajax.reload();
            $('#modal-create-or-update').modal('hide');
            isModalShow = false;
            messageSuccess("Dados alterado com sucesso!");
        },
        error: function (data) {
            swal.close();
            if (data.status === HTTP_INTERNAL_SERVER_ERROR) {
                messageError("Ocorreu uma inconsistência no sistema ao tentar alterar os dados do fornecedor.");
            } else if (data.status === HTTP_UNAUTHORIZED && data.responseJSON.message === "Expired JWT Token") {
                swal(
                    {
                        title: 'Informação',
                        text: 'Sua sessão expirou faça o login novamente.',
                        type: 'info',
                        allowOutsideClick: false
                    }
                ).then((result) => {
                    if (result.value) {
                        window.location.href = "/logout";
                    }
                });
            } else if (data.responseJSON.message) {
                messageError(data.responseJSON.message);
            } else {
                messageError("Ocorreu uma inconsistência no sistema ao tentar alterar os dados do fornecedor.");
            }
        }
    });
}

function details(id) {
    $.ajax({
        type: 'GET',
        url: uri.provider.find(id),
        dataType: 'json',
        headers: {
            "content-type": "application/json;charset=UTF-8"
        },
        beforeSend: function (xhr) {
            showLoading('Por favor aguarde', 'Carregando os dados do fornecedor');
            // xhr.setRequestHeader(`Authorization', 'Bearer ${token}`);
        },
        success: function (response) {
            swal.close();
            $('#provider-create-update').trigger("reset");
            $('#provider_id').val(response.data.id);
            $('#name').val(response.data.name);
            $('#cnpj').val(response.data.cnpj ? mask(response.data.cnpj, '##.###.###/####-##'): null);
            $('#phone').val(response.data.phone ? mask(response.data.phone, '(##) #.####-####') : null);
            $('.title-form-provider').text('Atualizar dados do fornecedor');
            $('.create-update').text('Atualizar');
            $('#modal-create-or-update').modal({ backdrop: 'static', keyboard: true, show: true });
        },
        error: function (data) {
            swal.close();
            if (data.status === HTTP_INTERNAL_SERVER_ERROR) {
                messageError("Ocorreu uma inconsistência ao tentar editar um fornecedor.");
            } else if (data.status === HTTP_UNAUTHORIZED && data.responseJSON.message === "Expired JWT Token") {
                swal(
                    {
                        title: 'Informação',
                        text: 'Sua sessão expirou faça o login novamente.',
                        type: 'info',
                        allowOutsideClick: false
                    }
                ).then((result) => {
                    if (result.value) {
                        window.location.href = "/logout";
                    }
                });
            } else if (data.responseJSON.message) {
                messageError(data.responseJSON.message);
            } else {
                messageError("Ocorreu uma inconsistência ao tentar editar um fornecedor.");
            }
        }
    });
}

function removeStyle() {
    removeBorderOnFocus('#name');
    removeBorderOnFocus('#cnpj');
    // removeBorderOnFocus('#phone');
}

function modalClose() {
    $('#provider_id').val(null);
    $('#provider-create-update').trigger("reset");
    removeBorder('#name');
    removeBorder('#cnpj');
}