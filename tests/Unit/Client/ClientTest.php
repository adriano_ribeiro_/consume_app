<?php

namespace Unit\Client;

use App\Models\Client;
use App\Repositories\Client\ClientRepository;
use Tests\TestCase;

class ClientTest extends TestCase
{
    public function testGetName()
    {
        $clientRepository = new ClientRepository(new Client());
        $data = $clientRepository->findByName('Adr');
        $this->assertIsArray($data);
    }
}