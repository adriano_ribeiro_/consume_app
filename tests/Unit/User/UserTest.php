<?php

namespace Unit\User;

use App\Models\User;
use App\Repositories\User\UserRepository;
use Tests\TestCase;

class UserTest extends TestCase
{
    public function testFindUserByName()
    {
        $userRepository = new UserRepository(new User());
        $data = $userRepository->findUserByName('Adriano M Ribeiro');
        $this->assertIsArray($data);
    }
}
