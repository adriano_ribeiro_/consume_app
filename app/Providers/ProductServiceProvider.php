<?php

namespace App\Providers;

use App\Models\Category;
use App\Models\Product;
use App\Models\Provider;
use App\Repositories\Category\CategoryRepository;
use App\Repositories\Product\ProductRepository;
use App\Repositories\Provider\ProviderRepository;
use App\Services\Product\ProductService;
use Illuminate\Support\ServiceProvider;

class ProductServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ProductService::class, function ($app) {
            return new ProductService(
                new ProductRepository(new Product()), 
                new ProviderRepository(new Provider()),
                new CategoryRepository(new Category())
            );
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
