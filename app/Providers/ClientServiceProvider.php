<?php

namespace App\Providers;

use App\Models\Client;
use App\Models\Person;
use App\Repositories\Client\ClientRepository;
use App\Repositories\Person\PersonRepository;
use App\Services\Client\ClientService;
use Illuminate\Support\ServiceProvider;

class ClientServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ClientService::class, function ($app) {
            return new ClientService(
                new ClientRepository(new Client()),
                new PersonRepository(new Person())
            );
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
