<?php

namespace App\Providers;

use App\Models\Client;
use App\Models\Item;
use App\Models\Product;
use App\Models\Sale;
use App\Models\User;
use App\Repositories\Client\ClientRepository;
use App\Repositories\Item\ItemRepository;
use App\Repositories\Product\ProductRepository;
use App\Repositories\Sale\SaleRepository;
use App\Repositories\User\UserRepository;
use App\Services\SaleItem\SaleItemService;
use Illuminate\Support\ServiceProvider;

class SaleItemServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(SaleItemService::class, function ($app) {
            return new SaleItemService(
                new SaleRepository(new Sale()),
                new ItemRepository(new Item()),
                new ProductRepository(new Product()),
                new ClientRepository(new Client()),
                new UserRepository(new User()),
            );
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
