<?php

namespace App\Providers;

use App\Models\Provider;
use App\Repositories\Provider\ProviderRepository;
use App\Services\Provider\ProviderService;
use Illuminate\Support\ServiceProvider;

class ProviderServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ProviderService::class, function ($app) {
            return new ProviderService(new ProviderRepository(new Provider()));
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
