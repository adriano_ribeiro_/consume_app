<?php

declare (strict_types = 1);

namespace App\Interfaces;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

/**
 * Interface ServiceInterface
 * @package App\Interfaces
 */
interface ServiceInterface
{
    /**
     * @param array $data
     * @return JsonResponse
     */
    public function create(array $data): JsonResponse;

    /**
     * @param Request $request
     * @param int $limit
     * @param array $orderBy
     * @return JsonResponse
     */
    public function findAll(Request $request, int $limit = 10, array $orderBy = []): JsonResponse;

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function findOneBy(int $id): JsonResponse;

    /**
     * @param string $param
     * @param array $data
     * @return JsonResponse
     */
    public function editBy(string $param, array $data): JsonResponse;

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function delete(int $id): JsonResponse;

    /**
     * @param string $string
     * @param array $searchFields
     * @param int $limit
     * @param array $orderBy
     * @return JsonResponse
     */
    public function searchBy(
        string $string,
        array $searchFields,
        int $limit = 10,
        array $orderBy = []
    ): JsonResponse;

    /**
     * @param int $status
     * @return array
     */
    public function findByStatus(int $status): JsonResponse;
}