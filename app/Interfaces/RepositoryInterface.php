<?php

declare(strict_types=1);

namespace App\Interfaces;

/**
 * Interface RepositoryInterface
 * @package App\Interfaces
 */
interface RepositoryInterface
{
    /**
     * @param array $data
     * @return array
     */
    public function create(array $data): array;

    /**
     * @param int $limit
     * @param array $orderBy
     * @return array
     */
    public function findAll(int $limit = 10, array $orderBy = []): array;

    /**
     * @param int $id
     * @return array
     */
    public function findOneBy(int $id): array;

    /**
     * @param string $param
     * @return array
     */
    public function findBy(string $param, $value): array;

    /**
     * @param string $param
     * @param array $data
     * @return bool
     */
    public function editBy(string $param, array $data): bool;

    /**
     * @param int $id
     * @return bool
     */
    public function delete(int $id): bool;

    /**
     * @param string $string
     * @param array $searchFields
     * @param int $limit
     * @param array $orderBy
     * @return array
     */
    public function searchBy(
        string $string,
        array $searchFields,
        int $limit = 10,
        array $orderBy = []
    ): array;

    /**
     * @param boolean $status
     * @return array
     */
    public function findByStatus(bool $status): array;
}