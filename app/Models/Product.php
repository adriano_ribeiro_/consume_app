<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Product extends Model
{
    use HasFactory;
    
    protected $table = "version_01.products";
    
    protected $fillable = [
        'name', 
        'barcode',
        'category_id',
        'purchase_price',
        'percentage_wholesale',
        'wholesale_price', // preço no atacado
        'percentage_retail',
        'retail_price', // preço no varejo
        'quantity',
        'purchase_date',
        'provider_id',
        'status'
    ];

    /** @var string[] */
    protected $dates = ['purchase_date'];

    /**
     * @return HasOne
     */
    public function category(): HasOne
    {
        return $this->hasOne(Category::class, 'category_id', 'id');
    }

    /**
     * @return HasOne
     */
    public function provider(): HasOne
    {
        return $this->hasOne(Provider::class, 'id', 'provider_id');
    }
}