<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Client extends Model
{
    use HasFactory;

    protected $table = "version_01.clients";

    protected $appends = ['person'];

    protected $fillable = [
        'instagram', 
        'phone',
        'birth_date',
        'person_id'
    ];

    /** @var string[] */
    protected $dates = ['birth_date'];

    /**
     * The attributes that should be hidden for serialization.
     * @var array
     */
    protected $hidden = [
        'person_id'
    ];

    /**
     * @return HasOne
     */
    public function person(): HasOne
    {
        return $this->hasOne(Person::class, 'id', 'person_id');
    }

    /**
     * @return array
     */
    public function getPersonAttribute(): array
    {
        return Person::where([
            "id" => $this->person_id,
        ])->get()->first()->toArray();
    }
}