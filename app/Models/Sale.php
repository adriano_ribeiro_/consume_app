<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Sale extends Model
{
    use HasFactory;

    protected $table = "version_01.sales";
    
    protected $fillable = [
        'amount',
        'sale_value',
        'client_id',
        'status'
    ];

    /**
     * @return HasOne
     */
    public function client(): HasOne
    {
        return $this->hasOne(Client::class, 'client_id', 'id');
    }

    /**
     * @return BelongsTo
     */
    public function items(): BelongsTo
    {
        return $this->belongsTo(Item::class);
    }
}