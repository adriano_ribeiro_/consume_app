<?php

declare(strict_types=1);

namespace App\Http\Controllers\V1;

use App\Http\Controllers\AbstractController;
use App\Services\SaleItem\SaleItemService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

/**
 * Class SaleItemController
 * @package App\Http\Controllers\V1
 */
class SaleItemController extends AbstractController
{
    /**
     * @var SaleItemService
     */
    protected SaleItemService $service;

    /**
     * UserController constructor.
     * @param SaleItemService $service
     */
    public function __construct(SaleItemService $service)
    {
        $this->service = $service;
    }

    /**
     * @return JsonResponse
     */
    public function findSaleById(int $saleId): JsonResponse
    {
        return $this->service->findSaleById($saleId);
    }

    /**
     * @return JsonResponse
     */
    public function findBySaleActive(): JsonResponse
    {
        return $this->service->findSaleByStatus(true);
    }

    /**
     * @return JsonResponse
     */
    public function findBySaleInactive(): JsonResponse
    {
        return $this->service->findSaleByStatus(false);
    }

    /**
     * @param string $start
     * @param string $end
     * @return JsonResponse findByStartDateAndEndDate 
     */
    public function findByStartDateAndEndDate (string $start, string $end): JsonResponse
    {
        return $this->service->findByStartDateAndEndDate($start, $end);
    }

    /**
     * @param int $personId
     * @param string $start
     * @param string $end
     * @return JsonResponse
     */
    public function findCommissionByDate(int $personId, string $start, string $end): JsonResponse
    {
        return $this->service->findCommissionByDate($personId, $start, $end);
    }

    /**
     * @param string $start
     * @param string $end
     * @return JsonResponse
     */
    public function findAllCommissionByDate(string $start, string $end): JsonResponse
    {
        return $this->service->findAllCommissionByDate($start, $end);
    }

    /**
     ** Finaliza a venda
     * @return JsonResponse
     */
    public function finishSale(Request $request): JsonResponse
    {
        return $this->service->finishSale($request->all());
    }

    /**
     * @return JsonResponse
     */
    public function countSaleOpenFinish(): JsonResponse
    {
        return $this->service->countSaleOpenFinish();
    }

    /**
     * @param array $data
     * @return JsonResponse
     */
    public function removeItem(Request $request): JsonResponse
    {
        return $this->service->removeItem($request->all());
    }

    /**
    * @param int $status
    * @param Request $request
    * @return JsonResponse
    */
    public function getOpenFinishSales(int $status, Request $request): JsonResponse
    {
        return $this->service->getOpenFinishSales($request, ($status == 1));
    }
}