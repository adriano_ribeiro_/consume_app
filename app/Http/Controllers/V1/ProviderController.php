<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\AbstractController;
use App\Services\Provider\ProviderService;

/**
 * Class ProviderController
 * @package App\Http\Controllers\V1
 */
class ProviderController extends AbstractController
{
    /**
     * @var ProviderService
     */
    protected ProviderService $service;

    /**
     * ProviderController constructor.
     * @param ProviderService $service
     */
    public function __construct(ProviderService $service)
    {
        $this->service = $service;
    }
}