<?php

declare(strict_types=1);

namespace App\Http\Controllers\V1;

use App\Http\Controllers\AbstractController;
use App\Services\Client\ClientService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

/**
 * Class ClientController
 * @package App\Http\Controllers\V1
 */
class ClientController extends AbstractController
{
    /**
     * @var ClientService
     */
    protected ClientService $service;

    /**
     * ClientController constructor.
     * @param ClientService $service
     */
    public function __construct(ClientService $service)
    {
        $this->service = $service;
    }

    /**
     * @param string $name
     * @return JsonResponse
     */
    public function findByName(Request $request): JsonResponse
    {
        return $this->service->findByName($request->term);
        
    }
}