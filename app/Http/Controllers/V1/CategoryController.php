<?php

declare(strict_types=1);

namespace App\Http\Controllers\V1;

use App\Http\Controllers\AbstractController;
use App\Services\Category\CategoryService;

/**
 * Class CategoryController
 * @package App\Http\Controllers\V1
 */
class CategoryController extends AbstractController
{
    /**
     * @var CategoryService
     */
    protected CategoryService $service;

    /**
     * CategoryController constructor.
     * @param CategoryService $service
     */
    public function __construct(CategoryService $service)
    {
        $this->service = $service;
    }
}