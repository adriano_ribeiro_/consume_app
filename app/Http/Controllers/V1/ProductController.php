<?php

declare(strict_types=1);

namespace App\Http\Controllers\V1;

use App\Http\Controllers\AbstractController;
use App\Services\Product\ProductService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

/**
 * Class ProductController
 * @package App\Http\Controllers\V1
 */
class ProductController extends AbstractController
{
    /**
     * @var ProductService
     */
    protected ProductService $service;

    /**
     * ProductController constructor.
     * @param ProductService $service
     */
    public function __construct(ProductService $service)
    {
        $this->service = $service;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function findByBarcodeOrName(Request $request): JsonResponse
    {
        return $this->service->findByBarcodeOrName($request->term);
    }
}