<?php

declare(strict_types=1);

namespace App\Http\Controllers\V1;

use App\Http\Controllers\AbstractController;
use App\Services\User\UserService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Class UserController
 * @package App\Http\Controllers\V1
 */
class UserController extends AbstractController
{
    /**
     * @var UserService
     */
    protected UserService $service;

    /**
     * UserController constructor.
     * @param UserService $service
     */
    public function __construct(UserService $service)
    {
        $this->service = $service;
    }

    /**
     * @param Request $request
     * @param Session $session
     * @return JsonResponse
     */
    public function auth(Request $request, Session $session): JsonResponse
    {
        return $this->service->auth($request->only('login', 'password'), $session);
    }

    /**
     * @param Request $request
     * @param Session $session
     * @return JsonResponse
     */
    public function logout(Session $session): JsonResponse
    {
        return $this->service->logout($session);
    }
}