<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Interfaces\ControllerInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;

/**
 * Class Controller
 * @package App\Http\Controllers
 */
abstract class AbstractController extends Controller implements ControllerInterface
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        return $this->service->create($request->all());
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function findAll(Request $request): JsonResponse
    {
        return $this->service->findAll($request);
    }

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function findOneBy(int $id): JsonResponse
    {
        return $this->service->findOneBy($id);
    
    }

    /**
     * @param Request $request
     * @param string $param
     * @return JsonResponse
     */
    public function editBy(Request $request, string $param): JsonResponse
    {
        return $this->service->editBy($param, $request->all());
    }

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function delete(int $id): JsonResponse
    {
        return $this->service->delete($id);
    }

    /**
     * @param int $status
     * @return JsonResponse
     */
    public function findByStatus(int $status): JsonResponse
    {
        return $this->service->findByStatus($status);
    }
}