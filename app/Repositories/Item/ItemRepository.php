<?php

declare(strict_types=1);

namespace App\Repositories\Item;

use App\Repositories\AbstractRepository;
use Illuminate\Support\Facades\DB;
use Throwable;

class ItemRepository extends AbstractRepository
{
    /**
     * @param int $saleId
     * @param int $productId
     * @param int $userId
     * @return array
     */
    public function findByProductAndUser(int $saleId, int $productId, int $userId, float $unitPrice): array 
    {
        try {
            return json_decode(json_encode(DB::table('version_01.items AS item')
                ->join('version_01.sales AS sale', 'sale.id', '=', 'item.sale_id')
                ->join('version_01.products AS product', 'product.id', '=', 'item.product_id')
                ->join('version_01.users AS user', 'user.id', '=', 'item.user_id')
                ->where('sale.id', $saleId)
                ->where('product.id', $productId)
                ->where('item.unit_price', $unitPrice)
                ->where('user.id', $userId)
                ->select('item.*')
                ->get()
                ->toArray()[0] ?? []), true)
            ;
        } catch (Throwable $e) {
            throw $e;
        }
    }
}