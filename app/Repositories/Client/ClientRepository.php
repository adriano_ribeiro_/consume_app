<?php

declare(strict_types=1);

namespace App\Repositories\Client;

use App\Models\Client;
use App\Repositories\AbstractRepository;
use Illuminate\Support\Facades\DB;
use Throwable;

/**
 * Class ClientRepository
 * @package App\Repositories\Client
 */
class ClientRepository extends AbstractRepository
{
    /**
     * @param boolean $status
     * @return array
     */
    public function findByStatus(bool $status, int $limit = 10, array $orderBy = []): array
    {
        try {
            // return $this
            //     ->model::query()
            //     ->with('person')
            //     ->whereHas("person", function($q) use ($status) {
            //         $q->where("status", "=", $status);
            //     })
            //     ->get()->toArray() ?? []
            // ;
            
            $data = $this->model::query();

            foreach ($orderBy as $key => $value) {
                if (strstr($key, '-')) {
                    $key = substr($key, 1);
                }

                $data->orderBy($key, $value);
            }

            return $data
                ->with('person')
                ->whereHas("person", function($q) use ($status) {
                    $q->where("status", "=", $status);
                })
                ->paginate($limit)
                ->appends([
                    'order_by' => implode(',', array_keys($orderBy)),
                    'limit' => $limit
                ])
                ->toArray();
        } catch (Throwable $e) {
            throw $e;
        }
    }

    /**
     * @param boolean $status
     * @return array
     */
    public function findClientByName(string $name): array
    {
        try {
            return $this
                ->model::query()
                ->with('person')
                ->whereHas("person", function($q) use ($name) {
                    $q->where("name", "=", $name);
                })
                ->get()->toArray()[0] ?? []
            ;
        } catch (Throwable $e) {
            throw $e;
        }
    }

    /**
     * @param string $name
     * @return array
     */
    public function findByName(string $name): array
    {
        try {
            return json_decode(json_encode(DB::table('version_01.clients AS c')
                    ->select(['c.id', 'p.name']) 
                    ->join('version_01.people AS p', 'p.id', '=', 'c.person_id')
                    ->where(DB::raw('lower(name)'), "ILIKE", '%'.strtolower($name).'%')
                    ->get()
                    ->toArray()), true)
            ;
        } catch (Throwable $e) {
            throw $e;
        }
    }
}