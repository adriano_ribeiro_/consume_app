<?php

declare(strict_types=1);

namespace App\Repositories\Sale;

use App\Models\Sale;
use App\Repositories\AbstractRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Throwable;

class SaleRepository extends AbstractRepository
{
    /**
     * @param boolean $status
     * @return array
     */
    public function findSaleById(int $saleId): array
    {
        try {
            return DB::table('version_01.items AS item')
                ->join('version_01.sales AS sale', 'sale.id', '=', 'item.sale_id')
                ->join('version_01.products AS product', 'product.id', '=', 'item.product_id')
                ->join('version_01.users AS user', 'user.id', '=', 'item.user_id')
            // ->join("version_01.sales_boards",function($join) {
            //     $join
            //         ->on("version_01.sales_boards.sale_id","=","version_01.sales.id");
            //         // ->on("version_01.sales_boards.board_id","=","version_01.boards.id");
            // })
                ->where('sale.id', $saleId)
                ->select('item.id AS item_id', 'sale.id AS sale_id', 'product.barcode', 'product.name AS product', DB::raw('SUM(item.quantity) AS quantity'), 'item.unit_price AS unit_price', DB::raw('SUM(item.subtotal) AS subtotal'), 'sale.amount', 'sale.sale_value')
                ->groupBy('item.id', 'sale.id', 'product.barcode', 'product.name', 'unit_price', 'amount', 'sale_value')
                ->get()
                ->toArray()
            ;
        } catch (Throwable $e) {
            throw $e;
        }
    }

    /**
     * @param boolean $status
     * @return array
     */
    public function findSaleByStatus(bool $status): array
    {
        try {
            $items = DB::table('version_01.items AS item')
                ->join('version_01.sales AS sale', 'sale.id', '=', 'item.sale_id')
                ->join('version_01.products AS product', 'product.id', '=', 'item.product_id')
                ->join('version_01.users AS user', 'user.id', '=', 'item.user_id')
            // ->join("version_01.sales_boards",function($join) {
            //     $join
            //         ->on("version_01.sales_boards.sale_id","=","version_01.sales.id");
            //         // ->on("version_01.sales_boards.board_id","=","version_01.boards.id");
            // })
                ->where('sale.status', $status)
                ->select('sale.id AS sale_id', 'product.barcode', 'product.name AS product', DB::raw('SUM(item.quantity) AS quantity'), 'item.unit_price AS unit_price', DB::raw('SUM(item.subtotal) AS subtotal'))
                ->groupBy('sale.id', 'product.barcode', 'product.name', 'unit_price')
                ->get()
                ->toArray()
            ;

            // $items = json_decode(json_encode($items), true);
            // foreach ($items as $key => $item) {
            //     /** @var Sale */
            //     $sale = Sale::find($item['sale_id']);
            //     /** @var Board */
            //     foreach($sale->boards()->getResults() as $board) {
            //         $items[$key]['boards'][] = [
            //             'board_id' => $board->id,
            //             'board' => $board->name,
            //         ];
            //     }
            // }
            return $items;
        } catch (Throwable $e) {
            throw $e;
        }
    }

    /**
     * @param string $startDate
     * @param string $endDate
     * @return void
     */
    public function findByStartDateAndEndDate(string $startDate, string $endDate)
    {
        try {
            return DB::table('version_01.items AS item')
                ->join('version_01.sales AS sale', 'sale.id', '=', 'item.sale_id')
                ->join('version_01.products AS product', 'product.id', '=', 'item.product_id')
                ->join('version_01.users AS user', 'user.id', '=', 'item.user_id')
                ->where('sale.status', false)
                ->whereBetween('sales.created_at', ["{$startDate} 00:00:00", "{$endDate} 23:59:59"])
                ->select('items.*')
                ->get()
                ->toArray()
            ;
        } catch (Throwable $e) {
            throw $e;
        }
    }

    /**
     * @param integer $userId
     * @param string $start
     * @param string $end
     * @return float
     */
    public function findCommissionByDate(int $userId, string $start, string $end): float
    {
        try {
            return DB::table('version_01.items AS item')
                ->join('version_01.sales AS sale', 'sale.id', '=', 'item.sale_id')
                ->join('version_01.products AS product', 'product.id', '=', 'item.product_id')
                ->join('version_01.users AS user', 'user.id', '=', 'item.user_id')
                ->where('sale.status', false)
                ->where('user.id', $userId)
                ->whereBetween('item.created_at', ["{$start} 00:00:00", "{$end} 23:59:59"])
                ->sum('item.subtotal') * 10 / 100
            ;
        } catch (Throwable $e) {
            throw $e;
        }
    }

    /**
     * @param integer $userId
     * @param string $start
     * @param string $end
     * @return array
     */
    public function findAllCommissionByDate(string $start, string $end): array
    {
        try {
            return DB::table('version_01.items AS item')
                ->join('version_01.sales AS sale', 'sale.id', '=', 'item.sale_id')
                ->join('version_01.users AS user', 'user.id', '=', 'item.user_id')
                ->where('sale.status', false)
                ->whereBetween('item.created_at', ["{$start} 00:00:00", "{$end} 23:59:59"])
                ->select(
                    'user.name AS user',
                    DB::raw('SUM(item.subtotal) * 10 / 100 AS commission')
                )
                ->groupBy('user.name')
                ->get()
                ->toArray()
            ;
        } catch (Throwable $e) {
            throw $e;
        }
    }

    /**
     * @param int $clientId
     * @return array
     */
    public function findSaleByClient(int $clientId): array
    {
        try {
            // $items = DB::table('version_01.sales AS sale')
            //     ->join('version_01.clients AS client', 'client.id', '=', 'sale_board.board_id')
            //     ->where('sale.status', true)
            //     ->where('board.id', $clientId)
            //     ->select('sale.id AS sale_id', 'sale.amount', 'sale.status AS sale_status', 'board.id AS board_id', 'board.name AS board')
            //     ->get()
            // ;

            // return count($items) > 0 ? json_decode(json_encode($items[0]), true) : [];
        } catch (Throwable $e) {
            throw $e;
        }
    }

    /**
     * @param array $data
     * @return bool
     */
    public function finishSale(array $data): bool
    {
        try {
            return $this->editBy($data['sale_id'], $data);
        } catch (Throwable $e) {
            throw $e;
        }
    }

    /**
     * @return array
     */
    public function countSaleOpenFinish(): array
    {
        try {
            return [
                'count_open' => Sale::query()->where('status', true)->count(),
                'count_finish' => Sale::query()->where('status', false)->whereBetween('updated_at', [Carbon::parse()->format('Y-m-d').' 00:00:00', Carbon::parse()->format('Y-m-d').' 23:59:59'])->count()
            ];
        } catch (Throwable $e) {
            throw $e;
        }
    }

    /**
    * @param array $data
    * @return array
    */
    public function getOpenFinishSales(array $data): array
    {
        try {
            $totalData = Sale::query()->where('status', $data['status'])->count();
                
            $totalFiltered = $totalData; 

            $columns = $data['status'] ? ['amount', 'updated_at', 'id'] : ['sale_value', 'updated_at', 'id'];
            $search = $data['search']; 

            if(empty($search)) {            
                $sales = Sale::query()
                                ->select($columns)
                                ->where('status', $data['status'])
                                ->offset($data['start'])
                                ->limit($data['limit'])
                                ->orderBy($columns[$data['order']], $data['dir'])
                                ->get()
                                ->toArray();
            } else {
                $sales = Sale::query()
                                ->select($columns)
                                ->where('status', $data['status'])
                                ->where(function ($query) use ($search) {
                                    $query
                                        ->where('amount', 'LIKE',"%{$search}%")
                                        ->orWhere('updated_at', 'LIKE',"%{$search}%")
                                        ->orWhere('updated_at', 'LIKE',"-%{$search}%")
                                    ;
                                })
                                ->offset($data['start'])
                                ->limit($data['limit'])
                                ->orderBy($columns[$data['order']], $data['dir'])
                                ->get()
                                ->toArray();

                $totalFiltered = Sale::query()
                                    ->select($columns)
                                    ->where('status', $data['status'])
                                    ->where(function ($query) use ($search) {
                                        $query
                                            ->where('amount', 'LIKE',"%{$search}%")
                                            ->orWhere('updated_at', 'LIKE',"%{$search}%")
                                        ;
                                    })
                                    ->count();
            }

            return [
                "draw"            => intval($data['draw']),  
                "recordsTotal"    => intval($totalData),  
                "recordsFiltered" => intval($totalFiltered), 
                "data"            => $sales  
            ];
        } catch (Throwable $e) {
            throw $e;
        }
    }
}