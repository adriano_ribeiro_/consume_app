<?php

declare(strict_types=1);

namespace App\Repositories;

use App\Interfaces\RepositoryInterface;
use Illuminate\Database\Eloquent\Model;
use Throwable;

/**
 * Class AbstractRepository
 * @package App\Repositories
 */
abstract class AbstractRepository implements RepositoryInterface
{
    /**
     * @var Model
     */
    protected Model $model;

    /**
     * AbstractRepository constructor.
     * @param Model $model
     */
    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    /**
     * @param array $data
     * @return array
     */
    public function create(array $data): array
    {
        try {
            return $this->model::create($data)->toArray();
        } catch (Throwable $e) {
            throw $e;
        }
    }

    /**
     * @param int $limit
     * @param array $orderBy
     * @return array
     */
    public function findAll(int $limit = 10, array $orderBy = []): array
    {
        try {
            $data = $this->model::query();

            foreach ($orderBy as $key => $value) {
                if (strstr($key, '-')) {
                    $key = substr($key, 1);
                }

                $data->orderBy($key, $value);
            }

            return $data
                ->paginate($limit)
                ->appends([
                    'order_by' => implode(',', array_keys($orderBy)),
                    'limit' => $limit
                ])
                ->toArray();
        } catch (Throwable $e) {
            throw $e;
        }
    }

    /**
     * @param int $id
     * @return array
     */
    public function findOneBy(int $id): array
    {
        try {
            return $this
                ->model::query()
                ->where('id', $id)
                ->get()
                ->toArray()[0] ?? []
            ;
        } catch (Throwable $e) {
            throw $e;
        }
    }

    /**
     * @param string $param
     * @return array
     */
    public function findBy(string $param, $value): array
    {
        try {
            return $this
                ->model::query()
                ->where($param, $value)
                ->get()
                ->toArray()
            ;
        } catch (Throwable $e) {
            throw $e;
        }
    }

    /**
     * @param string $param
     * @param array $data
     * @return bool
     */
    public function editBy(string $param, array $data): bool
    {
        try {
            return $this
                ->model::find($param)
                ->update($data) ? true : false;
        } catch (Throwable $e) {
            throw $e;
        }
    }

    /**
     * @param int $id
     * @return bool
     */
    public function delete(int $id): bool
    {
        try {
            return $this->model::destroy($id) ? true : false;
        } catch (Throwable $e) {
            throw $e;
        }
    }

    /**
     * @param string $string
     * @param array $searchFields
     * @param int $limit
     * @param array $orderBy
     * @return array
     */
    public function searchBy(
        string $string,
        array $searchFields,
        int $limit = 10,
        array $orderBy = []
    ): array 
    {
        try {
            $results = $this->model::where($searchFields[0], 'like', '%' . $string . '%');

            if (count($searchFields) > 1) {
                foreach ($searchFields as $field) {
                    $results->orWhere($field, 'like', '%' . $string . '%');
                }
            }

            foreach ($orderBy as $key => $value) {
                if (strstr($key, '-')) {
                    $key = substr($key, 1);
                }

                $results->orderBy($key, $value);
            }

            return $results
                ->paginate($limit)
                ->appends([
                    'order_by' => implode(',', array_keys($orderBy)),
                    'q' => $string,
                    'limit' => $limit
                ])
                ->toArray();
        } catch (Throwable $e) {
            throw $e;
        }
    }

    /**
     * @param boolean $status
     * @return array
     */
    public function findByStatus(bool $status): array
    {
        try {
            return json_decode(json_encode($this
                ->model::query()
                ->where('status', $status)
                ->get()), true) ?? []
            ;
        } catch (Throwable $e) {
            throw $e;
        }
    }
}