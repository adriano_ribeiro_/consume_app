<?php

declare(strict_types=1);

namespace App\Repositories\Person;

use App\Repositories\AbstractRepository;

/**
 * Class PersonRepository
 * @package App\Repositories\Person
 */
class PersonRepository extends AbstractRepository
{
}