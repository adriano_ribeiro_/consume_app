<?php

declare(strict_types=1);

namespace App\Repositories\Product;

use App\Models\Product;
use App\Repositories\AbstractRepository;
use Illuminate\Support\Facades\DB;
use Throwable;

/**
 * Class ProductRepository
 * @package App\Repositories\Product
 */
class ProductRepository extends AbstractRepository
{
    /**
     * @param int $limit
     * @param array $orderBy
     * @return array
     */
    public function findAll(int $limit = 10, array $orderBy = []): array
    {
        try {
            return DB::table('version_01.products as product')
                ->join('version_01.providers as provider', 'provider.id', '=', 'product.provider_id')
                ->join('version_01.categories as category', 'category.id', '=', 'product.category_id')
                ->select('product.*', 'provider.name as provider_name', 'category.name as category_name')
                ->paginate($limit)
                ->appends([
                    'order_by' => implode(',', array_keys($orderBy)),
                    'limit' => $limit
                ])
                ->toArray()
            ;
        } catch (Throwable $e) {
            throw $e;
        }
    }

    /**
     * @param string $barcodeOrName
     * @return array
     */
    public function findByBarcodeOrName(string $barcodeOrName): array
    {
        try {
            return Product::query()
                   ->select(['id', 'name', 'quantity']) 
                   ->where(DB::raw('lower(barcode)'), "ILIKE", '%'.strtolower($barcodeOrName).'%')
                   ->orWhere(DB::raw('lower(name)'), "ILIKE", '%'.strtolower($barcodeOrName).'%')
                   ->get()
                   ->toArray()
            ;
        } catch (Throwable $e) {
            throw $e;
        }
    }
}