<?php

declare(strict_types=1);

namespace App\Repositories\User;

use App\Repositories\AbstractRepository;
use Throwable;

/**
 * Class UserRepository
 * @package App\Repositories\User
 */
class UserRepository extends AbstractRepository
{
    /**
     * @param string $login
     * @param string $password
     * @return array
     */
    public function findByLogin(string $login): array
    {
        try {
            $user = $this
                        ->model::query()
                        ->where('login', '=', $login)
                        ->with('person')
                        ->whereHas("person", function($q) {
                            $q->where("status", "=", true);
                        })
                        ->first();

            if(!$user) {
                return [];
            }

            $user = $user->makeVisible(['password']);
            return json_decode(json_encode($user), true);
        } catch (Throwable $e) {
            throw $e;
        }
    }

    /**
     * @param boolean $status
     * @return array
     */
    public function findByStatus(bool $status): array
    {
        try {
            return $this
                ->model::query()
                ->with('person')
                ->whereHas("person", function($q) use ($status) {
                    $q->where("status", "=", $status);
                })
                ->get()->toArray() ?? []
            ;
        } catch (Throwable $e) {
            throw $e;
        }
    }

    /**
     * @param boolean $status
     * @return array
     */
    public function findUserByName(string $name): array
    {
        try {
            return $this
                ->model::query()
                ->with('person')
                ->whereHas("person", function($q) use ($name) {
                    $q->where("name", "=", $name);
                })
                ->get()->toArray()[0] ?? []
            ;
        } catch (Throwable $e) {
            throw $e;
        }
    }
}