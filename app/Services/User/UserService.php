<?php

declare(strict_types=1);

namespace App\Services\User;

use App\Exceptions\ValidationException;
use App\Helpers\OrderByHelper;
use App\Interfaces\ServiceInterface;
use App\Models\User;
use App\Repositories\Person\PersonRepository;
use App\Repositories\User\UserRepository;
use App\Utils\ResponseApi;
use App\Validators\User\UserValidator;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Throwable;

/**
 * Class UserService
 * @package App\Services\User
 */
class UserService implements ServiceInterface
{
    /**
     * @var UserRepository
     */
    protected UserRepository $userRepository;

    /**
     * @var PersonRepository
     */
    protected PersonRepository $personRepository;

    /**
     * @var array
     */
    protected array $searchFields = [];

   /**
    * @param UserRepository $userRepository
    * @param PersonRepository $personRepository
    */
    public function __construct(UserRepository $userRepository, PersonRepository $personRepository)
    {
        $this->userRepository = $userRepository;
        $this->personRepository = $personRepository;
    }

    /**
     * @param array $data
     * @param Session $session
     * @return JsonResponse
     */
    public function auth(array $data, Session $session): JsonResponse
    {
        try {
            $user = $this->userRepository->findByLogin($data['login']);
            if(empty($user) || !Hash::check($data['password'], $user['password'])) {
                throw new ValidationException("Login ou senha inválido");
            }
            unset($user['password']); 
            $session->set('user', $user);
            return ResponseApi::success('Autenticação bem sucedida', true);
        } catch (ValidationException $e) {
            return ResponseApi::warning($e->getMessage(), false);
        } catch (Throwable $e) {
            return ResponseApi::warning("Ocorreu uma inconsistência ao tentar cadastrar um usuário, caso persista contate  o suporte", $e->getMessage());
        }
    }

    /**
     * @param Session $session
     * @return void
     */
    public function logout(Session $session): JsonResponse
    {
        try {
            $session->clear();
            return ResponseApi::success("Logout efetuado com sucesso!", true);
        } catch (Throwable $e) {
            return ResponseApi::warning("Ocorreu uma inconsistência ao tentar sair do sistema", $e->getMessage());
        }
    }

    /**
     * @param array $data
     * @return JsonResponse
     */
    public function create(array $data): JsonResponse
    {
        try {
            UserValidator::validate($data);
            return DB::transaction(function () use ($data) {
                $person = $this->personRepository->create($data);

                $data['person_id'] = $person['id'];
                $data['password'] = Hash::make($data['password'], [
                    'rounds' => 12,
                ]);
                // if (Hash::check('plain-text', $hashedPassword)) {
                //     // The passwords match...
                // }
                return ResponseApi::success("Usuário cadastrado com sucesso", $this->userRepository->create($data));
            });
        } catch (ValidationException $e) {
            return ResponseApi::warning($e->getMessage(), false);
        } catch (Throwable $e) {
            return ResponseApi::warning("Ocorreu uma inconsistência ao tentar cadastrar um usuário, caso persista contate  o suporte", $e->getMessage());
        }
    }

   /**
    * @param Request $request
    * @param integer $limit
    * @param array $orderBy
    * @return JsonResponse
    */
    public function findAll(Request $request, int $limit = 10, array $orderBy = []): JsonResponse
    {
        try {
            $limit = (int) $request->get('limit', $limit);
            $orderBy = $request->get('order_by', $orderBy);

            if (!empty($orderBy)) {
                $orderBy = OrderByHelper::treatOrderBy($orderBy);
            }

            $searchString = $request->get('q', '');

            if (!empty($searchString)) {
                $data = $this->searchBy(
                    $searchString,
                    $this->searchFields,
                    $limit,
                    $orderBy
                );
            } else {
                $data = $this->userRepository->findAll($limit, $orderBy);
            }
            return ResponseApi::successList("Detalhes do(s) usuário(s)", $data);
        } catch (Throwable $e) {
            return ResponseApi::warning("Ocorreu uma inconsistência ao tentar listar os destalhes do(s) usuário(s)", $e->getMessage());
        }
    }

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function findOneBy(int $id): JsonResponse
    {
        try {
            $user = $this->userRepository->findOneBy($id);
            if(empty($user)) {
                throw new ValidationException("Usuário não encontrado", Response::HTTP_BAD_REQUEST);
            }
            return ResponseApi::success("Detalhes do usuário", $user);
        } catch (ValidationException $e) {
            return ResponseApi::warning($e->getMessage(), false);
        } catch (Throwable $e) {
            return ResponseApi::warning("Ocorreu uma inconsistência ao tentar listar os detalhes do usuário", $e->getMessage());
        }
    }

    /**
     * @param string $param
     * @param array $data
     * @return JsonResponse
     */
    public function editBy(string $param, array $data): JsonResponse
    {
        try {
            return DB::transaction(function () use ($param, $data) {
                $user = $this->userRepository->findBy('id', $param);
                if(empty($user)) {
                    throw new ValidationException("Usuário não encontrado", Response::HTTP_BAD_REQUEST);
                } 

                $person = $this->personRepository->findBy('name', $data['name']);
                if(!empty($person) && $person[0]['id'] != $user[0]['person']['id']) {
                    throw new ValidationException("Já existe um usuário cadastrado com esse nome: {$data['name']}", Response::HTTP_BAD_REQUEST);
                }

                $person = $this->personRepository->editBy((string)$user[0]['person']['id'], $data);

                $user = $this->userRepository->findBy('login', $data['login']);
                if(!empty($user) && $user[0]['id'] != $param) {
                    throw new ValidationException("Já existe um usuário cadastrado com esse login: {$data['login']}", Response::HTTP_BAD_REQUEST);
                }
                return ResponseApi::success("Dados do usuário alterado com sucesso", $this->userRepository->editBy($param, $data));
            });
        } catch (ValidationException $e) {
            return ResponseApi::warning($e->getMessage(), false);
        } catch (Throwable $e) {
            return ResponseApi::error("Ocorreu uma inconsistência ao tentar alterar os dados do usuário, caso persista contate  o suporte", $e->getMessage());
        }
    }

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function delete(int $id): JsonResponse
    {
        try {
            return DB::transaction(function () use ($id) {
                $user = $this->userRepository->findOneBy($id);
                if(empty($user)) {
                    throw new ValidationException("Usuário não encontrado", Response::HTTP_BAD_REQUEST);
                }
                return ResponseApi::success("Usuário excluido com sucesso", $this->userRepository->delete($id));
            });
        } catch (ValidationException $e) {
            return ResponseApi::warning($e->getMessage(), false);
        } catch (Throwable $e) {
            return ResponseApi::warning("Ocorreu uma inconsistência ao tentar excluir o usuário", $e->getMessage());
        }
    }

    /**
     * @param string $string
     * @param array $searchFields
     * @param int $limit
     * @param array $orderBy
     * @return JsonResponse
     */
    public function searchBy(string $string, array $searchFields, int $limit = 10, array $orderBy = []): JsonResponse
    {
        try {
            return ResponseApi::success(
                "Dados do(s) usuário(s)", 
                $this
                    ->repository
                    ->searchBy(
                        $string,
                        $searchFields,
                        $limit, $orderBy
                    )
            );
        } catch (Throwable $e) {
            return ResponseApi::warning("Ocorreu uma inconsistência ao tentar listar os dados do(s) usuário(s)", $e->getMessage());
        }
    }
    
    /**
     * @param string $param
     * @return JsonResponse
     */
    public function findBy(string $param, $value): JsonResponse
    {
        try {
            return ResponseApi::success("Detalhes do usuário", $this->userRepository->findBy($param, $value));
        } catch (Throwable $e) {
            return ResponseApi::warning("Ocorreu uma inconsistência ao tentar buscar detalhes do usuário", $e->getMessage());
        }
    }

    /**
     * @param int $status
     * @return JsonResponse
     */
    public function findByStatus(int $status): JsonResponse
    {
        try {
            if($status == 1) {
                $status = true;
                $statusCurrent = 'ativos';
            } else if($status == 0) {
                $status = false;
                $statusCurrent = 'inativos';
            } else {
                throw new ValidationException("Informe 1 para ativos e 0 para inativos", Response::HTTP_BAD_REQUEST);
            }
            return ResponseApi::success("Detalhes do(s) usuário(s) {$statusCurrent}", $this->userRepository->findByStatus($status));
        } catch (ValidationException $e) {
            return ResponseApi::warning($e->getMessage(), false);
        } catch (Throwable $e) {
            return ResponseApi::warning("Ocorreu uma inconsistência ao tentar buscar detalhes do(s) usuário(s)", $e->getMessage());
        }
    }

    /**
     * @param string $name
     * @return JsonResponse
     */
    public function findUserByName(string $name): JsonResponse
    {
        try {
            return ResponseApi::success("Detalhes do usuário", $this->userRepository->findUserByName($name));
        } catch (Throwable $e) {
            return ResponseApi::warning("Ocorreu uma inconsistência ao tentar buscar detalhes do usuário", $e->getMessage());
        }
    }
}