<?php

declare(strict_types=1);

namespace App\Services\Product;

use App\Exceptions\ValidationException;
use App\Helpers\OrderByHelper;
use App\Interfaces\ServiceInterface;
use App\Repositories\Category\CategoryRepository;
use App\Repositories\Product\ProductRepository;
use App\Repositories\Provider\ProviderRepository;
use App\Utils\ResponseApi;
use App\Validators\Product\ProductValidator;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;
use Throwable;

/**
 * Class ProductService
 * @package App\Services\Product
 */
class ProductService implements ServiceInterface
{
    /**
     * @var ProductRepository
     */
    protected ProductRepository $repository;

    /**
     * @var ProviderRepository
     */
    protected ProviderRepository $providerRepository;

    /**
     * @var CategoryRepository
     */
    protected CategoryRepository $categoryRepository;

    /**
     * @var array
     */
    protected array $searchFields = [];

    /**
     * @param ProductRepository $repository
     */
    public function __construct(ProductRepository $repository, ProviderRepository $providerRepository, CategoryRepository $categoryRepository)
    {
        $this->repository = $repository;
        $this->providerRepository = $providerRepository;
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * @param array $data
     * @return JsonResponse
     */
    public function create(array $data): JsonResponse
    {
        try {
            ProductValidator::validate($data);
            return DB::transaction(function () use ($data) {
                $provider = $this->providerRepository->findOneBy((int)$data['provider_id']);
                if(empty($provider)) {
                    throw new ValidationException("Fornecedor não encontrado em nossa base de dados", Response::HTTP_BAD_REQUEST);
                }

                $category = $this->categoryRepository->findOneBy((int)$data['category_id']);
                if(empty($category)) {
                    throw new ValidationException("Categoria não encontrada em nossa base de dados", Response::HTTP_BAD_REQUEST);
                }
                return ResponseApi::success("Produto criado com sucesso", $this->repository->create($data));
            });
        } catch (ValidationException $e) {
            return ResponseApi::warning($e->getMessage(), false);
        } catch (Throwable $e) {
            return ResponseApi::warning("Ocorreu uma inconsistência ao tentar cadastrar um produto, caso persista contate  o suporte", $e->getMessage());
        }
    }

   /**
    * @param Request $request
    * @param integer $limit
    * @param array $orderBy
    * @return JsonResponse
    */
    public function findAll(Request $request, int $limit = 10, array $orderBy = []): JsonResponse
    {
        try {
            $limit = (int) $request->get('limit', $limit);
            $orderBy = $request->get('order_by', $orderBy);

            if (!empty($orderBy)) {
                $orderBy = OrderByHelper::treatOrderBy($orderBy);
            }

            $searchString = $request->get('q', '');

            if (!empty($searchString)) {
                $data = $this->searchBy(
                    $searchString,
                    $this->searchFields,
                    $limit,
                    $orderBy
                );
            } else {
                $data = $this->repository->findAll($limit, $orderBy);
            }
            return ResponseApi::successList("Detalhes dos produto", $data);
        } catch (Throwable $e) {
            return ResponseApi::warning("Ocorreu uma inconsistência ao tentar listar os produtos", $e->getMessage());
        }
    }

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function findOneBy(int $id): JsonResponse
    {
        try {
            $product = $this->repository->findOneBy($id);
            if(empty($product)) {
                throw new ValidationException("produto não encontrada", Response::HTTP_BAD_REQUEST);
            }
            return ResponseApi::success("Detalhes do produto", $product);
        } catch (ValidationException $e) {
            return ResponseApi::warning($e->getMessage(), false);
        } catch (Throwable $e) {
            return ResponseApi::warning("Ocorreu uma inconsistência ao tentar listar os detalhes do produto", $e->getMessage());
        }
    }

    /**
     * @param string $param
     * @param array $data
     * @return JsonResponse
     */
    public function editBy(string $param, array $data): JsonResponse
    {
        try {
            return DB::transaction(function () use ($param, $data) {
                $product = $this->repository->findBy('id', $param);
                if(empty($product)) {
                    throw new ValidationException("Produto não encontrada", Response::HTTP_BAD_REQUEST);
                } 
                
                $product = $this->repository->findBy('name', $data['name']);
                if(!empty($product) && $product[0]['id'] != $param) {
                    throw new ValidationException("Já existe um produto cadastrado com esse nome: {$data['name']}", Response::HTTP_BAD_REQUEST);
                }
                return ResponseApi::success("Dados do produto alterado com sucesso", $this->repository->editBy($param, $data));
            });
        } catch (ValidationException $e) {
            return ResponseApi::warning($e->getMessage(), false);
        } catch (Throwable $e) {
            return ResponseApi::error("Ocorreu uma inconsistência ao tentar alterar os dados do produto, caso persista contate  o suporte", $e->getMessage());
        }
    }

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function delete(int $id): JsonResponse
    {
        try {
            return DB::transaction(function () use ($id) {
                $product = $this->repository->findOneBy($id);
                if(empty($product)) {
                    throw new ValidationException("Produto não encontrada", Response::HTTP_BAD_REQUEST);
                }
                return ResponseApi::success("Produto deletada com sucesso", $this->repository->delete($id));
            });
        } catch (ValidationException $e) {
            return ResponseApi::warning($e->getMessage(), false);
        } catch (Throwable $e) {
            return ResponseApi::warning("Ocorreu uma inconsistência ao tentar deletar o produto", $e->getMessage());
        }
    }

    /**
     * @param string $string
     * @param array $searchFields
     * @param int $limit
     * @param array $orderBy
     * @return JsonResponse
     */
    public function searchBy(string $string, array $searchFields, int $limit = 10, array $orderBy = []): JsonResponse
    {
        try {
            return ResponseApi::success(
                "Dados dos produtos", 
                $this
                    ->repository
                    ->searchBy(
                        $string,
                        $searchFields,
                        $limit, $orderBy
                    )
            );
        } catch (Throwable $e) {
            return ResponseApi::warning("Ocorreu uma inconsistência ao tentar listar os dados dos produtos", $e->getMessage());
        }
    }
    
    /**
     * @param string $param
     * @return JsonResponse
     */
    public function findBy(string $param, $value): JsonResponse
    {
        try {
            return ResponseApi::success("Detalhes do produto", $this->repository->findBy($param, $value));
        } catch (Throwable $e) {
            return ResponseApi::warning("Ocorreu uma inconsistência ao tentar buscar detalhes do produto", $e->getMessage());
        }
    }

    /**
     * @param int $status
     * @return JsonResponse
     */
    public function findByStatus(int $status): JsonResponse
    {
        try {
            if($status == 1) {
                $status = true;
                $statusCurrent = 'ativos';
            } else if($status == 0) {
                $status = false;
                $statusCurrent = 'inativos';
            } else {
                throw new ValidationException("Informe 1 para ativos e 0 para inativos", Response::HTTP_BAD_REQUEST);
            }
            return ResponseApi::success("Detalhes do(s) produto(s) {$statusCurrent}", $this->repository->findByStatus($status));
        } catch (ValidationException $e) {
            return ResponseApi::warning($e->getMessage(), false);
        } catch (Throwable $e) {
            return ResponseApi::warning("Ocorreu uma inconsistência ao tentar buscar detalhes dos produtos", $e->getMessage());
        }
    }

    /**
     * @param string $barcodeOrName
     * @return JsonResponse
     */
    public function findByBarcodeOrName(string $barcodeOrName): JsonResponse
    {
        try {
            return ResponseApi::success("Detalhes do(s) produto(s)", $this->repository->findByBarcodeOrName($barcodeOrName));
        } catch (Throwable $e) {
            throw $e;
        }
    }
}