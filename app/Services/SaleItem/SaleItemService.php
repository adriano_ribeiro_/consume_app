<?php

declare(strict_types=1);

namespace App\Services\SaleItem;

use App\Exceptions\ValidationException;
use App\Helpers\OrderByHelper;
use App\Interfaces\ServiceInterface;
use App\Models\Item;
use App\Models\Product;
use App\Models\Sale;
use App\Models\User;
use App\Repositories\Client\ClientRepository;
use App\Repositories\Item\ItemRepository;
use App\Repositories\Product\ProductRepository;
use App\Repositories\Sale\SaleRepository;
use App\Repositories\User\UserRepository;
use App\Utils\ResponseApi;
use App\Validators\Item\ItemValidator;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Symfony\Component\HttpFoundation\Response;
use Throwable;

class SaleItemService implements ServiceInterface
{
    /**
     * @var SaleRepository
     */
    protected SaleRepository $saleRepository;

    /**
     * @var ItemRepository
     */
    protected ItemRepository $itemRepository;

    /**
     * @var ProductRepository
     */
    protected ProductRepository $productRepository;

    /**
     * @var ClientRepository
     */
    protected ClientRepository $clientRepository;

    /**
     * @var UserRepository
     */
    protected UserRepository $userRepository;

    /**
     * @var array
     */
    protected array $searchFields = [];

   /**
    * @param SaleRepository $saleRepository
    * @param ItemRepository $itemRepository
    * @param ProductRepository $productRepository
    * @param clientRepository $clientRepository
    * @param UserRepository $userRepository
    */ 
    public function __construct(
        SaleRepository $saleRepository, 
        ItemRepository $itemRepository,
        ProductRepository $productRepository,
        clientRepository $clientRepository,
        UserRepository $userRepository
    ) {
        $this->saleRepository = $saleRepository;
        $this->itemRepository = $itemRepository;
        $this->productRepository = $productRepository;
        $this->clientRepository = $clientRepository;
        $this->userRepository = $userRepository;
    }

    /**
     * @param array $data
     * @return JsonResponse
     */
    public function create(array $data): JsonResponse
    {
        try {
            ItemValidator::validate($data);
            return DB::transaction(function () use ($data) {
                $product = Product::find(intval($data['product_id']));
                if(!$product) {
                    throw new ValidationException("Produto não encontrado em nossa base de dados", Response::HTTP_BAD_REQUEST);
                } 
                if($product->quantity === 0) {
                    throw new ValidationException("Produto sem estoque", Response::HTTP_BAD_REQUEST);
                }
                if($product->quantity < $data['quantity']) {
                    throw new ValidationException("Quantidade de items menor que a do estoque: {$product->quantity}", Response::HTTP_BAD_REQUEST);
                }

                $product->quantity = $product->quantity - $data['quantity'];
                $product->update();

                //! Amarrado
                $data['user_id'] = Session::get('id');//User::all()->first()['id'];

                $sale = null;
                if($data['sale_id']) {
                    $item = $this->itemRepository->findBy('sale_id', $data['sale_id']);
                    if(empty($item)) {
                        throw new ValidationException("Venda não encontrada", Response::HTTP_BAD_REQUEST);
                    }

                    $data['unit_price'] = $data['is-attacked'] ? floatval($product->wholesale_price) : floatval($product->retail_price);
                    $item = $this->itemRepository->findByProductAndUser(intval($data['sale_id']), intval($data['product_id']), intval($data['user_id']), $data['unit_price']); 

                        if(!empty($item)) {
                        $data['unit_price'] = $data['is-attacked'] ? floatval($product->wholesale_price) : floatval($product->retail_price);
                        $data['quantity'] += $item['quantity'];
                        $data['subtotal'] = $data['unit_price'] * $data['quantity'];
                        /** @var Sale */
                        $sale = Sale::find(intval($data['sale_id']));
                        $subtotal = $data['subtotal'] - floatval($item['subtotal']);
                        $sale->amount += $subtotal;
                        $sale->update();
                        
                        $result = $this->itemRepository->editBy(strval($item['id']), $data);
                        if(!$result) {
                            throw new ValidationException("Não foi possível atualizar o item na venda", Response::HTTP_BAD_REQUEST);
                        }
                        $result = $this->itemRepository->findOneBy($item['id']);
                        $result['amount'] = floatval($sale->amount);
                    } else {
                        $data['unit_price'] = $data['is-attacked'] ? floatval($product->wholesale_price) : floatval($product->retail_price);
                        $data['subtotal'] = $data['unit_price'] * $data['quantity'];
                        $data['amount'] = $data['subtotal'];
                        $result = $this->itemRepository->create($data);
                        /** @var Sale */
                        $sale = Sale::find($data['sale_id']);
                        $sale->amount += $data['quantity'] * $data['unit_price'];
                        $sale->update();
                        $result['amount'] = floatval($sale->amount);
                    }
                } else {
                    $data['unit_price'] = $data['is-attacked'] ? floatval($product->wholesale_price) : floatval($product->retail_price);
                    $data['subtotal'] = $data['unit_price'] * $data['quantity'];
                    $data['amount'] = $data['subtotal'];
                    $sale = $this->saleRepository->create($data);
                    $data['sale_id'] = $sale['id'];
                    $result = $this->itemRepository->create($data);
                    $result['amount'] = $data['subtotal'];
                }
                
                return ResponseApi::success("Item adicionado com sucesso", $result);
            });
        } catch (ValidationException $e) {
            return ResponseApi::warning($e->getMessage(), false);
        } catch (Throwable $e) {
            return ResponseApi::error("Ocorreu uma inconsistência ao tentar cadastrar um item a venda, caso persista contate o suporte", 
                                      "{$e->getMessage()} line: {$e->getLine()} file: {$e->getFile()}");
        }
    }

   /**
    * @param Request $request
    * @param integer $limit
    * @param array $orderBy
    * @return JsonResponse
    */
    public function findAll(Request $request, int $limit = 10, array $orderBy = []): JsonResponse
    {
        try {
            $limit = (int) $request->get('limit', $limit);
            $orderBy = $request->get('order_by', $orderBy);

            if (!empty($orderBy)) {
                $orderBy = OrderByHelper::treatOrderBy($orderBy);
            }

            $searchString = $request->get('q', '');

            if (!empty($searchString)) {
                $data = $this->searchBy(
                    $searchString,
                    $this->searchFields,
                    $limit,
                    $orderBy
                );
            } else {
                $data = $this->itemRepository->findAll($limit, $orderBy);
            }
            return ResponseApi::success("Detalhes da(s) venda(s)", $data);
        } catch (Throwable $e) {
            return ResponseApi::error("Ocorreu uma inconsistência ao tentar listar os destalhes da(s) venda(s)", $e->getMessage());
        }
    }

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function findOneBy(int $id): JsonResponse
    {
        try {
            $user = $this->saleRepository->findOneBy($id);
            if(empty($user)) {
                throw new ValidationException("Venda não encontrada", Response::HTTP_BAD_REQUEST);
            }
            return ResponseApi::success("Detalhes da venda", $user);
        } catch (ValidationException $e) {
            return ResponseApi::warning($e->getMessage(), false);
        } catch (Throwable $e) {
            return ResponseApi::error("Ocorreu uma inconsistência ao tentar listar os detalhes da venda", $e->getMessage());
        }
    }

    /**
     ** usado apenas para encerrar a venda 
     *
     * @param string $param
     * @param array $data
     * @return JsonResponse
     */
    public function editBy(string $param, array $data): JsonResponse
    {
        try {
            return DB::transaction(function () use ($param, $data) {
                $sale = Sale::find($param);
                if(!$sale) {
                    throw new ValidationException("Venda não encontrada", Response::HTTP_BAD_REQUEST);
                }
                
                return ResponseApi::success("Venda finalizada com sucesso", $this->saleRepository->editBy($param, $data));
            });
        } catch (ValidationException $e) {
            return ResponseApi::warning($e->getMessage(), false);
        } catch (Throwable $e) {
            return ResponseApi::error("Ocorreu uma inconsistência ao tentar finalizar uma venda, caso persista contate  o suporte", $e->getMessage());
        }
    }

    /**
     *? Remove o item da venda.
     * @param int $id
     * @return JsonResponse
     */
    public function delete(int $id): JsonResponse
    {
        try {
            return DB::transaction(function () use ($id) {
                $item = $this->itemRepository->delete($id);
                if(!$item) {
                    throw new ValidationException("Ocorreu uma inconsistência ao tentar remover o item", Response::HTTP_BAD_REQUEST);
                }
                return ResponseApi::success("Item removido com sucesso", true);
            });
        } catch (ValidationException $e) {
            return ResponseApi::warning($e->getMessage(), false);
        } catch (Throwable $e) {
            return ResponseApi::error("Ocorreu uma inconsistência ao tentar remover o item", $e->getMessage());
        }
    }

    /**
     * @param string $string
     * @param array $searchFields
     * @param int $limit
     * @param array $orderBy
     * @return JsonResponse
     */
    public function searchBy(string $string, array $searchFields, int $limit = 10, array $orderBy = []): JsonResponse
    {
        try {
            return ResponseApi::success(
                "Dados das pessoas", 
                $this
                    ->itemRepository
                    ->searchBy(
                        $string,
                        $searchFields,
                        $limit, $orderBy
                    )
            );
        } catch (Throwable $e) {
            return ResponseApi::error("Ocorreu uma inconsistência ao tentar listar os dados da(s) venda(s)", $e->getMessage());
        }
    }
    
    /**
     * @param string $param
     * @return JsonResponse
     */
    public function findBy(string $param, $value): JsonResponse
    {
        try {
            return ResponseApi::success("Detalhes da venda", $this->saleRepository->findBy($param, $value));
        } catch (Throwable $e) {
            return ResponseApi::error("Ocorreu uma inconsistência ao tentar buscar detalhes da venda", $e->getMessage());
        }
    }

    /**
     * @param int $status
     * @return JsonResponse
     */
    public function findByStatus(int $status): JsonResponse
    {
        try {
            if($status == 1) {
                $status = true;
                $statusCurrent = 'ativas';
            } else if($status == 0) {
                $status = false;
                $statusCurrent = 'inativas';
            } else {
                throw new ValidationException("Informe 1 para ativos e 0 para inativos", Response::HTTP_BAD_REQUEST);
            }
            return ResponseApi::success("Detalhes da(s) venda(s) {$statusCurrent}", $this->saleRepository->findByStatus($status));
        } catch (ValidationException $e) {
            return ResponseApi::warning($e->getMessage(), false);
        } catch (Throwable $e) {
            return ResponseApi::error("Ocorreu uma inconsistência ao tentar buscar detalhes da(s) pessoa(s)", $e->getMessage());
        }
    }

    /**
     * @param boolean $status
     * @return JsonResponse
     */
    public function findSaleById(int $saleId): JsonResponse
    {
        try {
            return ResponseApi::success("Detalhes da venda", $this->saleRepository->findSaleById($saleId));
        } catch (Throwable $e) {
            return ResponseApi::warning("Ocorreu uma inconsistência ao tentar buscar detalhes da venda", $e->getMessage());
        }
    }

    /**
     * @param boolean $status
     * @return JsonResponse
     */
    public function findSaleByStatus(bool $status): JsonResponse
    {
        try {
            $message = $status ? 'ativas': 'inativas';
            return ResponseApi::success("Detalhes das vendas {$message}", $this->saleRepository->findSaleByStatus($status));
        } catch (Throwable $e) {
            return ResponseApi::warning("Ocorreu uma inconsistência ao tentar buscar detalhes das vendas {$message}", $e->getMessage());
        }
    }

    /**
     * @param string $startDate
     * @param string $endDate
     * @return JsonResponse
     */
    public function findByStartDateAndEndDate(string $startDate, string $endDate): JsonResponse
    {
        try {
            return ResponseApi::success("Detalhes das vendas por período", $this->saleRepository->findByStartDateAndEndDate($startDate, $endDate));
        } catch (Throwable $e) {
            return ResponseApi::warning("Ocorreu uma inconsistência ao tentar buscar detalhes das vendas por período", $e->getMessage());
        }
    }

    /**
     * @param int $personId
     * @param string $start
     * @param string $end
     * @return JsonResponse
     */
    public function findCommissionByDate(int $personId, string $start, string $end): JsonResponse
    {
        try {
            return ResponseApi::success("Comissão do garçom por período", $this->saleRepository->findCommissionByDate($personId, $start, $end));
        } catch (Throwable $e) {
            return ResponseApi::warning("Ocorreu uma inconsistência ao tentar buscar a comissão do garçom por período", $e->getMessage());
        }
    }

    /**
     * @param string $start
     * @param string $end
     * @return JsonResponse
     */
    public function findAllCommissionByDate(string $start, string $end): JsonResponse
    {
        try {
            return ResponseApi::success("Comissão dos garçons por período", $this->saleRepository->findAllCommissionByDate($start, $end));
        } catch (Throwable $e) {
            return ResponseApi::warning("Ocorreu uma inconsistência ao tentar buscar a comissão do garçom por período", $e->getMessage());
        }
    }

    /**
     * @param string $start
     * @param string $end
     * @return JsonResponse
     */
    public function finishSale(array $data): JsonResponse
    {
        try {
            // dd($data);
            if(!$this->saleRepository->finishSale($data)) {
                return ResponseApi::warning("Ocorreu uma inconsistência ao tentar finalizar a venda", false);
            }
            return ResponseApi::success("Venda finalizada com sucesso", true);
        } catch (Throwable $e) {
            return ResponseApi::warning("Ocorreu uma inconsistência ao tentar finalizar a venda", $e->getMessage());
        }
    }

    /**
     * @return JsonResponse
     */
    public function countSaleOpenFinish(): JsonResponse
    {
        try {
            return ResponseApi::success("quantidade de venda(s)", $this->saleRepository->countSaleOpenFinish());
        } catch (Throwable $e) {
            return ResponseApi::warning("Ocorreu uma inconsistência ao carregar a quantidade de vendas em aberto.", $e->getMessage());
        }
    }

    /**
     * @param array $data
     * @return JsonResponse
     */
    public function removeItem(array $data): JsonResponse
    {
        try {
            return DB::transaction(function () use ($data) {
                $item = Item::find($data['item_id']);
                if(!$item) {
                    throw new ValidationException("Item não encontrado", Response::HTTP_BAD_REQUEST);
                }

                $sale = Sale::find($item->sale_id);
                if($item->quantity == $data['quantity'] && abs(floatval($sale->amount)-floatval($item->subtotal)) < 0.00001) {
                    if(!$this->itemRepository->delete($item->id)) {
                        throw new ValidationException("Ocorreu uma inconsistência ao tentar remover o item", Response::HTTP_BAD_REQUEST);
                    }
                    if(!$this->saleRepository->delete($sale->id)) {
                        throw new ValidationException("Ocorreu uma inconsistência ao tentar remover o item", Response::HTTP_BAD_REQUEST);
                    }
                    
                    $data = [
                        'sale_id' => 0,
                        'amount' => 0.00
                    ];
                    return ResponseApi::success("Item removido com sucesso", $data);
                }
                
                $item->quantity -= $data['quantity'];
                $subtotal = $item->subtotal;
                $item->subtotal = $item->quantity * floatval($item->unit_price);
                $subtotal -= $item->subtotal;
                $sale->amount -= $subtotal; 
                if($item->subtotal == 0) {
                    if(!$this->itemRepository->delete($item->id)) {
                        throw new ValidationException("Ocorreu uma inconsistência ao tentar remover o item", Response::HTTP_BAD_REQUEST);
                    }
                } else {
                    $item->update();
                }
                $sale->update();

                $data = [
                    'sale_id' => $sale->id,
                    'amount' => floatval($sale->amount)
                ];
                
                return ResponseApi::success("Item removido com sucesso", $data);
            });
        } catch (ValidationException $e) {
            return ResponseApi::warning($e->getMessage(), false);
        } catch (Throwable $e) {
            return ResponseApi::warning("Ocorreu uma inconsistência ao tentar remover o item.", $e->getMessage());
        }
    }

    /**
    * @param Request $request
    * @return JsonResponse
    */
    public function getOpenFinishSales(Request $request, bool $status): JsonResponse
    {
        try {
            return ResponseApi::successList("Detalhes das vendas", $this->saleRepository->getOpenFinishSales([
                'status' => $status,
                'limit' => $request->input('length'),
                'start' => $request->input('start'),
                'order' => $request->input('order.0.column'),
                'dir' => $request->input('order.0.dir'),
                'search' => $request->input('search.value'),
                'draw' => intval($request->input('draw'))
            ]));
        } catch (Throwable $e) {
            return ResponseApi::warning("Ocorreu uma inconsistência ao tentar listar as vendas", $e->getMessage());
        }
    }
}