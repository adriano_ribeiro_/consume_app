<?php

namespace App\Services\Provider;

use App\Exceptions\ValidationException;
use App\Helpers\OrderByHelper;
use App\Interfaces\ServiceInterface;
use App\Repositories\Provider\ProviderRepository;
use App\Utils\ResponseApi;
use App\Validators\Provider\ProviderValidator;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;
use Throwable;

/**
 * Class ProviderService
 * @package App\Services\Provider
 */
class ProviderService implements ServiceInterface
{
    /**
     * @var ProviderRepository
     */
    protected ProviderRepository $repository;

    /**
     * @var array
     */
    protected array $searchFields = [];

    /**
     * @param ProviderRepository $repository
     */
    public function __construct(ProviderRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param array $data
     * @return JsonResponse
     */
    public function create(array $data): JsonResponse
    {
        try {
            ProviderValidator::validate($data);
            return DB::transaction(function () use ($data) {
                return ResponseApi::success("Fornecedor criado com sucesso", $this->repository->create($data));
            });
        } catch (ValidationException $e) {
            return ResponseApi::warning($e->getMessage(), false);
        } catch (Throwable $e) {
            return ResponseApi::warning("Ocorreu uma inconsistência ao tentar criar um fornecedor, caso persista contate  o suporte", $e->getMessage());
        }
    }

   /**
    * @param Request $request
    * @param integer $limit
    * @param array $orderBy
    * @return JsonResponse
    */
    public function findAll(Request $request, int $limit = 10, array $orderBy = []): JsonResponse
    {
        try {
            $limit = (int) $request->get('limit', $limit);
            $orderBy = $request->get('order_by', $orderBy);

            if (!empty($orderBy)) {
                $orderBy = OrderByHelper::treatOrderBy($orderBy);
            }

            $searchString = $request->get('q', '');

            if (!empty($searchString)) {
                $data = $this->searchBy(
                    $searchString,
                    $this->searchFields,
                    $limit,
                    $orderBy
                );
            } else {
                $data = $this->repository->findAll($limit, $orderBy);
            }
            return ResponseApi::successList("Detalhes do(s) fornecedore(s)", $data);
        } catch (Throwable $e) {
            return ResponseApi::warning("Ocorreu uma inconsistência ao tentar listar o(s) fornecedor(es)", $e->getMessage());
        }
    }

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function findOneBy(int $id): JsonResponse
    {
        try {
            $provider = $this->repository->findOneBy($id);
            if(empty($provider)) {
                throw new ValidationException("Fornecedor não encontrado", Response::HTTP_BAD_REQUEST);
            }
            return ResponseApi::success("Detalhes do fornecedor", $provider);
        } catch (ValidationException $e) {
            return ResponseApi::warning($e->getMessage(), false);
        } catch (Throwable $e) {
            return ResponseApi::warning("Ocorreu uma inconsistência ao tentar listar os detalhes do fornecedor", $e->getMessage());
        }
    }

    /**
     * @param string $param
     * @param array $data
     * @return JsonResponse
     */
    public function editBy(string $param, array $data): JsonResponse
    {
        try {
            return DB::transaction(function () use ($param, $data) {
                $provider = $this->repository->findBy('id', $param);
                if(empty($provider)) {
                    throw new ValidationException("Fornecedor não encontrado", Response::HTTP_BAD_REQUEST);
                } 
                
                $provider = $this->repository->findBy('name', $data['name']);
                if(!empty($provider) && $provider[0]['id'] != $param) {
                    throw new ValidationException("Já existe um fornecedor cadastrado com esse nome: {$data['name']}", Response::HTTP_BAD_REQUEST);
                }

                return ResponseApi::success("Dados do fornecedor alterado com sucesso", $this->repository->editBy($param, $data));
        });
        } catch (ValidationException $e) {
            return ResponseApi::warning($e->getMessage(), false);
        } catch (Throwable $e) {
            return ResponseApi::error("Ocorreu uma inconsistência ao tentar alterar os dados do fornecedor, caso persista contate  o suporte", $e->getMessage());
        }
    }

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function delete(int $id): JsonResponse
    {
        try {
            return DB::transaction(function () use ($id) {
                $provider = $this->repository->findOneBy($id);
                if(empty($provider)) {
                    throw new ValidationException("Fornecedor não encontrado", Response::HTTP_BAD_REQUEST);
                }
                return ResponseApi::success("Fornecedor deletada com sucesso", $this->repository->delete($id));
            });
        } catch (ValidationException $e) {
            return ResponseApi::warning($e->getMessage(), false);
        } catch (Throwable $e) {
            return ResponseApi::warning("Ocorreu uma inconsistência ao tentar deletar a Fornecedor", $e->getMessage());
        }
    }

    /**
     * @param string $string
     * @param array $searchFields
     * @param int $limit
     * @param array $orderBy
     * @return JsonResponse
     */
    public function searchBy(string $string, array $searchFields, int $limit = 10, array $orderBy = []): JsonResponse
    {
        try {
            return ResponseApi::success(
                "Dados do fornecedor alterado com sucesso", 
                $this
                    ->repository
                    ->searchBy(
                        $string,
                        $searchFields,
                        $limit, $orderBy
                    )
            );
        } catch (Throwable $e) {
            return ResponseApi::warning("Ocorreu uma inconsistência ao tentar listar o(s) fornecedor(es)", $e->getMessage());
        }
    }
    
    /**
     * @param string $param
     * @return JsonResponse
     */
    public function findBy(string $param, $value): JsonResponse
    {
        try {
            return ResponseApi::success("Detalhes do(s) fornecedor(es)", $this->repository->findBy($param, $value));
        } catch (Throwable $e) {
            return ResponseApi::warning("Ocorreu uma inconsistência ao tentar buscar detalhes do(s) fornecedore(s)", $e->getMessage());
        }
    }

    /**
     * @param int $status
     * @return JsonResponse
     */
    public function findByStatus(int $status): JsonResponse
    {
        try {
            if($status == 1) {
                $status = true;
                $statusCurrent = 'ativas';
            } else if($status == 0) {
                $status = false;
                $statusCurrent = 'inativas';
            } else {
                throw new ValidationException("Informe 1 para ativos e 0 para inativos", Response::HTTP_BAD_REQUEST);
            }
            return ResponseApi::success("Detalhes do(s) fornecedor(es) {$statusCurrent}", $this->repository->findByStatus($status));
        } catch (ValidationException $e) {
            return ResponseApi::warning($e->getMessage(), false);
        } catch (Throwable $e) {
            return ResponseApi::warning("Ocorreu uma inconsistência ao tentar buscar detalhes do(s) fornecedore(s)", $e->getMessage());
        }
    }
}