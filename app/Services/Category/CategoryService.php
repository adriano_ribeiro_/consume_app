<?php

declare(strict_types=1);

namespace App\Services\Category;

use App\Exceptions\ValidationException;
use App\Helpers\OrderByHelper;
use App\Interfaces\ServiceInterface;
use App\Models\Category;
use App\Repositories\Category\CategoryRepository;
use App\Utils\ResponseApi;
use App\Validators\Category\CategoryValidator;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;
use Throwable;

/**
 * Class CategoryService
 * @package App\Services\Category
 */
class CategoryService implements ServiceInterface
{
    /**
     * @var CategoryRepository
     */
    protected CategoryRepository $repository;

    /**
     * @var array
     */
    protected array $searchFields = [];

    /**
     * @param CategoryRepository $repository
     */
    public function __construct(CategoryRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param array $data
     * @return JsonResponse
     */
    public function create(array $data): JsonResponse
    {
        try {
            CategoryValidator::validate($data);
            return DB::transaction(function () use ($data) {
                $category = $this->repository->findBy('name', $data['name']);
                if(!empty($category)) {
                    throw new ValidationException("Já existe uma categoria cadastrada com esse nome: {$data['name']}", Response::HTTP_BAD_REQUEST);
                }
                return ResponseApi::success("Categoria criada com sucesso", $this->repository->create($data));
            });
        } catch (ValidationException $e) {
            return ResponseApi::warning($e->getMessage(), false);
        } catch (Throwable $e) {
            return ResponseApi::warning("Ocorreu uma inconsistência ao tentar criar uma categoria, caso persista contate  o suporte", $e->getMessage());
        }
    }

        /**
//     * @param Request $request
//     * @param integer $limit
//     * @param array $orderBy
//     * @return JsonResponse
//     */
//     public function findAll(Request $request, int $limit = 10, array $orderBy = []): JsonResponse
//     {
//         try {
//             $limit = (int) $request->get('limit', $limit);
//             $orderBy = $request->get('order_by', $orderBy);

//             if (!empty($orderBy)) {
//                 $orderBy = OrderByHelper::treatOrderBy($orderBy);
//             }

//             $searchString = $request->get('q', '');

//             if (!empty($searchString)) {
//                 $data = $this->searchBy(
//                     $searchString,
//                     $this->searchFields,
//                     $limit,
//                     $orderBy
//                 );
//             } else {
//                 $data = $this->repository->findAll($limit, $orderBy);
//             }
//             return ResponseApi::successList("Detalhes das categorias", $data);
//         } catch (Throwable $e) {
//             return ResponseApi::warning("Ocorreu uma inconsistência ao tentar listar as categorias", $e->getMessage());
//         }
//     }

    /**
    * @param Request $request
    * @param integer $limit
    * @param array $orderBy
    * @return JsonResponse
    */
    public function findAll(Request $request, int $limit = 10, array $orderBy = []): JsonResponse
    {
        try {
            $totalData = Category::query()->where('status', true)->count();
                
            $totalFiltered = $totalData; 

            $columns = ['id', 'name'];

            $limit = $request->input('length');
            $start = $request->input('start');
            $order = $columns[$request->input('order.0.column')];
            $dir = $request->input('order.0.dir');
                
            if(empty($request->input('search.value'))) {            
                $categories = Category::query()
                                ->select($columns)
                                ->where('status', true)
                                ->offset($start)
                                ->limit($limit)
                                ->orderBy($order, $dir)
                                ->get()
                                ->toArray();
            } else {
                $search = $request->input('search.value'); 

                $categories = Category::query()
                                ->select($columns)
                                ->where('name', 'LIKE',"%{$search}%")
                                ->offset($start)
                                ->limit($limit)
                                ->orderBy($order, $dir)
                                ->get()
                                ->toArray();

                $totalFiltered = Category::query()
                                    ->select($columns)
                                    ->where('name', 'LIKE',"%{$search}%")
                                    ->count();
            }

            $data = [
                "draw"            => intval($request->input('draw')),  
                "recordsTotal"    => intval($totalData),  
                "recordsFiltered" => intval($totalFiltered), 
                "data"            => $categories  
            ];
    
            return ResponseApi::successList("Detalhes das categorias", $data);
        } catch (Throwable $e) {
            return ResponseApi::warning("Ocorreu uma inconsistência ao tentar listar as categorias", $e->getMessage());
        }
    }

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function findOneBy(int $id): JsonResponse
    {
        try {
            $category = $this->repository->findOneBy($id);
            if(empty($category)) {
                throw new ValidationException("Categoria não encontrada", Response::HTTP_BAD_REQUEST);
            }
            return ResponseApi::success("Detalhes da categoria", $category);
        } catch (ValidationException $e) {
            return ResponseApi::warning($e->getMessage(), false);
        } catch (Throwable $e) {
            return ResponseApi::warning("Ocorreu uma inconsistência ao tentar listar os detalhes da categoria", $e->getMessage());
        }
    }

    /**
     * @param string $param
     * @param array $data
     * @return JsonResponse
     */
    public function editBy(string $param, array $data): JsonResponse
    {
        try {
            return DB::transaction(function () use ($param, $data) {
                $category = $this->repository->findBy('id', $param);
                if(empty($category)) {
                    throw new ValidationException("Categoria não encontrada", Response::HTTP_BAD_REQUEST);
                } 
                
                $category = $this->repository->findBy('name', $data['name']);
                if(!empty($category) && $category[0]['id'] != $param) {
                    throw new ValidationException("Já existe uma categoria cadastrada com esse nome: {$data['name']}", Response::HTTP_BAD_REQUEST);
                }

                return ResponseApi::success("Dados da categoria alterado com sucesso", $this->repository->editBy($param, $data));
        });
        } catch (ValidationException $e) {
            return ResponseApi::warning($e->getMessage(), false);
        } catch (Throwable $e) {
            return ResponseApi::error("Ocorreu uma inconsistência ao tentar alterar os dados da categoria, caso persista contate  o suporte", $e->getMessage());
        }
    }

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function delete(int $id): JsonResponse
    {
        try {
            return DB::transaction(function () use ($id) {
                $category = $this->repository->findOneBy($id);
                if(empty($category)) {
                    throw new ValidationException("Categoria não encontrada", Response::HTTP_BAD_REQUEST);
                }
                return ResponseApi::success("Categoria deletada com sucesso", $this->repository->delete($id));
            });
        } catch (ValidationException $e) {
            return ResponseApi::warning($e->getMessage(), false);
        } catch (Throwable $e) {
            return ResponseApi::warning("Ocorreu uma inconsistência ao tentar deletar a categoria", $e->getMessage());
        }
    }

    /**
     * @param string $string
     * @param array $searchFields
     * @param int $limit
     * @param array $orderBy
     * @return JsonResponse
     */
    public function searchBy(string $string, array $searchFields, int $limit = 10, array $orderBy = []): JsonResponse
    {
        try {
            return ResponseApi::success(
                "Dados da categoria alterado com sucesso", 
                $this
                    ->repository
                    ->searchBy(
                        $string,
                        $searchFields,
                        $limit, $orderBy
                    )
            );
        } catch (Throwable $e) {
            return ResponseApi::warning("Ocorreu uma inconsistência ao tentar listar as categorias", $e->getMessage());
        }
    }
    
    /**
     * @param string $param
     * @return JsonResponse
     */
    public function findBy(string $param, $value): JsonResponse
    {
        try {
            return ResponseApi::success("Detalhes da(s) categoria(s)", $this->repository->findBy($param, $value));
        } catch (Throwable $e) {
            return ResponseApi::warning("Ocorreu uma inconsistência ao tentar buscar detalhes das categorias", $e->getMessage());
        }
    }

    /**
     * @param int $status
     * @return JsonResponse
     */
    public function findByStatus(int $status): JsonResponse
    {
        try {
            if($status == 1) {
                $status = true;
                $statusCurrent = 'ativas';
            } else if($status == 0) {
                $status = false;
                $statusCurrent = 'inativas';
            } else {
                throw new ValidationException("Informe 1 para ativos e 0 para inativos", Response::HTTP_BAD_REQUEST);
            }
            return ResponseApi::success("Detalhes da(s) categoria(s) {$statusCurrent}", $this->repository->findByStatus($status));
        } catch (ValidationException $e) {
            return ResponseApi::warning($e->getMessage(), false);
        } catch (Throwable $e) {
            return ResponseApi::warning("Ocorreu uma inconsistência ao tentar buscar detalhes das categorias", $e->getMessage());
        }
    }
}