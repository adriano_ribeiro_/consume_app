<?php

declare(strict_types=1);

namespace App\Services\Client;

use App\Exceptions\ValidationException;
use App\Helpers\OrderByHelper;
use App\Interfaces\ServiceInterface;
use App\Models\Client;
use App\Repositories\Person\PersonRepository;
use App\Repositories\Client\ClientRepository;
use App\Utils\PaginationDataTable;
use App\Utils\ResponseApi;
use App\Validators\Client\ClientValidator;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;
use Throwable;

/**
 * Class ClientService
 * @package App\Services\Client
 */
class ClientService implements ServiceInterface
{
    /**
     * @var ClientRepository
     */
    protected ClientRepository $clientRepository;

    /**
     * @var PersonRepository
     */
    protected PersonRepository $personRepository;

    /**
     * @var array
     */
    protected array $searchFields = [];

   /**
    * @param ClientService $clientRepository
    * @param PersonRepository $personRepository
    */
    public function __construct(ClientRepository $clientRepository, PersonRepository $personRepository)
    {
        $this->clientRepository = $clientRepository;
        $this->personRepository = $personRepository;
    }

    /**
     * @param array $data
     * @return JsonResponse
     */
    public function create(array $data): JsonResponse
    {
        try {
            ClientValidator::validate($data);
            return DB::transaction(function () use ($data) {
                $client = $this->clientRepository->findClientByName($data['name']);
                if(!empty($client)) {
                    throw new ValidationException("Já existe um cliente com esse nome", Response::HTTP_BAD_REQUEST);
                }
                
                $person = $this->personRepository->create($data);

                $data['person_id'] = $person['id'];

                return ResponseApi::success("Cliente cadastrado com sucesso", $this->clientRepository->create($data));
            });
        } catch (ValidationException $e) {
            return ResponseApi::warning($e->getMessage(), false);
        } catch (Throwable $e) {
            return ResponseApi::warning("Ocorreu uma inconsistência ao tentar cadastrar um cliente, caso persista contate  o suporte", $e->getMessage());
        }
    }

//    /**
//     * @param Request $request
//     * @param integer $limit
//     * @param array $orderBy
//     * @return JsonResponse
//     */
//     public function findAll(Request $request, int $limit = 10, array $orderBy = []): JsonResponse
//     {
//         try {
//             $limit = (int) $request->get('limit', $limit);
//             $orderBy = $request->get('order_by', $orderBy);

//             if (!empty($orderBy)) {
//                 $orderBy = OrderByHelper::treatOrderBy($orderBy);
//             }

//             $searchString = $request->get('q', '');

//             if (!empty($searchString)) {
//                 $data = $this->searchBy(
//                     $searchString,
//                     $this->searchFields,
//                     $limit,
//                     $orderBy
//                 );
//             } else {
//                 $data = $this->clientRepository->findAll($limit, $orderBy);
//             }
//             return ResponseApi::successList("Detalhes do(s) Cliente(s)", $data);
//         } catch (Throwable $e) {
//             return ResponseApi::warning("Ocorreu uma inconsistência ao tentar listar os destalhes do(s) Cliente(s)", $e->getMessage());
//         }
//     }

    /**
    * @param Request $request
    * @param integer $limit
    * @param array $orderBy
    * @return JsonResponse
    */
    public function findAll(Request $request, int $limit = 10, array $orderBy = []): JsonResponse
    {
        try {
            $totalData = Client::query()
                            ->with('person')
                            ->whereHas("person", function($q) {
                                $q->where("status", "=", true);
                            })->count();
                
            $totalFiltered = $totalData; 

            $columns = ['id', 'name', 'email', 'instagram', 'phone', 'birth_date'];

            $limit = $request->input('length');
            $start = $request->input('start');
            $order = $columns[$request->input('order.0.column')];
            $dir = $request->input('order.0.dir');
                
            if(empty($request->input('search.value'))) {      
                $clients = DB::table('version_01.clients AS client')
                                ->select('client.id', 'person.name', 'person.email', 'client.instagram', 'client.phone', 'client.birth_date')
                                ->join('version_01.people AS person', 'person.id', '=', 'client.person_id')
                                ->where('person.status', true)
                                ->offset($start)
                                ->limit($limit)
                                ->orderBy($order, $dir)
                                ->get()
                                ->toArray();
            } else {
                $search = $request->input('search.value'); 

                $clients = DB::table('version_01.clients AS client')
                                ->select('client.id', 'person.name', 'person.email', 'client.instagram', 'client.phone', 'client.birth_date')
                                ->join('version_01.people AS person', 'person.id', '=', 'client.person_id')
                                ->where('name','LIKE',"%{$search}%")
                                ->orWhere('email', 'LIKE',"%{$search}%")
                                ->orWhere('instagram', 'LIKE',"%{$search}%")
                                ->orWhere('phone', 'LIKE',"%{$search}%")
                                ->orWhere('birth_date', 'LIKE',"%{$search}%")
                                ->offset($start)
                                ->limit($limit)
                                ->orderBy($order, $dir)
                                ->get()
                                ->toArray();

                $totalFiltered = DB::table('version_01.clients AS client')
                                    ->select('client.id', 'person.name', 'person.email', 'client.instagram', 'client.phone', 'client.birth_date')
                                    ->join('version_01.people AS person', 'person.id', '=', 'client.person_id')
                                    ->where('name','LIKE',"%{$search}%")
                                    ->orWhere('email', 'LIKE',"%{$search}%")
                                    ->orWhere('instagram', 'LIKE',"%{$search}%")
                                    ->orWhere('phone', 'LIKE',"%{$search}%")
                                    ->orWhere('birth_date', 'LIKE',"%{$search}%")
                                    ->count();
            }

            $data = [
                "draw"            => intval($request->input('draw')),  
                "recordsTotal"    => intval($totalData),  
                "recordsFiltered" => intval($totalFiltered), 
                "data"            => $clients  
            ];
    
            return ResponseApi::successList("Lista de cliente(s)", $data);
        } catch (Throwable $e) {
            return ResponseApi::warning("Ocorreu uma inconsistência ao tentar listar o(s) cliente(s)", $e->getMessage());
        }
    }

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function findOneBy(int $id): JsonResponse
    {
        try {
            $user = $this->clientRepository->findOneBy($id);
            if(empty($user)) {
                throw new ValidationException("Cliente não encontrado", Response::HTTP_BAD_REQUEST);
            }
            return ResponseApi::success("Detalhes do cliente", $user);
        } catch (ValidationException $e) {
            return ResponseApi::warning($e->getMessage(), false);
        } catch (Throwable $e) {
            return ResponseApi::warning("Ocorreu uma inconsistência ao tentar listar os detalhes do cliente", $e->getMessage());
        }
    }

    /**
     * @param string $param
     * @param array $data
     * @return JsonResponse
     */
    public function editBy(string $param, array $data): JsonResponse
    {
        try {
            return DB::transaction(function () use ($param, $data) {
                $client = $this->clientRepository->findBy('id', $param);
                if(empty($client)) {
                    throw new ValidationException("Cliente não encontrado", Response::HTTP_BAD_REQUEST);
                } 

                $clientValid = $this->clientRepository->findClientByName($data['name']);
                
                if(!empty($clientValid) && $clientValid['id'] !== intval($param)) {
                    throw new ValidationException("Já existe um cliente cadastrado com esse nome: {$data['name']}", Response::HTTP_BAD_REQUEST);
                }

                if(!$this->personRepository->editBy((string)$client[0]['person']['id'], $data)) {
                    throw new ValidationException("Ocorreu uma inconsistência ao tentar alterar os dados do cliente", Response::HTTP_BAD_REQUEST);
                }
                if(!$this->clientRepository->editBy($param, $data)) {
                    throw new ValidationException("Ocorreu uma inconsistência ao tentar alterar os dados do cliente", Response::HTTP_BAD_REQUEST);
                }

                return ResponseApi::success("Dados do cliente alterado com sucesso", true);
            });
        } catch (ValidationException $e) {
            return ResponseApi::warning($e->getMessage(), false);
        } catch (Throwable $e) {
            return ResponseApi::error("Ocorreu uma inconsistência ao tentar alterar os dados do cliente, caso persista contate  o suporte", $e->getMessage());
        }
    }

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function delete(int $id): JsonResponse
    {
        try {
            return DB::transaction(function () use ($id) {
                $user = $this->clientRepository->findOneBy($id);
                if(empty($user)) {
                    throw new ValidationException("Cliente não encontrado", Response::HTTP_BAD_REQUEST);
                }
                return ResponseApi::success("Cliente excluido com sucesso", $this->clientRepository->delete($id));
            });
        } catch (ValidationException $e) {
            return ResponseApi::warning($e->getMessage(), false);
        } catch (Throwable $e) {
            return ResponseApi::warning("Ocorreu uma inconsistência ao tentar excluir o cliente", $e->getMessage());
        }
    }

    /**
     * @param string $string
     * @param array $searchFields
     * @param int $limit
     * @param array $orderBy
     * @return JsonResponse
     */
    public function searchBy(string $string, array $searchFields, int $limit = 10, array $orderBy = []): JsonResponse
    {
        try {
            return ResponseApi::success(
                "Dados do(s) cliente(s)", 
                $this
                    ->repository
                    ->searchBy(
                        $string,
                        $searchFields,
                        $limit, $orderBy
                    )
            );
        } catch (Throwable $e) {
            return ResponseApi::warning("Ocorreu uma inconsistência ao tentar listar os dados do(s) cliente(s)", $e->getMessage());
        }
    }
    
    /**
     * @param string $param
     * @return JsonResponse
     */
    public function findBy(string $param, $value): JsonResponse
    {
        try {
            return ResponseApi::success("Detalhes do cliente", $this->clientRepository->findBy($param, $value));
        } catch (Throwable $e) {
            return ResponseApi::warning("Ocorreu uma inconsistência ao tentar buscar detalhes do cliente", $e->getMessage());
        }
    }

    /**
     * @param int $status
     * @return JsonResponse
     */
    public function findByStatus(int $status): JsonResponse
    {
        try {
            if($status == 1) {
                $status = true;
                $statusCurrent = 'ativos';
            } else if($status == 0) {
                $status = false;
                $statusCurrent = 'inativos';
            } else {
                throw new ValidationException("Informe 1 para ativos e 0 para inativos", Response::HTTP_BAD_REQUEST);
            }
            return ResponseApi::successList("Detalhes do(s) cliente(s) {$statusCurrent}", $this->clientRepository->findByStatus($status));
        } catch (ValidationException $e) {
            return ResponseApi::warning($e->getMessage(), false);
        } catch (Throwable $e) {
            return ResponseApi::warning("Ocorreu uma inconsistência ao tentar buscar detalhes do(s) cliente(s)", $e->getMessage());
        }
    }

    /**
     * @param string $name
     * @return JsonResponse
     */
    public function findByName(string $name): JsonResponse
    {
        try {
            return ResponseApi::success("Detalhes do(s) cliente(s)", $this->clientRepository->findByName($name));
        } catch (Throwable $e) {
            throw $e;
        }
    }
}