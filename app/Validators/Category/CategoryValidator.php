<?php

declare(strict_types=1);

namespace App\Validators\Category;

use App\Exceptions\ValidationException;
use App\Models\Category;
use App\Validators\ValidatorInterface;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

abstract class CategoryValidator implements ValidatorInterface
{
    /**
     * @param array $data
     * @return void
     */
    public static function validate(array $data): void
    {
        $messages = [
            "name.required" => "É obrigatório informar o nome da categoria.",
            "name.unique" => "Já existe uma categoria cadastrada com esse nome.",
        ];

        $validator = Validator::make($data, [
            "name" => [
                "required","min:1",
                Rule::unique(Category::class, "name")
            ],
        ], $messages);

        if ($validator->fails()) {
            $messages = [];

            foreach ($validator->errors()->getMessages() as $msg) {
                $messages = array_merge($messages, $msg);
            }

            throw new ValidationException(implode(" ", $messages), Response::HTTP_BAD_REQUEST);
        }
    }
}