<?php

namespace App\Validators\Item;

use App\Exceptions\ValidationException;
use App\Validators\ValidatorInterface;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

abstract class ItemValidator implements ValidatorInterface
{
    /**
     * @param array $data
     * @return void
     */
    public static function validate(array $data): void
    {
        $messages = [
            "product_id.required" => "É obrigatório informar um produto.",
            "quantity.required" => "É obrigatório informar a quantidade do produto."
        ];

        $fields = [
            "product_id" => [
                "required","min:1","not_in:0"
            ],
            "quantity" => [
                "required","min:1","not_in:0"
            ]
        ];

        $validator = Validator::make($data, $fields, $messages);

        if ($validator->fails()) {
            $messages = [];

            foreach ($validator->errors()->getMessages() as $msg) {
                $messages = array_merge($messages, $msg);
            }

            throw new ValidationException(implode(" ", $messages), Response::HTTP_BAD_REQUEST);
        }
    }
}