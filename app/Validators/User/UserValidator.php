<?php

namespace App\Validators\User;

use App\Exceptions\ValidationException;
use App\Models\Person;
use App\Models\User;
use App\Validators\ValidatorInterface;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Symfony\Component\HttpFoundation\Response;

abstract class UserValidator implements ValidatorInterface
{
    /**
     * @param array $data
     * @return void
     */
    public static function validate(array $data): void
    {
        $messages = [
            "name.required" => "É obrigatório informar o nome do cliente.",
            "email.email" => "Informe um E-mail válido.",
            "login.required" => "É obrigatório informar o login do usuário.",
            "login.unique" => "Já existe um usuário cadastrado com esse login.",
            "password.required" => "É obrigatório informar a senha do usuário.",
            "password.min" => "Informe no mínimo 4 digitos para senha."
        ];

        $fields = [
            "login" => [
                "required","min:1",
                Rule::unique(User::class, "login")
            ],
            "password" => [
                "required","min:4"
            ],
        ];

        if(isset($data['email']) && !empty(trim($data['email']))) {
            $messages["email.email"] = "Informe um E-mail válido.";
            $fields["email"] = ["required","email"];
        }

        $validator = Validator::make($data, $fields, $messages);

        if ($validator->fails()) {
            $messages = [];

            foreach ($validator->errors()->getMessages() as $msg) {
                $messages = array_merge($messages, $msg);
            }

            throw new ValidationException(implode(" ", $messages), Response::HTTP_BAD_REQUEST);
        }
    }
}