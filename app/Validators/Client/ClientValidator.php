<?php

namespace App\Validators\Client;

use App\Exceptions\ValidationException;
use App\Validators\ValidatorInterface;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

abstract class ClientValidator implements ValidatorInterface
{
    /**
     * @param array $data
     * @return void
     */
    public static function validate(array $data): void
    {
        $messages = [
            "name.required" => "É obrigatório informar o nome do cliente.",
        ];

        $fields = [
            "name" => [
                "required","min:1"
            ]
        ];

        if(isset($data['email']) && !empty(trim($data['email']))) {
            $messages["email.email"] = "Informe um E-mail válido.";
            $fields["email"] = ["required","email"];
        }

        $validator = Validator::make($data, $fields, $messages);

        if ($validator->fails()) {
            $messages = [];

            foreach ($validator->errors()->getMessages() as $msg) {
                $messages = array_merge($messages, $msg);
            }

            throw new ValidationException(implode(" ", $messages), Response::HTTP_BAD_REQUEST);
        }
    }
}