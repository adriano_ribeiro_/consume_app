<?php

declare(strict_types=1);

namespace App\Validators;

interface ValidatorInterface
{
    public static function validate(array $data);
}