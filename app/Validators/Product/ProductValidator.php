<?php

namespace App\Validators\Product;

use App\Exceptions\ValidationException;
use App\Models\Product;
use App\Validators\ValidatorInterface;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Symfony\Component\HttpFoundation\Response;

abstract class ProductValidator implements ValidatorInterface
{
    /**
     * @param array $data
     * @return void
     */
    public static function validate(array $data): void
    {
        $messages = [
            "name.required" => "É obrigatório informar o nome do produto.",
            "name.unique" => "Já existe um produto cadastrado com esse nome.",
            "barcode.required" => "É obrigatório informar o código de barras do produto.",
            "barcode.unique" => "Já existe um produto cadastrado com esse código de barras.",
            "provider_id.required" => "É obrigatório informar o fonocedor do produto.",
            "category_id.required" => "É obrigatório informar a categoria do produto.",
            "purchase_price.required" => "É obrigatório informar o preço de compra do produto.",
            "percentage_wholesale.required" => "É obrigatório informar a porcentagem do valor de atacado.",
            "wholesale_price.required" => "É obrigatório informar o preço de venda no atacado do produto.",
            "percentage_retail.required" => "É obrigatório informar a porcentagem do valor de varejo.",
            "retail_price.required" => "É obrigatório informar o preço de venda no varejo do produto.",
            "quantity.required" => "É obrigatório informar a quantidade do produto.",
            "purchase_date.required" => "É obrigatório informar a data da compra do produto.",
            "purchase_date.date" => "Informe uma data valida"
        ];

        $fields = [
            "name" => [
                "required",
                Rule::unique(Product::class, "name")
            ],
            "barcode" => [
                "required",
                Rule::unique(Product::class, "barcode")
            ],
            "provider_id" => [
                "required"
            ],
            "category_id" => [
                "required"
            ],
            "purchase_price" => [
                "required"
            ],
            "percentage_wholesale" => [
                "required"
            ],
            "wholesale_price" => [
                "required"
            ],
            "percentage_retail" => [
                "required"
            ],
            "retail_price" => [
                "required"
            ],
            "quantity" => [
                "required"
            ],
            "purchase_date" => [
                "required","date"
            ]
        ];

        $validator = Validator::make($data, $fields, $messages);

        if ($validator->fails()) {
            $messages = [];

            foreach ($validator->errors()->getMessages() as $msg) {
                $messages = array_merge($messages, $msg);
            }

            throw new ValidationException(implode(" ", $messages), Response::HTTP_BAD_REQUEST);
        }

        $data['purchase_price'] = str_replace("R$ ", "",$data['purchase_price']);
        $data['wholesale_price'] = str_replace("R$ ", "",$data['wholesale_price']);
        $data['retail_price'] = str_replace("R$ ", "",$data['retail_price']);
    }
}