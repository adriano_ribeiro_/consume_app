<?php

namespace App\Validators\Provider;

use App\Exceptions\ValidationException;
use App\Models\Provider;
use App\Validators\ValidatorInterface;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Symfony\Component\HttpFoundation\Response;

abstract class ProviderValidator implements ValidatorInterface
{
    /**
     * @param array $data
     * @return void
     */
    public static function validate(array $data): void
    {
        $validator = Validator::make($data, [
            "name" => [
                "required","min:1",
                Rule::unique(Provider::class, "name")
                ],
            ],
            [
                "name.required" => "É obrigatório informar o nome do fornecedor.",
                "name.unique" => "Já existe um fornecedor cadastrado com esse nome.",
            ]
        );

        if ($validator->fails()) {
            $messages = [];

            foreach ($validator->errors()->getMessages() as $msg) {
                $messages = array_merge($messages, $msg);
            }

            throw new ValidationException(implode(" ", $messages), Response::HTTP_BAD_REQUEST);
        }
    }
}