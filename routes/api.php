<?php

use App\Http\Controllers\V1\CategoryController;
use App\Http\Controllers\V1\ClientController;
use App\Http\Controllers\V1\ProductController;
use App\Http\Controllers\V1\ProviderController;
use App\Http\Controllers\V1\SaleItemController;
use App\Http\Controllers\V1\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'V1', 'prefix' => 'v1'], function () {
    # rotas para categorias
    Route::group(['prefix' => '/category'], function () {
        Route::post('/create', [CategoryController::class, 'store']);
        Route::post('/update/{param}', [CategoryController::class, 'editBy']);
        Route::get('/all', [CategoryController::class, 'findAll']);
        Route::get('/find/{id}', [CategoryController::class, 'findOneBy']);
        Route::get('/delete/{id}', [CategoryController::class, 'delete']);
        Route::get('/find/status/{status}', [CategoryController::class, 'findByStatus']);
    });

    # rotas para usuários
    Route::group(['prefix' => '/user'], function () {
        Route::post('/auth', [UserController::class, 'auth']);
        Route::get('/logout', [UserController::class, 'logout']);
        Route::post('/create', [UserController::class, 'store']);
        Route::post('/update/{param}', [UserController::class, 'editBy']);
        Route::get('/all', [UserController::class, 'findAll']);
        Route::get('/find/{id}', [UserController::class, 'findOneBy']);
        Route::get('/delete/{id}', [UserController::class, 'delete']);
        Route::get('/find/status/{status}', [UserController::class, 'findByStatus']);
    });

    # rotas para clientes
    Route::group(['prefix' => '/client'], function () {
        Route::post('/create', [ClientController::class, 'store']);
        Route::post('/update/{param}', [ClientController::class, 'editBy']);
        Route::get('/all', [ClientController::class, 'findAll']);
        Route::get('/find/{id}', [ClientController::class, 'findOneBy']);
        Route::get('/delete/{id}', [ClientController::class, 'delete']);
        Route::get('/find/status/{status}', [ClientController::class, 'findByStatus']);
        Route::get('/get/name', [ClientController::class, 'findByName']);
    });

    # rotas para fornecedores
    Route::group(['prefix' => '/provider'], function () {
        Route::post('/create', [ProviderController::class, 'store']);
        Route::post('/update/{param}', [ProviderController::class, 'editBy']);
        Route::get('/all', [ProviderController::class, 'findAll']);
        Route::get('/find/{id}', [ProviderController::class, 'findOneBy']);
        Route::get('/delete/{id}', [ProviderController::class, 'delete']);
        Route::get('/find/status/{status}', [ProviderController::class, 'findByStatus']);
    });

    # rotas para produtos
    Route::group(['prefix' => '/product'], function () {
        Route::post('/create', [ProductController::class, 'store']);
        Route::post('/update/{param}', [ProductController::class, 'editBy']);
        Route::get('/all', [ProductController::class, 'findAll']);
        Route::get('/find/{id}', [ProductController::class, 'findOneBy']);
        Route::get('/delete/{id}', [ProductController::class, 'delete']);
        Route::get('/find/status/{status}', [ProductController::class, 'findByStatus']);
        Route::get('/barcode-name', [ProductController::class, 'findByBarcodeOrName']);
    });

    # rotas para os vendas de itens
    Route::group(['prefix' => '/sale-item'], function () {
        Route::post('/create', [SaleItemController::class, 'store']);
        Route::post('/update/{param}', [SaleItemController::class, 'editBy']);
        Route::get('/all', [SaleItemController::class, 'findAll']);
        Route::get('/find/{id}', [SaleItemController::class, 'findOneBy']);
        Route::get('/delete/{id}', [SaleItemController::class, 'delete']);
        Route::get('/find/status/{status}', [SaleItemController::class, 'findByStatus']);
        Route::get('/find/sale/{id}', [SaleItemController::class, 'findSaleById']);
        Route::get('/active', [SaleItemController::class, 'findBySaleActive']);
        Route::get('/inactive', [SaleItemController::class, 'findBySaleInactive']);
        Route::get('/find-sales-between-dates/{start}/{end}', [SaleItemController::class, 'findByStartDateAndEndDate']);
        Route::get('/find-sales-commission-dates/{personId}/{start}/{end}', [SaleItemController::class, 'findCommissionByDate']);
        Route::get('/find-sales-all-commission-dates/{start}/{end}', [SaleItemController::class, 'findAllCommissionByDate']);
        Route::post('/finish-sale', [SaleItemController::class, 'finishSale']);
        Route::get('/sale-open-finish', [SaleItemController::class, 'countSaleOpenFinish']);
        Route::post('/sale-remove-item', [SaleItemController::class, 'removeItem']);
        Route::get('/open-finish-sales/{status}', [SaleItemController::class, 'getOpenFinishSales']);
    });
});