<?php

use Illuminate\Support\Facades\Route;
use Symfony\Component\HttpFoundation\Session\Session;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    return view('user.auth');
});

Route::get('/home', function (Session $session) {
    if(!$session->has('user')) {
        return redirect('/');
    }
    return view('dashboard', ['name' => $session->get('user')]);
});

Route::get('/usuarios', function (Session $session) {
    if(!$session->has('user')) {
        return redirect('/');
    }
    return view('user.view', ['name' => $session->get('user')]);
});

Route::get('/clientes', function (Session $session) {
    if(!$session->has('user')) {
        return redirect('/');
    }
    return view('client.view', ['name' => $session->get('user')]);
});

Route::get('/categorias', function (Session $session) {
    if(!$session->has('user')) {
        return redirect('/');
    }
    return view('category.view', ['name' => $session->get('user')]);
});

Route::get('/fornecedores', function (Session $session) {
    if(!$session->has('user')) {
        return redirect('/');
    }
    return view('provider.view', ['name' => $session->get('user')]);
});

Route::get('/produtos', function (Session $session) {
    if(!$session->has('user')) {
        return redirect('/');
    }
    return view('product.view', ['name' => $session->get('user')]);
});

Route::get('/nova-venda', function (Session $session) {
    if(!$session->has('user')) {
        return redirect('/');
    }
    return view('financial.new_sale', ['name' => $session->get('user')]);
});

Route::get('/vendas-em-aberto', function (Session $session) {
    if(!$session->has('user')) {
        return redirect('/');
    }
    return view('financial.open_sales', ['name' => $session->get('user')]);
});

Route::get('/vendas-finalizadas', function (Session $session) {
    if(!$session->has('user')) {
        return redirect('/');
    }
    return view('financial.finish_sales', ['name' => $session->get('user')]);
});