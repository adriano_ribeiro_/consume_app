<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('version_01.clients', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('instagram')->nullable(true);
            $table->string('phone', 11)->nullable(true);
            $table->date('birth_date')->nullable(true);
            $table->bigInteger('person_id');
            $table->foreign('person_id')->references('id')->on('version_01.people');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('version_01.clients');
    }
}