<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('version_01.users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('login');
            $table->string('password', 62);
            $table->bigInteger('person_id');
            $table->foreign('person_id')->references('id')->on('version_01.people');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('version_01.users');
    }
}