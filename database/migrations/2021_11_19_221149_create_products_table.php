<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('version_01.products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->unique();
            $table->string('barcode')->unique();
            $table->bigInteger('category_id');
            $table->decimal('purchase_price', 10, 2);
            $table->integer('quantity');
            $table->integer('percentage_wholesale');
            $table->decimal('wholesale_price', 10, 2);
            $table->integer('percentage_retail');
            $table->decimal('retail_price', 10, 2);
            $table->date('purchase_date');
            $table->bigInteger('provider_id');
            $table->boolean('status');
            $table->foreign('category_id')->references('id')->on('version_01.categories');
            $table->foreign('provider_id')->references('id')->on('version_01.providers');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('version_01.products');
    }
}