@extends('master')

@section('title', 'Dashboard')

@push('stylesheet')
   
@endpush

@section('content')
 <!-- page content -->
 <div class="right_col" role="main">
    <!-- top tiles -->
    <div class="row" style="display: inline-block;" >
    <div class="align-content-center">
        <img src="/assets/images/system.dev.ti.png" alt="">
    </div>
      {{-- <div class="col-md-2 col-sm-4  tile_stats_count">
        <span class="count_top"><i class="fa fa-adjust"></i> Total Produtos</span>
        <div class="count">2500</div>
        <span class="count_bottom"><i class="green">4% </i> Total de </span>
      </div>
      <div class="col-md-2 col-sm-4  tile_stats_count">
        <span class="count_top"><i class="fa fa-clock-o"></i> Average Time</span>
        <div class="count">123.50</div>
        <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>3% </i> From last Week</span>
      </div>
      <div class="col-md-2 col-sm-4  tile_stats_count">
        <span class="count_top"><i class="fa fa-user"></i> Total Males</span>
        <div class="count green">2,500</div>
        <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>34% </i> From last Week</span>
      </div>
      <div class="col-md-2 col-sm-4  tile_stats_count">
        <span class="count_top"><i class="fa fa-user"></i> Total Females</span>
        <div class="count">4,567</div>
        <span class="count_bottom"><i class="red"><i class="fa fa-sort-desc"></i>12% </i> From last Week</span>
      </div>
      <div class="col-md-2 col-sm-4  tile_stats_count">
        <span class="count_top"><i class="fa fa-user"></i> Total Collections</span>
        <div class="count">2,315</div>
        <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>34% </i> From last Week</span>
      </div>
      <div class="col-md-2 col-sm-4  tile_stats_count">
        <span class="count_top"><i class="fa fa-user"></i> Total Connections</span>
        <div class="count">7,325</div>
        <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>34% </i> From last Week</span>
      </div> --}}
    </div>
  </div>
    <!-- /top tiles -->

    <br />

    <div class="row">
    </div>

  </div>
  <!-- /page content -->
@endsection

@push('javascript')
{{-- <script type="text/javascript" src="/assets/js/custom/product.js"></script> --}}
@endpush