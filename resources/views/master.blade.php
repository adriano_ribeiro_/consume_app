<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    {{-- <meta http-equiv="X-UA-Compatible" content="IE=edge"> --}}
    <meta http-equiv="Content-Language" content="pt-br">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	  <link rel="icon" href="/assets/images/favicon.png" type="image/ico" />

    <title>@yield('title')</title>

    <!-- Bootstrap -->
    <link type="text/css" href="/plugins/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link type="text/css" href="/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link type="text/css" href="/plugins/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link type="text/css" href="/plugins/iCheck/skins/flat/green.css" rel="stylesheet">
	
    <!-- bootstrap-progressbar -->
    <link type="text/css" href="/plugins/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- JQVMap -->
    <link type="text/css" href="/plugins/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
    <link type="text/css" href="/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet"/>
    <!-- bootstrap-datepicker -->
    {{-- <link type="text/css" href="/plugins/datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet"> --}}

    {{-- sweetalert2 --}}
    <link rel="stylesheet" type="text/css" href="/plugins/sweetalert2/sweetalert2.css">

    {{-- toastr --}}
    <link rel="stylesheet" type="text/css" href="/plugins/toastr/toastr.css">

    {{-- select2 --}}
    <link rel="stylesheet" type="text/css" href="/plugins/select2/dist/css/select2.min.css">

    {{-- Tippy --}}
    <link rel="stylesheet" type="text/css" href="/plugins/tippy.js/styles/scale.css">

    <!-- Datatables -->
    <link href="./plugins/datatables.net-bs/css/dataTables.bootstrap.min.css" type="text/css" rel="stylesheet">
    <link href="./plugins/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" type="text/css" rel="stylesheet">
    <link href="./plugins/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" type="text/css" rel="stylesheet">
    <link href="./plugins/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" type="text/css" rel="stylesheet">
    <link href="./plugins/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" type="text/css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link type="text/css" href="/assets/css/custom.min.css" rel="stylesheet">

    <style>
      /* body input {
          text-transform: uppercase !important;
      } */
      .dropdown-menu {
        position: absolute !important;
        will-change: transform !important;
        border-radius: 4px !important;
        width: 1px !important;
        top: 10px !important;
        left: 0px !important;
        transform: translate3d(-28px, 21px, 0px) !important;
      }
      .color-default {
        background-color: #2A3F54
      }
      .color-close {
        color: #FFFFFF
      }
    </style>
    @stack('stylesheet')
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <!-- sidebar menu -->
        @include('includes.sidebar')
        <!-- /sidebar menu -->
          
        <!-- top navigation -->
        @include('includes.navbar')
        <!-- /top navigation -->

        <!-- page content -->
        @yield('content')
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="text-center">
            <span>&#169;</span> 2021 system.dev.ti - Todos os direitos reservados.
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script type="text/javascript" src="/plugins/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script type="text/javascript" src="/plugins/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <!-- FastClick -->
    <script type="text/javascript" src="/plugins/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script type="text/javascript" src="/plugins/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script type="text/javascript" src="/plugins/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script type="text/javascript" src="/plugins/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script type="text/javascript" src="/plugins/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script type="text/javascript" src="/plugins/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script type="text/javascript" src="/plugins/skycons/skycons.js"></script>
    <!-- Flot -->
    <script type="text/javascript" src="/plugins/Flot/jquery.flot.js"></script>
    <script type="text/javascript" src="/plugins/Flot/jquery.flot.pie.js"></script>
    <script type="text/javascript" src="/plugins/Flot/jquery.flot.time.js"></script>
    <script type="text/javascript" src="/plugins/Flot/jquery.flot.stack.js"></script>
    <script type="text/javascript" src="/plugins/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script type="text/javascript" src="/plugins/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script type="text/javascript" src="/plugins/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script type="text/javascript" src="/plugins/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script type="text/javascript" src="/plugins/DateJS/build/date.js"></script>
    <!-- JQVMap -->
    <script type="text/javascript" src="/plugins/jqvmap/dist/jquery.vmap.js"></script>
    <script type="text/javascript" src="/plugins/jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script type="text/javascript" src="/plugins/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
    <!-- bootstrap-datepicker -->
    <script type="text/javascript" src="/plugins/moment/min/moment.min.js"></script>
    <script type="text/javascript" src="/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
    {{-- <script type="text/javascript" src="/plugins/datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="/plugins/datepicker/dist/locales/bootstrap-datepicker.pt-BR.min.js"></script> --}}

    {{-- select2 --}}
    <script type="text/javascript" src="/plugins/select2/dist/js/select2.min.js"></script>
    <script type="text/javascript" src="/plugins/select2/dist/js/i18n/pt-BR.js"></script>

    {{-- sweet alert --}}
    <script type="text/javascript" src="/plugins/sweetalert2/sweetalert2.all.js"></script>

    {{-- toastr --}}
    <script type="text/javascript" src="/plugins/toastr/toastr.js"></script>

    {{-- mask --}}
    <script type="text/javascript" src="/plugins/jquery-validation/demo/marketo/jquery.maskedinput.js"></script>
    <script type="text/javascript" src="/plugins/jquery-mask-plugin/dist/jquery.mask.min.js"></script>
    <script type="text/javascript" src="/plugins/string-mask-master/src/string-mask.js"></script>
    <script type="text/javascript" src="/plugins/jquery-maskmoney/dist/jquery.maskMoney.min.js"></script>

    {{-- http request status --}}
    <script type="text/javascript" src="/assets/js/custom/http.request.status.js"></script>

    {{-- message --}}
    <script type="text/javascript" src="/assets/js/custom/message.js"></script>

    <!-- jquery-validator -->
    <script type="text/javascript" src="/plugins/validator/multifield.js"></script>
    <script type="text/javascript" src="/plugins/validator/validator.js"></script>

    {{-- Tippy --}}
    <script type="text/javascript" src="/plugins/tippy.js/popper.min.js"></script>
    <script type="text/javascript" src="/plugins/tippy.js/tippy-bundle.umd.min.js"></script>
    <script type="text/javascript" src="/assets/js/custom/tippy-tooltips.js"></script>

    <script>
      $("[rel=tooltip]").tooltip({html:true});
    </script>

    <!-- Datatables -->
    <script type="text/javascript" src="/plugins/datatables.net/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="/plugins/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript" src="/plugins/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="/plugins/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script type="text/javascript" src="/plugins/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script type="text/javascript" src="/plugins/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="/plugins/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script type="text/javascript" src="/plugins/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script type="text/javascript" src="/plugins/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script type="text/javascript" src="/plugins/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="/plugins/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script type="text/javascript" src="/plugins/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script type="text/javascript" src="/plugins/jszip/dist/jszip.min.js"></script>
    <script type="text/javascript" src="/plugins/pdfmake/build/pdfmake.min.js"></script>
    <script type="text/javascript" src="/plugins/pdfmake/build/vfs_fonts.js"></script>

    <!-- Custom Theme Scripts -->
    <script type="text/javascript" src="/assets/js/custom.min.js"></script>

    <!-- uri -->
    <script type="text/javascript" src="/assets/js/custom/uri.js"></script>
     <!-- app -->
    <script type="text/javascript" src="/assets/js/custom/constants.js"></script>
    <script type="text/javascript" src="/assets/js/custom/app.js"></script>

    <script type="text/javascript">
        countSaleOpenFinish();
    </script>
    
    @stack('javascript')
  </body>
</html>