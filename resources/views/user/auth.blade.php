<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    {{-- <meta http-equiv="X-UA-Compatible" content="IE=edge"> --}}
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Language" content="pt-br">
    <link rel="icon" href="/assets/images/favicon.png" type="image/ico" />

    <title>Login</title>

    <!-- Bootstrap -->
    <link href="/plugins/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="/plugins/nprogress/nprogress.css" rel="stylesheet">
    <!-- Animate.css -->
    <link href="/plugins/animate.css/animate.min.css" rel="stylesheet">

    {{-- toastr --}}
    <link rel="stylesheet" type="text/css" href="/plugins/toastr/toastr.css">

    {{-- sweetalert2 --}}
    <link rel="stylesheet" type="text/css" href="/plugins/sweetalert2/sweetalert2.css">

    <!-- Custom Theme Style -->
    <link href="/assets/css/custom.min.css" rel="stylesheet">
    <style type="text/css">
        .body {
            background-color: #394D5F
        }
    </style>
  </head>

  <body class="login body">
    <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>

      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
            <form id="user-auth">
              <h1>Autenticação</h1>
              <div>
                <input type="text" class="form-control" id="username" name="username" placeholder="Login"/>
              </div>
              <div>
                <input type="password" class="form-control" id="password" name="password" placeholder="Senha"/>
              </div>
              <div>
                <button type="submit" class="btn btn-outline-light btn-lg">Acessar</button>
              </div>

              <div class="clearfix"></div>

              <div class="separator">
                <div class="clearfix"></div>
                <br />

                <div>
                  <h1>system.dev.ti</h1>
                  <p><span>&#169;</span> 2021 - Todos os direitos reservados.</p>
                </div>
              </div>
            </form>
          </section>
        </div>
      </div>
    </div>

    <!-- jQuery -->
    <script type="text/javascript" src="/plugins/jquery/dist/jquery.min.js"></script>
    {{-- sweet alert --}}
    <script type="text/javascript" src="/plugins/sweetalert2/sweetalert2.all.js"></script>
    {{-- toastr --}}
    <script type="text/javascript" src="/plugins/toastr/toastr.js"></script>
    <script type="text/javascript" src="/assets/js/custom/auth.js"></script>
  </body>
</html>