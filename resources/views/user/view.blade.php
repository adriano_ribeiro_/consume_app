@extends('master')

@section('title', 'Administração de usuários')

@push('stylesheet')
   
@endpush

@section('content')
<!-- page content -->
<div class="right_col" role="main">
    <div class="">
      <div class="page-title">
        <div class="text-center">
          <h3><b>Administração de usuários</b></h3>
        </div>
      </div>

      <div class="clearfix mt-5"></div>

      <div class="row">
        <div class="col-md-12 col-sm-12 ">
          <div class="x_panel">
            <div class="x_title">
                <div class="float-right">
                    <button type="button" onClick="modalCreateOrUpdate()" class="btn btn-success scroll-click float-right">
                        <i class="icon-copy fa fa-plus" aria-hidden="true"></i> Cadastrar usuários
                    </button>
                </div>
              <h2>
                <i class="icon-copy fa fa-align-justify" aria-hidden="true"></i>
                  <b>Lista de usuários</b>
              </h2>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box table-responsive">
                            <table id="users-table" class="table table-striped table-bordered w-100">
                                <thead>
                                <tr>
                                    <th>Nome</th>
                                    <th>Email</th>
                                    <th>Login</th>
                                    <th style="width: 5em">Ação</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal" tabindex="-1" role="dialog" id="modal-create-or-update">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header color-default">
                            <h5 class="modal-title"></h5>
                            <button type="button" onClick="modalClose()" class="close color-close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div role="main">
                                <div class="">
                                    <div class="clearfix"></div>
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12">
                                            <div class="x_panel">
                                                <div class="x_title text-center">
                                                    <h1><b class="title-form-user"></b></h1>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="x_content">
                                                    <br />
                                                    <form id="user-create-update">
                                                        <input type="hidden" id="user_id" name="user_id">
                                                        <div class="item form-group">
                                                            <label class="col-form-label col-md-3 col-sm-1" for="name"><b>Nome</b> 
                                                                <span class="required text-danger">*</span>
                                                            </label>
                                                            <div class="col-md-6 col-sm-6">
                                                                <input type="text" id="name" name="name" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="item form-group">
                                                            <label class="col-form-label col-md-3 col-sm-1" for="email"><b>Email</b> 
                                                                <span class="required text-danger">*</span>
                                                            </label>
                                                            <div class="col-md-6 col-sm-6">
                                                                <input type="text" id="email" name="email" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="item form-group">
                                                            <label class="col-form-label col-md-3 col-sm-1" for="login"><b>Login</b>
                                                                 <span class="required text-danger">*</span>
                                                            </label>
                                                            <div class="col-md-6 col-sm-6 ">
                                                                <input class="form-control" type="text" id="login" name="login">
                                                            </div>
                                                        </div>
                                                        <div class="item form-group password">
                                                            <label for="password" class="col-form-label col-md-3 col-sm-1"><b>Senha</b>
                                                                <span class="required text-danger">*</span>
                                                            </label>
                                                            <div class="col-md-6 col-sm-6 ">
                                                                <input class="form-control" type="password" id="password" name="password">
                                                            </div>
                                                        </div>
                                                        <div class="item form-group password">
                                                            <label for="confirm_password" class="col-form-label col-md-3 col-sm-1"><b>Confirme senha</b>
                                                                <span class="required text-danger">*</span>
                                                            </label>
                                                            <div class="col-md-6 col-sm-6">
                                                                <input class="form-control" type="password" id="confirm_password" name="confirm_password">
                                                            </div>
                                                        </div>
                                                        <div class="ln_solid"></div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer color-default">
                            <button type="button" onClick="modalClose()" class="btn btn-danger" data-dismiss="modal">
                                <i class="icon-copy fa fa-close" aria-hidden="true"></i> Fechar
                            </button>
                            <button type="button" onClick="createOrUpdate()" class="btn btn-info">
                                <i class="icon-copy fa fa-check" aria-hidden="true"> <a class="create-update"></a></i> 
                            </button>
                        </div>
                    </div>
                </div>
            </div>

          </div>
        </div>
      </div>
    </div>
</div>
  <!-- /page content -->
@endsection
@push('javascript')
<script type="text/javascript" src="/assets/js/custom/user.js"></script>
@endpush