@extends('master')

@section('title', 'Produtos')

@push('stylesheet')
   
@endpush

@section('content')
<div class="right_col" role="main">
    <div class="">
      <div class="page-title">
        <div class="text-center">
          <h3><b>Administração de Produtos</b></h3>
        </div>
      </div>

      <div class="clearfix mt-5"></div>

      <div class="row">
        <div class="col-md-12 col-sm-12 ">
          <div class="x_panel">
            <div class="x_title">
                <div class="float-right">
                    <button type="button" onClick="modalCreateOrUpdate()" class="btn btn-success scroll-click float-right">
                        <i class="icon-copy fa fa-plus" aria-hidden="true"></i> Cadastrar Produto
                    </button>
                </div>
              <h2>
                <i class="icon-copy fa fa-align-justify" aria-hidden="true"></i>
                  <b>Lista de Produtos</b>
              </h2>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box table-responsive">
                            <table id="products-table" class="table table-striped table-bordered w-100">
                                <thead>
                                <tr>
                                    {{-- <th>Código de barras</th> --}}
                                    <th>Nome</th>
                                    <th>Fornecedor</th>
                                    <th>Categoria</th>
                                    {{-- <th>Preço de compra</th> --}}
                                    {{-- <th>Atacado %</th> --}}
                                    {{-- <th>Preço de atacado</th> --}}
                                    {{-- <th>Varejo %</th> --}}
                                    <th>Valor</th>
                                    <th>qtd</th>
                                    {{-- <th>Data de compra</th> --}}
                                    <th style="width: 13em">Ação</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal" tabindex="-1" role="dialog" id="modal-create-or-update">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header color-default">
                            <h5 class="modal-title"></h5>
                            <button type="button" onClick="modalClose()" class="close color-close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body overflow-auto">
                            <div role="main">
                                <div class="">
                                    <div class="clearfix"></div>
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12">
                                            <div class="x_panel">
                                                <div class="x_title text-center">
                                                    <h1><b class="title-form-product"></b></h1>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="x_content">
                                                    <br />
                                                    <form id="product-create-update">
                                                        <input type="hidden" id="product_id" name="product_id">
                                                        <div class="item form-group">
                                                            <label class="col-form-label col-md-3 col-sm-6" for="barcode"><b>Código de barras</b> 
                                                                <span class="required text-danger">*</span>
                                                            </label>
                                                            <div class="col-md-6 col-sm-6">
                                                                <input type="text" id="barcode" name="barcode" class="form-control">
                                                            </div>
                                                        </div>
                            
                                                        <div class="item form-group">
                                                            <label class="col-form-label col-md-3 col-sm-6" for="name"><b>Nome</b> 
                                                                <span class="required text-danger">*</span>
                                                            </label>
                                                            <div class="col-md-6 col-sm-6">
                                                                <input type="text" id="name" name="name" class="form-control">
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="item form-group">
                                                            <label class="col-form-label col-md-3 col-sm-6" for="provider_id"><b>Fornecedor</b> 
                                                                <span class="required text-danger">*</span>
                                                            </label>
                                                            <div class="col-md-6 col-sm-6">
                                                                <select id="provider_id" name="provider" class="form-control"></select>
                                                            </div>
                                                        </div>
                            
                                                        <div class="item form-group">
                                                            <label class="col-form-label col-md-3 col-sm-6" for="category_id"><b>Categoria</b> 
                                                                <span class="required text-danger">*</span>
                                                            </label>
                                                            <div class="col-md-6 col-sm-6">
                                                                <select id="category_id" name="category" class="form-control"></select>
                                                            </div>
                                                        </div>
                            
                                                        <div class="item form-group">
                                                            <label class="col-form-label col-md-3 col-sm-6" for="purchase_price"><b>Preço de compra</b> 
                                                                <span class="required text-danger">*</span>
                                                            </label>
                                                            <div class="col-md-6 col-sm-6">
                                                                <input type="text" id="purchase_price" name="purchase_price" class="form-control money calculate_price">
                                                            </div>
                                                        </div>

                                                        <div class="item form-group">
                                                            <label class="col-form-label col-md-3 col-sm-6" for="percentage_wholesale"><b>Atacado %</b> 
                                                                <span class="required text-danger">*</span>
                                                            </label>
                                                            <div class="col-md-6 col-sm-6">
                                                                <input type="number" id="percentage_wholesale" name="percentage_wholesale" class="form-control calculate_price" value="30">
                                                            </div>
                                                        </div>
                            
                                                        <div class="item form-group">
                                                            <label class="col-form-label col-md-3 col-sm-6" for="wholesale_price"><b>Preço no atacado</b> 
                                                                <span class="required text-danger">*</span>
                                                            </label>
                                                            <div class="col-md-6 col-sm-6">
                                                                <input type="text" id="wholesale_price" name="wholesale_price" class="form-control money calculate_price">
                                                            </div>
                                                        </div>

                                                        <div class="item form-group">
                                                            <label class="col-form-label col-md-3 col-sm-6" for="percentage_retail"><b>Varejo %</b> 
                                                                <span class="required text-danger">*</span>
                                                            </label>
                                                            <div class="col-md-6 col-sm-6">
                                                                <input type="number" id="percentage_retail" name="percentage_retail" class="form-control calculate_price" value="40">
                                                            </div>
                                                        </div>

                                                        <div class="item form-group">
                                                            <label class="col-form-label col-md-3 col-sm-6" for="retail_price"><b>Preço no varejo</b> 
                                                                <span class="required text-danger">*</span>
                                                            </label>
                                                            <div class="col-md-6 col-sm-6">
                                                                <input type="text" id="retail_price" name="retail_price" class="form-control money calculate_price">
                                                            </div>
                                                        </div>
                            
                                                        <div class="item form-group">
                                                            <label class="col-form-label col-md-3 col-sm-6" for="quantity"><b>Qtd</b> 
                                                                <span class="required text-danger">*</span>
                                                            </label>
                                                            <div class="col-md-6 col-sm-6">
                                                                <input type="number" id="quantity" name="quantity" class="form-control">
                                                            </div>
                                                        </div>
                            
                                                        <div class="item form-group">
                                                            <label class="col-form-label col-md-3 col-sm-6" for="purchase_date"><b>Data de compra</b> 
                                                                <span class="required text-danger">*</span>
                                                            </label>
                                                            <div class="col-md-6 col-sm-6">
                                                                <input type="text" id="purchase_date" name="purchase_date" class="form-control">
                                                            </div>
                                                        </div>
                            
                                                        <div class="ln_solid"></div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer color-default">
                            <button type="button" onClick="modalClose()" class="btn btn-danger" data-dismiss="modal">
                                <i class="icon-copy fa fa-close" aria-hidden="true"></i> Fechar
                            </button>
                            <button type="button" onClick="createOrUpdate()" class="btn btn-info details">
                                <i class="icon-copy fa fa-check" aria-hidden="true"> <a class="create-update"></a></i> 
                            </button>
                        </div>
                    </div>
                </div>
            </div>

          </div>
        </div>
      </div>
    </div>
</div>
@endsection
@push('javascript')
<script type="text/javascript" src="/assets/js/custom/product.js"></script>
@endpush