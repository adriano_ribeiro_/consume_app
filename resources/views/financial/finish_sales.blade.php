@extends('master')

@section('title', 'Vendas finalizadas')

@push('stylesheet')
   
@endpush

@section('content')
<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="text-center">
            <h3><b>Venda Finalizadas</b></h3>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="x_content">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card-box table-responsive">
                        <table id="finish-sales-table" class="table table-striped table-bordered w-100">
                            <thead>
                            <tr>
                                <th>Valor Total</th>
                                <th>Data da venda</th>
                                <th style="width: 10em">Ação</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        {{-- modal remover item --}}
        <div class="modal" tabindex="-1" role="dialog" id="modal-details">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header color-default">
                        <h5 class="modal-title"></h5>
                        <button type="button" class="close color-close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="h2"><b class="text-danger sale_value">R$ 0,00</b></div>
                        <div class="card-box table-responsive">
                            <table id="finish-sale-table" class="table table-striped table-bordered w-100">
                                <thead>
                                <tr>
                                    <th>Cód de barras</th>
                                    <th>Produto</th>
                                    <th>Preço Unit</th>
                                    <th>Qtd</th>
                                    <th>Subtotal</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <div class="modal-footer color-default">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">
                            <i class="icon-copy fa fa-close" aria-hidden="true"></i> FECHAR
                        </button>
                        <button type="button" onClick="print()" class="btn btn-info">
                            <i class="icon-copy fa fa-print" aria-hidden="true"></i> Imprimir
                        </button>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
@endsection

@push('javascript')
<script type="text/javascript" src="/assets/js/custom/finish.sales.js"></script>
@endpush