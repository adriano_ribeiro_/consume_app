@extends('master')

@section('title', 'Caixa')

@push('stylesheet')
<style type="text/css">
    .select2-container {
        width: 100% !important;
        padding: 0;
    }
</style>
@endpush

@section('content')
<!-- page content -->
<div class="right_col" role="main">
    <div class="">
      <div class="page-title">
        <div class="text-center">
          <h3><b>Caixa</b></h3>
        </div>
      </div>

      <div class="clearfix"></div>

      <div>
        <div class="float-right">
            <button type="button" onClick="newSale()" class="btn btn-success scroll-click float-right">
                <i class="icon-copy fa fa-plus" aria-hidden="true"></i> Nova venda
            </button>
        </div>
      <div class="clearfix"></div>
    </div>

      <div class="container">
        <div class="row">
          <div class="col-sm-12">
            <div role="main">
              <div class="">
                  <div class="clearfix"></div>
                  <div class="row">
                      <div class="col-md-12 col-sm-12">
                          <div class="x_panel">
                              <div class="x_content">
                                  <br />
                                  <form id="sale-item-create-update">
                                      <input type="hidden" id="sale_id" name="sale_id">
                                      <div class="item form-group">
                                          <label class="col-form-label col-md-3 col-sm-3" for="barcode-product"><b>Cód de barras | Produto</b> 
                                              <span class="required text-danger">*</span>
                                          </label>
                                          <div class="col-xs-12 col-sm-4 col-md-12 col-lg-6">
                                            <select id="barcode-product" name="barcode-product" class="form-control"></select>
                                          </div>
                                      </div>
                                      <div class="item form-group">
                                          <label class="col-form-label col-md-3 col-sm-3" for="quantity"><b>Qtd</b> 
                                              <span class="required text-danger">*</span>
                                          </label>
                                          <div class="col-xs-12 col-sm-4 col-md-12 col-lg-6">
                                              <input type="number" class="form-control" id="quantity" name="quantity" value="1">
                                          </div>
                                      </div>
                                      <div class="item form-group">
                                        <div class="checkbox">
                                            <label class="">
                                                <b>Venda no atacado? </b>
                                                <div class="icheckbox_flat-green checked" style="position: relative;">
                                                    <input type="checkbox" id="is-attacked" name="is-attacked" class="flat" style="position: absolute; opacity: 0;">
                                                    <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                                </div>
                                            </label>
                                        </div>
                                      </div>
                                      <div class="ln_solid"></div>
                                      <button type="button" onClick="box()" class="btn btn-success btn-box">
                                          <i class="icon-copy fa fa-plus" aria-hidden="true"></i> Adicionar
                                      </button>
                                  </form>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          </div>
            <div class="col-sm-12">
                <div role="main">
                    <div class="">
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="x_panel">
                                <div class="x_content">
                                    <div class="float-right">
                                        <button type="button" onClick="modalFinishSale()" class="btn btn-info scroll-click float-right">
                                            <i class="icon-copy fa fa-check" aria-hidden="true"></i> Finalizar venda
                                        </button>
                                    </div>
                                    <div class="h2"><b class="text-danger amount">R$ 0,00</b></div>
                                    <div class="card-box table-responsive">
                                        <table id="sale-table" class="table table-striped table-bordered w-100">
                                            <thead>
                                            <tr>
                                                <th>Cód de barras</th>
                                                <th>Produto</th>
                                                <th>Preço Unit</th>
                                                <th>Qtd</th>
                                                <th>Subtotal</th>
                                                <th style="width: 10em">Ação</th>
                                            </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
          
           </div>
        </div>
      </div>

      {{-- modal remover item --}}
      <div class="modal" tabindex="-1" role="dialog" id="modal-remove-item">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header color-default">
                    <h5 class="modal-title"></h5>
                    <button type="button" onClick="modalRemoveItemClose()" class="close color-close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div role="main">
                        <div class="">
                            <div class="clearfix"></div>
                            <div class="row">
                                <div class="col-md-12 col-sm-12">
                                    <div class="x_panel">
                                        <div class="x_content">
                                            <br />
                                            <form id="finish-sale">
                                                <input type="hidden" id="item_id" name="item_id" class="form-control">
                                                <div class="item form-group">
                                                  <label class="col-form-label col-md-3 col-sm-1" for="items"><b>Tota de itens</b> 
                                                  </label>
                                                  <div class="col-md-6 col-sm-6">
                                                      <input type="text" id="items" name="items" class="form-control" disabled>
                                                  </div>
                                                </div>
                                                <div class="item form-group">
                                                    <label class="col-form-label col-md-3 col-sm-1" for="quantity-items"><b>Quantos items deseja remover?</b> 
                                                    </label>
                                                    <div class="col-md-6 col-sm-6">
                                                        <input type="text" id="quantity-items" name="quantity-items" class="form-control">
                                                    </div>
                                                  </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer color-default">
                    <button type="button" onClick="modalRemoveItemClose()" class="btn btn-danger" data-dismiss="modal">
                        <i class="icon-copy fa fa-close" aria-hidden="true"></i> FECHAR
                    </button>
                    <button type="button" onClick="removeItem()" class="btn btn-info">
                        <i class="icon-copy fa fa-check" aria-hidden="true"></i> Remover
                    </button>
                </div>
            </div>
        </div>
      </div>

      {{-- modal finalizar venda --}}
      <div class="modal" tabindex="-1" role="dialog" id="modal-finish-sale">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header color-default">
                    <h5 class="modal-title"></h5>
                    <button type="button" onClick="modalClose()" class="close color-close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div role="main">
                        <div class="">
                            <div class="clearfix"></div>
                            <div class="row">
                                <div class="col-md-12 col-sm-12">
                                    <div class="x_panel">
                                        <div class="x_content">
                                            <br />
                                            <form id="finish-sale">
                                                <div class="item form-group w-100">
                                                    <label class="col-form-label col-md-3 col-sm-3 w-100" for="discount"><b>Cliente</b> 
                                                        {{-- <span class="required text-danger">*</span> --}}
                                                    </label>
                                                    <div class="col-xs-12 col-sm-4 col-md-12 col-lg-6 w-100">
                                                      <select id="client_id" name="client_id" class="form-control w-100"></select>
                                                    </div>
                                                </div>
                                                <div class="item form-group">
                                                  <label class="col-form-label col-md-3 col-sm-3" for="is_discount"><b>Tem Desconto?</b> 
                                                      {{-- <span class="required text-danger">*</span> --}}
                                                  </label>
                                                  <div class="col-xs-12 col-sm-4 col-md-12 col-lg-6">
                                                    <select id="is_discount" name="is_discount" class="form-control">
                                                      <option value="0" selected>Não</option>
                                                      <option value="1">Sim</option>
                                                    </select>
                                                  </div>
                                                </div>
                                                <div class="item form-group discount" hidden>
                                                  <label class="col-form-label col-md-3 col-sm-1" for="discount"><b>Desconto %</b> 
                                                      <span class="required text-danger">*</span>
                                                  </label>
                                                  <div class="col-md-6 col-sm-6">
                                                      <input type="number" id="discount" name="discount" class="form-control">
                                                  </div>
                                                </div>

                                                <div class="item form-group">
                                                  <label class="col-form-label col-md-3 col-sm-1" for="amount"><b>Total a pagar</b> 
                                                  </label>
                                                  <div class="col-md-6 col-sm-6">
                                                      <input type="text" id="amount" name="amount" class="form-control" disabled>
                                                  </div>
                                                </div>

                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer color-default">
                    <button type="button" onClick="modalClose()" class="btn btn-danger" data-dismiss="modal">
                        <i class="icon-copy fa fa-close" aria-hidden="true"></i> FECHAR
                    </button>
                    <button type="button" onClick="finishSale()" class="btn btn-info">
                        <i class="icon-copy fa fa-check" aria-hidden="true"></i> Finalizar
                    </button>
                </div>
            </div>
        </div>
    </div>
      
    </div>
</div>
  <!-- /page content -->
@endsection
@push('javascript')
<script type="text/javascript" src="/assets/js/custom/new.sale.js"></script>
@endpush