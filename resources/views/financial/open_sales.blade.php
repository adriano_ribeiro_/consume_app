@extends('master')

@section('title', 'Vendas em aberto')

@push('stylesheet')
   
@endpush

@section('content')
<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="text-center">
            <h3><b>Venda em aberto</b></h3>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="x_content">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card-box table-responsive">
                        <table id="open-sales-table" class="table table-striped table-bordered w-100">
                            <thead>
                            <tr>
                                <th>Valor Total</th>
                                <th>Data da venda</th>
                                <th style="width: 5em">Ação</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
@endsection

@push('javascript')
<script type="text/javascript" src="/assets/js/custom/open.sales.js"></script>
@endpush